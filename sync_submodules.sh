#!/bin/bash
# git submodule foreach git pull --rebase origin master
git submodule update --recursive

# Update TurtleTests
(
    echo "[ Make TurtleTests ]"
    cd src/roms/TurtleTests
    make clean
    make all
)

# Update mooneye-gb
(
    echo "[ Make mooneye-gb ]"
    cd src/roms/mooneye-gb/tests
    make clean
    make all
)

# Update mealybug-tearoom-tests
(
    echo "[ Make mealybug-tearoom-tests ]"
    cd src/roms/mealybug-tearoom-tests
    make clean
    make all
)

# Update BullyGB
(
    echo "[ Make BullyGB ]"
    cd src/roms/BullyGB
    make clean
    make
)

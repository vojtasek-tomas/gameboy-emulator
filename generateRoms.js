/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable @typescript-eslint/no-var-requires */
const fg = require('fast-glob')
const { partition } = require('lodash')
const fs = require('fs')

const entries = fg.sync(['src/roms/**/*.gb'])

const roms = entries
  .map((e) => e.replace('src/roms/', ''))
  .map((e) => `'${e}'`)
  .sort()

const [games, tests] = partition(roms, (romName) =>
  romName.startsWith("'games")
)

function generateOutput(items) {
  return `export const roms = [
  ${items.join(',\n  ')}
]
`
}

fs.writeFileSync('./src/roms/tests.ts', generateOutput(tests))
fs.writeFileSync('./src/roms/games.ts', generateOutput(games))

/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { execSync } = require('child_process')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const CircularDependencyPlugin = require('circular-dependency-plugin')
const CompressionPlugin = require('compression-webpack-plugin')

const gitHash = execSync('git rev-parse --short=7 HEAD', {
  encoding: 'utf8'
}).trim()

const devOptions = {
  mode: 'development',
  devServer: {
    publicPath: '',
    host: '0.0.0.0',
    port: 8000,
    hot: true,
    inline: true,
    contentBase: path.join(__dirname, 'assets'),
    historyApiFallback: true
  },
  watchOptions: {
    ignored: [
      'node_modules', // saves lots of CPU, needs restart after adding new modules!
      '**/__tests__/**/*.{ts,tsx}'
    ]
  }
}

const productionOptions = {
  mode: 'production'
}

const basePlugins = [
  new HtmlWebpackPlugin({
    template: './index.html'
  }),
  new ForkTsCheckerWebpackPlugin(),
  new CircularDependencyPlugin({
    exclude: /node_modules/
  })
]

const devPlugins = [
  ...basePlugins,
  new webpack.HotModuleReplacementPlugin(),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('development')
    }
  })
]

const productionPlugins = [
  ...basePlugins,
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production')
    }
  }),
  new CompressionPlugin()
]

module.exports = (env, argv) => {
  const isProduction = argv.mode === 'production'
  return {
    ...(isProduction ? productionOptions : devOptions),
    optimization: {
      minimize: isProduction
    },
    performance: {
      hints: isProduction ? 'warning' : false // disable bundle size warning
    },
    devtool: isProduction ? 'source-map' : 'eval-cheap-source-map',
    entry: {
      app: './src/main.tsx'
    },
    output: {
      publicPath: '',
      path: path.join(__dirname, 'public'),
      filename: `[name].${gitHash}.js`,
      chunkFilename: `[id].[name].${gitHash}.js`
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loader: 'ts-loader',
          options: {
            // disable type checker - we will use it in fork plugin
            transpileOnly: true
          }
        },
        {
          test: /\.gb$/,
          use: [{ loader: 'binary-loader' }]
        }
      ]
    },
    plugins: isProduction ? productionPlugins : devPlugins
  }
}

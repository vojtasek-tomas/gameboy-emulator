module.exports = {
  jsxBracketSameLine: true,
  printWidth: 80,
  semi: false,
  singleQuote: true,
  trailingComma: 'none'
}

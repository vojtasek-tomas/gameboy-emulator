import fs from 'fs'
import path from 'path'

import { CYCLES_PER_FRAME } from '../../src/lib/constants'
import {
  getOuputDir,
  runVisualTest,
  generateVisualResultHeader,
  generateVisualResultRow
} from './runner_utils'

export function collectResults() {
  const originalReferencePath = `${__dirname}/../../src/roms/dmg-acid2/img/reference-dmg.png`

  const testPath = `${__dirname}/../../src/roms/dmg-acid2/build/dmg-acid2.gb`
  const referencePath = path.join(getOuputDir(), '/dmg-acid2-expected.png')
  const resultPath = path.join(getOuputDir(), '/dmg-acid2-result.png')
  const diffPath = path.join(getOuputDir(), '/dmg-acid2-diff.png')

  fs.copyFileSync(originalReferencePath, referencePath)

  return runVisualTest(
    testPath,
    20 * CYCLES_PER_FRAME,
    resultPath,
    referencePath,
    diffPath
  ).then((testResult) => {
    return `
${generateVisualResultHeader(
  'dmg-acid2 test',
  'https://github.com/mattcurrie/dmg-acid2'
)}
${generateVisualResultRow(
  'dmg-acid2',
  testResult,
  resultPath,
  referencePath,
  diffPath
)}
`
  })
}

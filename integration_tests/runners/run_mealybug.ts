import fg from 'fast-glob'
import path from 'path'
import fs from 'fs'

import {
  runVisualTest,
  initEnvironment,
  generateVisualResultRow,
  generateVisualResultHeader
} from './runner_utils'
import { CYCLES_PER_FRAME } from '../../src/lib/constants'

const EXPECTED_PATH = `${__dirname}/../../src/roms/mealybug-tearoom-tests/expected/DMG-blob`

function getTestFiles() {
  // find only tests with reference images
  return fg
    .sync([`${EXPECTED_PATH}/**/*.png`])
    .map((filename) =>
      filename
        .replace('/expected/DMG-blob/', '/build/ppu/')
        .replace('.png', '.gb')
    )
}

function runTest(filename: string) {
  const testPath = filename
  const [name] = path.basename(testPath).split('.') // foo/bar/test.gb -> test
  const originalReferencePath = path.join(EXPECTED_PATH, `${name}.png`)

  const diffPath = `${__dirname}/../results/mealybug/${name}-diff.png`
  const resultPath = `${__dirname}/../results/mealybug/${name}-result.png`
  const referencePath = `${__dirname}/../results/mealybug/${name}-expected.png`
  fs.copyFileSync(originalReferencePath, referencePath)

  return runVisualTest(
    testPath,
    20 * CYCLES_PER_FRAME,
    resultPath,
    referencePath,
    diffPath
  ).then((testResult) => {
    return generateVisualResultRow(
      name,
      testResult,
      resultPath,
      referencePath,
      diffPath
    )
  })
}

export function collectResults() {
  const filenames = getTestFiles()
  const promises = filenames.map(runTest)

  return Promise.all(promises).then((results) => {
    return `
${generateVisualResultHeader(
  'Mealybug Tearoom Tests',
  'https://github.com/mattcurrie/mealybug-tearoom-tests'
)}
${results.join('\n')}
    `
  })
}

if (require.main === module) {
  initEnvironment()
  collectResults().then(console.log)
}

import fg from 'fast-glob'
import path from 'path'

import { CYCLES_PER_FRAME } from '../../src/lib/constants'
import { runTestRom, getResultMark } from './runner_utils'

const customSkip = [
  /madness\/mgb_oam_dma_halt_sprites\.gb$/,
  /manual-only\/sprite_priority\.gb$/,
  /utils\/bootrom_dumper\.gb$/,
  /utils\/dump_boot_hwio\.gb$/
]

/**
 * Test groups:
 *
 * dmg = Game Boy
 * mgb = Game Boy Pocket
 * sgb = Super Game Boy
 * sgb2 = Super Game Boy 2
 * cgb = Game Boy Color
 * agb = Game Boy Advance
 * ags = Game Boy Advance SP
 *
 * G = dmg+mgb
 * S = sgb+sgb2
 * C = cgb+agb+ags
 * A = agb+ags
 */

const MOONEYE_TESTS_PATH = `${__dirname}/../../src/roms/mooneye-gb/tests/build`

function getTestFiles() {
  return fg.sync([`${MOONEYE_TESTS_PATH}/**/*.gb`]).filter((file) => {
    // Skip custom selection
    if (customSkip.some((pattern) => pattern.test(file))) {
      return false
    }

    const f = file.match(/.*\/(.*)\.gb$/)
    if (!f) {
      throw new Error(`Can't match filename ${file}`)
    }
    const [, testName] = f

    // Test with no group -> use
    if (!testName.includes('-')) {
      return true
    }
    const m = file.match(/^(.*)-(.*)\.gb$/)
    if (!m) {
      throw new Error(`Can't match filename ${file}`)
    }
    const [, , group] = m

    // TODO test dmgABC as well?
    // test only dmg0 + G groups
    return group.includes('G') || group.includes('dmg0')
  })
}

const ONE_SEC_CYCLES = CYCLES_PER_FRAME * 60
const DEFAULT_TIME = 3 * ONE_SEC_CYCLES
const runTimes = [
  {
    regexp: /mbc1\/bits_ramg\.gb$/,
    time: ONE_SEC_CYCLES * 6
  },
  {
    regexp: /mbc2\/bits_ramg\.gb$/,
    time: ONE_SEC_CYCLES * 8
  },
  {
    regexp: /mbc2\/bits_romb\.gb$/,
    time: ONE_SEC_CYCLES * 5
  }
]

function runTest(filename: string) {
  const testName = path.relative(MOONEYE_TESTS_PATH, filename)
  try {
    console.log('Running ', filename)

    const runTime = runTimes.find((t) => t.regexp.test(testName))
    const e = runTestRom(filename, runTime?.time || DEFAULT_TIME)
    const passed =
      e.cpu.get('B') === 0x03 &&
      e.cpu.get('C') === 0x05 &&
      e.cpu.get('D') === 0x08 &&
      e.cpu.get('E') === 0x0d &&
      e.cpu.get('H') === 0x15 &&
      e.cpu.get('L') === 0x22

    return {
      passed,
      testName
    }
  } catch (error) {
    return {
      passed: false,
      testName,
      error
    }
  }
}

export function collectResults() {
  const filenames = getTestFiles()
  const results = filenames.map(runTest)

  const md = `
## Mooneye's test

https://github.com/Gekkio/mooneye-gb

| Test | Result |
|------|--------|
${results
  .map((result) => `| ${result.testName} | ${getResultMark(result.passed)} |`)
  .join('\n')}
`
  return Promise.resolve(md)
}

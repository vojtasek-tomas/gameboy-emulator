import { CYCLES_PER_FRAME } from '../../src/lib/constants'
import {
  runVisualTest,
  generateVisualResultRow,
  generateVisualResultHeader
} from './runner_utils'

const testCases = [
  {
    name: 'window_y_trigger'
  },
  {
    name: 'window_y_trigger_wx_offscreen'
  }
]

export function collectResults() {
  const promises = testCases.map((testCase) => {
    const { name } = testCase

    const testPath = `${__dirname}/../../src/roms/TurtleTests/src/${name}/${name}.gb`
    const diffPath = `${__dirname}/../results/${name}-diff.png`
    const resultPath = `${__dirname}/../results/${name}-result.png`
    const referencePath = `${__dirname}/../results/${name}-expected.png`

    return runVisualTest(
      testPath,
      20 * CYCLES_PER_FRAME,
      resultPath,
      referencePath,
      diffPath
    ).then((testResult) => {
      return generateVisualResultRow(
        testCase.name,
        testResult,
        resultPath,
        referencePath,
        diffPath
      )
    })
  })

  return Promise.all(promises).then((results) => {
    return `
${generateVisualResultHeader(
  'TurtleTests',
  'https://github.com/Powerlated/TurtleTests'
)}
${results.join('\n')}
    `
  })
}

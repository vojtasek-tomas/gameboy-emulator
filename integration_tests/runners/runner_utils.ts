import fs from 'fs'
import path from 'path'
import enableDom from 'jsdom-global'
import { polyfill } from 'raf'
import Jimp from 'jimp'

import { Emulator } from '../../src/lib/Emulator'
import { MemoryDisplay } from '../../src/lib/ppu/displays/MemoryDisplay'
import { createCartridge } from '../../src/lib/cartridges/factory'
import { DISPLAY_WIDTH, DISPLAY_HEIGHT } from '../../src/lib/constants'
import { Palette } from '../../src/lib/types'

export const SUCCESS_MARK = '✅'
export const FAILURE_MARK = '❌'

type VisualTestResult = {
  success: boolean
  percent: number
}

export function initEnvironment() {
  enableDom() // Enable window
  polyfill(window) // Polyfill requestAnimationFrame
}

export function runTestRom(
  filename: string,
  cyclesToRun: number,
  palette?: Palette
) {
  const e = new Emulator(new MemoryDisplay(), { useBios: false, palette })
  const bytes = fs.readFileSync(filename)

  const { cartridge } = createCartridge(bytes)
  e.loadCartridge(cartridge)

  e.runCycles(cyclesToRun)
  e.stop()

  return e
}

export function getOuputDir() {
  return path.join(__dirname, '../results')
}

export function saveScreen(e: Emulator, resultPath: string) {
  const generatedImage = new Jimp({
    data: e.ppu.display.getImageData(),
    width: DISPLAY_WIDTH,
    height: DISPLAY_HEIGHT
  })
  generatedImage.write(resultPath)
  return generatedImage
}

export function runVisualTest(
  testPath: string,
  cycles: number,
  resultPath: string,
  referencePath: string,
  diffPath: string,
  palette?: Palette
): Promise<VisualTestResult> {
  const e = runTestRom(testPath, cycles, palette)
  const resultImage = saveScreen(e, resultPath)

  return Jimp.read(referencePath).then((referenceImage) => {
    // Pixel difference
    const diff = Jimp.diff(resultImage, referenceImage)
    diff.image.write(diffPath)

    return {
      success: diff.percent === 0,
      percent: diff.percent
    }
  })
}

export function getResultMark(success: boolean) {
  return success ? SUCCESS_MARK : FAILURE_MARK
}

/**
 * Relative path for markdown
 */
function getRelative(filepath: string) {
  return path.relative(path.join(__dirname, '..'), filepath)
}

export function generateVisualResultRow(
  name: string,
  testResult: VisualTestResult,
  resultPath: string,
  referencePath: string,
  diffPath: string
) {
  const resultMark = getResultMark(testResult.success)
  const expected = getRelative(referencePath)
  const result = getRelative(resultPath)
  const diff = getRelative(diffPath)
  return `| ${name} | ![](${expected})| ![](${result}) | ![](${diff}) | ${resultMark} Diff: *${testResult.percent.toFixed(
    4
  )}* |`
}

export function generateVisualResultHeader(name: string, url: string) {
  return `
## ${name}

${url}

| Name | Expected | Result | Diff | Status |
|------|----------|--------|------|--------|`
}

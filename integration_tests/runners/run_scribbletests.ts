import fs from 'fs'

import { CYCLES_PER_FRAME } from '../../src/lib/constants'
import {
  runVisualTest,
  generateVisualResultRow,
  generateVisualResultHeader
} from './runner_utils'
import { GREEN_COLORS } from '../../src/lib/ppu/utils'

const testCases = [
  {
    name: 'scxly',
    path: 'scxly/scxly.gb'
  },
  {
    name: 'lycscx',
    path: 'lycscx/lycscx.gb'
  },
  {
    name: 'lycscy',
    path: 'lycscy/lycscy.gb'
  },
  {
    name: 'palettely',
    path: 'palettely/palettely.gb'
  },
  {
    name: 'statcount-auto',
    path: 'statcount/statcount-auto.gb',
    customReferenceImage: true,
    cycles: 250 * CYCLES_PER_FRAME
  }
]

export function collectResults() {
  const promises = testCases.map((testCase) => {
    const { name, path, customReferenceImage, cycles } = testCase

    const testPath = `${__dirname}/../../src/roms/scribbltests/${path}`
    const diffPath = `${__dirname}/../results/${name}-diff.png`
    const resultPath = `${__dirname}/../results/${name}-result.png`
    const referencePath = `${__dirname}/../results/${name}-expected.png`

    if (!customReferenceImage) {
      const originalReferencePath = `${__dirname}/../../src/roms/scribbltests/${name}/screenshots/expected.png`
      fs.copyFileSync(originalReferencePath, referencePath)
    }

    return runVisualTest(
      testPath,
      cycles || 20 * CYCLES_PER_FRAME,
      resultPath,
      referencePath,
      diffPath,
      GREEN_COLORS
    ).then((testResult) => {
      return generateVisualResultRow(
        testCase.name,
        testResult,
        resultPath,
        referencePath,
        diffPath
      )
    })
  })

  return Promise.all(promises).then((results) => {
    return `
${generateVisualResultHeader(
  'scribbltests',
  'https://github.com/Hacktix/scribbltests'
)}
${results.join('\n')}
    `
  })
}

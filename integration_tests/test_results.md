# Test results

## Mooneye's test

https://github.com/Gekkio/mooneye-gb

| Test                                          | Result |
| --------------------------------------------- | ------ |
| acceptance/add_sp_e_timing.gb                 | ✅     |
| acceptance/boot_div-dmg0.gb                   | ✅     |
| acceptance/boot_hwio-dmg0.gb                  | ❌     |
| acceptance/boot_regs-dmg0.gb                  | ✅     |
| acceptance/call_cc_timing.gb                  | ✅     |
| acceptance/call_cc_timing2.gb                 | ✅     |
| acceptance/call_timing.gb                     | ✅     |
| acceptance/call_timing2.gb                    | ✅     |
| acceptance/di_timing-GS.gb                    | ✅     |
| acceptance/div_timing.gb                      | ✅     |
| acceptance/ei_sequence.gb                     | ✅     |
| acceptance/ei_timing.gb                       | ✅     |
| acceptance/halt_ime0_ei.gb                    | ✅     |
| acceptance/halt_ime0_nointr_timing.gb         | ✅     |
| acceptance/halt_ime1_timing.gb                | ✅     |
| acceptance/halt_ime1_timing2-GS.gb            | ✅     |
| acceptance/if_ie_registers.gb                 | ✅     |
| acceptance/intr_timing.gb                     | ✅     |
| acceptance/jp_cc_timing.gb                    | ✅     |
| acceptance/jp_timing.gb                       | ✅     |
| acceptance/ld_hl_sp_e_timing.gb               | ✅     |
| acceptance/oam_dma_restart.gb                 | ✅     |
| acceptance/oam_dma_start.gb                   | ✅     |
| acceptance/oam_dma_timing.gb                  | ✅     |
| acceptance/pop_timing.gb                      | ✅     |
| acceptance/push_timing.gb                     | ✅     |
| acceptance/rapid_di_ei.gb                     | ✅     |
| acceptance/ret_cc_timing.gb                   | ✅     |
| acceptance/ret_timing.gb                      | ✅     |
| acceptance/reti_intr_timing.gb                | ✅     |
| acceptance/reti_timing.gb                     | ✅     |
| acceptance/rst_timing.gb                      | ✅     |
| acceptance/bits/mem_oam.gb                    | ✅     |
| acceptance/bits/reg_f.gb                      | ✅     |
| acceptance/bits/unused_hwio-GS.gb             | ❌     |
| acceptance/instr/daa.gb                       | ✅     |
| acceptance/interrupts/ie_push.gb              | ✅     |
| acceptance/oam_dma/basic.gb                   | ✅     |
| acceptance/oam_dma/reg_read.gb                | ✅     |
| acceptance/oam_dma/sources-GS.gb              | ✅     |
| acceptance/ppu/hblank_ly_scx_timing-GS.gb     | ✅     |
| acceptance/ppu/intr_1_2_timing-GS.gb          | ✅     |
| acceptance/ppu/intr_2_0_timing.gb             | ✅     |
| acceptance/ppu/intr_2_mode0_timing.gb         | ✅     |
| acceptance/ppu/intr_2_mode0_timing_sprites.gb | ❌     |
| acceptance/ppu/intr_2_mode3_timing.gb         | ✅     |
| acceptance/ppu/intr_2_oam_ok_timing.gb        | ✅     |
| acceptance/ppu/lcdon_timing-GS.gb             | ✅     |
| acceptance/ppu/lcdon_write_timing-GS.gb       | ✅     |
| acceptance/ppu/stat_irq_blocking.gb           | ✅     |
| acceptance/ppu/stat_lyc_onoff.gb              | ✅     |
| acceptance/ppu/vblank_stat_intr-GS.gb         | ✅     |
| acceptance/timer/div_write.gb                 | ✅     |
| acceptance/timer/rapid_toggle.gb              | ✅     |
| acceptance/timer/tim00.gb                     | ✅     |
| acceptance/timer/tim00_div_trigger.gb         | ✅     |
| acceptance/timer/tim01.gb                     | ✅     |
| acceptance/timer/tim01_div_trigger.gb         | ✅     |
| acceptance/timer/tim10.gb                     | ✅     |
| acceptance/timer/tim10_div_trigger.gb         | ✅     |
| acceptance/timer/tim11.gb                     | ✅     |
| acceptance/timer/tim11_div_trigger.gb         | ✅     |
| acceptance/timer/tima_reload.gb               | ✅     |
| acceptance/timer/tima_write_reloading.gb      | ✅     |
| acceptance/timer/tma_write_reloading.gb       | ✅     |
| emulator-only/mbc1/bits_bank1.gb              | ✅     |
| emulator-only/mbc1/bits_bank2.gb              | ✅     |
| emulator-only/mbc1/bits_mode.gb               | ✅     |
| emulator-only/mbc1/bits_ramg.gb               | ✅     |
| emulator-only/mbc1/multicart_rom_8Mb.gb       | ❌     |
| emulator-only/mbc1/ram_256kb.gb               | ✅     |
| emulator-only/mbc1/ram_64kb.gb                | ✅     |
| emulator-only/mbc1/rom_16Mb.gb                | ✅     |
| emulator-only/mbc1/rom_1Mb.gb                 | ✅     |
| emulator-only/mbc1/rom_2Mb.gb                 | ✅     |
| emulator-only/mbc1/rom_4Mb.gb                 | ✅     |
| emulator-only/mbc1/rom_512kb.gb               | ✅     |
| emulator-only/mbc1/rom_8Mb.gb                 | ✅     |
| emulator-only/mbc2/bits_ramg.gb               | ✅     |
| emulator-only/mbc2/bits_romb.gb               | ✅     |
| emulator-only/mbc2/bits_unused.gb             | ✅     |
| emulator-only/mbc2/ram.gb                     | ✅     |
| emulator-only/mbc2/rom_1Mb.gb                 | ✅     |
| emulator-only/mbc2/rom_2Mb.gb                 | ✅     |
| emulator-only/mbc2/rom_512kb.gb               | ✅     |
| emulator-only/mbc5/rom_16Mb.gb                | ✅     |
| emulator-only/mbc5/rom_1Mb.gb                 | ✅     |
| emulator-only/mbc5/rom_2Mb.gb                 | ✅     |
| emulator-only/mbc5/rom_32Mb.gb                | ✅     |
| emulator-only/mbc5/rom_4Mb.gb                 | ✅     |
| emulator-only/mbc5/rom_512kb.gb               | ✅     |
| emulator-only/mbc5/rom_64Mb.gb                | ✅     |
| emulator-only/mbc5/rom_8Mb.gb                 | ✅     |

## dmg-acid2 test

https://github.com/mattcurrie/dmg-acid2

| Name      | Expected                            | Result                            | Diff                            | Status            |
| --------- | ----------------------------------- | --------------------------------- | ------------------------------- | ----------------- |
| dmg-acid2 | ![](results/dmg-acid2-expected.png) | ![](results/dmg-acid2-result.png) | ![](results/dmg-acid2-diff.png) | ✅ Diff: _0.0000_ |

## TurtleTests

https://github.com/Powerlated/TurtleTests

| Name                          | Expected                                                | Result                                                | Diff                                                | Status            |
| ----------------------------- | ------------------------------------------------------- | ----------------------------------------------------- | --------------------------------------------------- | ----------------- |
| window_y_trigger              | ![](results/window_y_trigger-expected.png)              | ![](results/window_y_trigger-result.png)              | ![](results/window_y_trigger-diff.png)              | ✅ Diff: _0.0000_ |
| window_y_trigger_wx_offscreen | ![](results/window_y_trigger_wx_offscreen-expected.png) | ![](results/window_y_trigger_wx_offscreen-result.png) | ![](results/window_y_trigger_wx_offscreen-diff.png) | ✅ Diff: _0.0000_ |

## scribbltests

https://github.com/Hacktix/scribbltests

| Name           | Expected                                 | Result                                 | Diff                                 | Status            |
| -------------- | ---------------------------------------- | -------------------------------------- | ------------------------------------ | ----------------- |
| scxly          | ![](results/scxly-expected.png)          | ![](results/scxly-result.png)          | ![](results/scxly-diff.png)          | ✅ Diff: _0.0000_ |
| lycscx         | ![](results/lycscx-expected.png)         | ![](results/lycscx-result.png)         | ![](results/lycscx-diff.png)         | ✅ Diff: _0.0000_ |
| lycscy         | ![](results/lycscy-expected.png)         | ![](results/lycscy-result.png)         | ![](results/lycscy-diff.png)         | ✅ Diff: _0.0000_ |
| palettely      | ![](results/palettely-expected.png)      | ![](results/palettely-result.png)      | ![](results/palettely-diff.png)      | ✅ Diff: _0.0000_ |
| statcount-auto | ![](results/statcount-auto-expected.png) | ![](results/statcount-auto-result.png) | ![](results/statcount-auto-diff.png) | ✅ Diff: _0.0000_ |

## Mealybug Tearoom Tests

https://github.com/mattcurrie/mealybug-tearoom-tests

| Name                              | Expected                                                             | Result                                                             | Diff                                                             | Status            |
| --------------------------------- | -------------------------------------------------------------------- | ------------------------------------------------------------------ | ---------------------------------------------------------------- | ----------------- |
| m2_win_en_toggle                  | ![](results/mealybug/m2_win_en_toggle-expected.png)                  | ![](results/mealybug/m2_win_en_toggle-result.png)                  | ![](results/mealybug/m2_win_en_toggle-diff.png)                  | ✅ Diff: _0.0000_ |
| m3_bgp_change                     | ![](results/mealybug/m3_bgp_change-expected.png)                     | ![](results/mealybug/m3_bgp_change-result.png)                     | ![](results/mealybug/m3_bgp_change-diff.png)                     | ❌ Diff: _0.1270_ |
| m3_bgp_change_sprites             | ![](results/mealybug/m3_bgp_change_sprites-expected.png)             | ![](results/mealybug/m3_bgp_change_sprites-result.png)             | ![](results/mealybug/m3_bgp_change_sprites-diff.png)             | ❌ Diff: _0.4056_ |
| m3_lcdc_bg_en_change              | ![](results/mealybug/m3_lcdc_bg_en_change-expected.png)              | ![](results/mealybug/m3_lcdc_bg_en_change-result.png)              | ![](results/mealybug/m3_lcdc_bg_en_change-diff.png)              | ❌ Diff: _0.1500_ |
| m3_lcdc_bg_map_change             | ![](results/mealybug/m3_lcdc_bg_map_change-expected.png)             | ![](results/mealybug/m3_lcdc_bg_map_change-result.png)             | ![](results/mealybug/m3_lcdc_bg_map_change-diff.png)             | ❌ Diff: _0.1036_ |
| m3_lcdc_obj_en_change             | ![](results/mealybug/m3_lcdc_obj_en_change-expected.png)             | ![](results/mealybug/m3_lcdc_obj_en_change-result.png)             | ![](results/mealybug/m3_lcdc_obj_en_change-diff.png)             | ❌ Diff: _0.0063_ |
| m3_lcdc_obj_en_change_variant     | ![](results/mealybug/m3_lcdc_obj_en_change_variant-expected.png)     | ![](results/mealybug/m3_lcdc_obj_en_change_variant-result.png)     | ![](results/mealybug/m3_lcdc_obj_en_change_variant-diff.png)     | ❌ Diff: _0.0579_ |
| m3_lcdc_obj_size_change           | ![](results/mealybug/m3_lcdc_obj_size_change-expected.png)           | ![](results/mealybug/m3_lcdc_obj_size_change-result.png)           | ![](results/mealybug/m3_lcdc_obj_size_change-diff.png)           | ❌ Diff: _0.0134_ |
| m3_lcdc_obj_size_change_scx       | ![](results/mealybug/m3_lcdc_obj_size_change_scx-expected.png)       | ![](results/mealybug/m3_lcdc_obj_size_change_scx-result.png)       | ![](results/mealybug/m3_lcdc_obj_size_change_scx-diff.png)       | ❌ Diff: _0.0082_ |
| m3_lcdc_tile_sel_change           | ![](results/mealybug/m3_lcdc_tile_sel_change-expected.png)           | ![](results/mealybug/m3_lcdc_tile_sel_change-result.png)           | ![](results/mealybug/m3_lcdc_tile_sel_change-diff.png)           | ❌ Diff: _0.1117_ |
| m3_lcdc_tile_sel_win_change       | ![](results/mealybug/m3_lcdc_tile_sel_win_change-expected.png)       | ![](results/mealybug/m3_lcdc_tile_sel_win_change-result.png)       | ![](results/mealybug/m3_lcdc_tile_sel_win_change-diff.png)       | ❌ Diff: _0.1190_ |
| m3_lcdc_win_en_change_multiple    | ![](results/mealybug/m3_lcdc_win_en_change_multiple-expected.png)    | ![](results/mealybug/m3_lcdc_win_en_change_multiple-result.png)    | ![](results/mealybug/m3_lcdc_win_en_change_multiple-diff.png)    | ❌ Diff: _0.3540_ |
| m3_lcdc_win_en_change_multiple_wx | ![](results/mealybug/m3_lcdc_win_en_change_multiple_wx-expected.png) | ![](results/mealybug/m3_lcdc_win_en_change_multiple_wx-result.png) | ![](results/mealybug/m3_lcdc_win_en_change_multiple_wx-diff.png) | ❌ Diff: _0.2155_ |
| m3_lcdc_win_map_change            | ![](results/mealybug/m3_lcdc_win_map_change-expected.png)            | ![](results/mealybug/m3_lcdc_win_map_change-result.png)            | ![](results/mealybug/m3_lcdc_win_map_change-diff.png)            | ❌ Diff: _0.0891_ |
| m3_obp0_change                    | ![](results/mealybug/m3_obp0_change-expected.png)                    | ![](results/mealybug/m3_obp0_change-result.png)                    | ![](results/mealybug/m3_obp0_change-diff.png)                    | ❌ Diff: _0.0180_ |
| m3_scx_high_5_bits                | ![](results/mealybug/m3_scx_high_5_bits-expected.png)                | ![](results/mealybug/m3_scx_high_5_bits-result.png)                | ![](results/mealybug/m3_scx_high_5_bits-diff.png)                | ❌ Diff: _0.2789_ |
| m3_scx_low_3_bits                 | ![](results/mealybug/m3_scx_low_3_bits-expected.png)                 | ![](results/mealybug/m3_scx_low_3_bits-result.png)                 | ![](results/mealybug/m3_scx_low_3_bits-diff.png)                 | ❌ Diff: _0.0234_ |
| m3_scy_change                     | ![](results/mealybug/m3_scy_change-expected.png)                     | ![](results/mealybug/m3_scy_change-result.png)                     | ![](results/mealybug/m3_scy_change-diff.png)                     | ❌ Diff: _0.3907_ |
| m3_window_timing                  | ![](results/mealybug/m3_window_timing-expected.png)                  | ![](results/mealybug/m3_window_timing-result.png)                  | ![](results/mealybug/m3_window_timing-diff.png)                  | ❌ Diff: _0.0289_ |
| m3_window_timing_wx_0             | ![](results/mealybug/m3_window_timing_wx_0-expected.png)             | ![](results/mealybug/m3_window_timing_wx_0-result.png)             | ![](results/mealybug/m3_window_timing_wx_0-diff.png)             | ❌ Diff: _0.0414_ |
| m3_wx_4_change                    | ![](results/mealybug/m3_wx_4_change-expected.png)                    | ![](results/mealybug/m3_wx_4_change-result.png)                    | ![](results/mealybug/m3_wx_4_change-diff.png)                    | ❌ Diff: _0.0099_ |
| m3_wx_4_change_sprites            | ![](results/mealybug/m3_wx_4_change_sprites-expected.png)            | ![](results/mealybug/m3_wx_4_change_sprites-result.png)            | ![](results/mealybug/m3_wx_4_change_sprites-diff.png)            | ❌ Diff: _0.0004_ |
| m3_wx_5_change                    | ![](results/mealybug/m3_wx_5_change-expected.png)                    | ![](results/mealybug/m3_wx_5_change-result.png)                    | ![](results/mealybug/m3_wx_5_change-diff.png)                    | ❌ Diff: _0.0277_ |
| m3_wx_6_change                    | ![](results/mealybug/m3_wx_6_change-expected.png)                    | ![](results/mealybug/m3_wx_6_change-result.png)                    | ![](results/mealybug/m3_wx_6_change-diff.png)                    | ❌ Diff: _0.5989_ |

Generated at 2020-12-09T10:12:31.687Z

import fs from 'fs'
import path from 'path'
import prettier from 'prettier'

import { initEnvironment } from './runners/runner_utils'
import { collectResults as collectMooneyeResults } from './runners/run_mooneye'
import { collectResults as collectAcid2Results } from './runners/run_acid2'
import { collectResults as collectTurtleTests } from './runners/run_turtletests'
import { collectResults as collectScribblTests } from './runners/run_scribbletests'
import { collectResults as collectMealybug } from './runners/run_mealybug'

initEnvironment()

function collectResults() {
  return Promise.all([
    collectMooneyeResults(),
    collectAcid2Results(),
    collectTurtleTests(),
    collectScribblTests(),
    collectMealybug()
  ]).then((results) => results.join('\n'))
}

collectResults().then((resultsMd) => {
  const md = `
  # Test results

  ${resultsMd}

  Generated at ${new Date().toISOString()}
  `
  fs.writeFileSync(
    path.join(__dirname, 'test_results.md'),
    prettier.format(md, { parser: 'markdown' })
  )
})

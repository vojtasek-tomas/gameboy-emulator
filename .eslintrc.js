// eslint-disable-next-line @typescript-eslint/no-var-requires
const prettierConfig = require('./.prettierrc')

module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  plugins: ['@typescript-eslint', 'react', 'import', 'jest', 'prettier'],
  env: {
    browser: true,
    node: true,
    jest: true,
    es6: true
  },
  extends: [
    'airbnb',
    'eslint:recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:react/recommended',
    'plugin:jest/recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'prettier/react',
    'prettier/@typescript-eslint'
  ],
  settings: {
    'import/extensions': ['.js', '.jsx', '.ts', '.tsx'],
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.json']
      }
    },
    react: {
      version: 'detect'
    }
  },
  rules: {
    'no-console': 0,
    'no-bitwise': 0,
    'no-plusplus': 0,
    'prefer-destructuring': 0,
    'no-continue': 0,
    'lines-between-class-members': 0,
    'class-methods-use-this': 0,
    '@typescript-eslint/prefer-interface': 0,
    '@typescript-eslint/ban-types': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/explicit-function-return-type': 0,
    '@typescript-eslint/camelcase': 0,
    '@typescript-eslint/ban-ts-ignore': 0,
    '@typescript-eslint/explicit-member-accessibility': 0,
    '@typescript-eslint/interface-name-prefix': 0,
    '@typescript-eslint/explicit-module-boundary-types': 0,
    '@typescript-eslint/ban-ts-comment': 0,
    'no-use-before-define': [0],
    '@typescript-eslint/no-use-before-define': [1],
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': ['error'],

    camelcase: 0,

    'react/jsx-filename-extension': [1, { extensions: ['.tsx', '.jsx'] }],
    'react/jsx-no-bind': 0,
    'react/destructuring-assignment': 0,
    'react/button-has-type': 0,
    'react/sort-comp': 0,
    'react/no-array-index-key': 0,
    'react/no-did-update-set-state': 0,
    'react/prefer-stateless-function': 0,
    'react/state-in-constructor': 0,
    'react/require-default-props': 0,
    'max-classes-per-file': 0,

    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: [
          '**/__tests__/*',
          './setupJest.ts',
          './webpack.config.js',
          '**/integration_tests/**/*'
        ]
      }
    ],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        tsx: 'never',
        ts: 'never'
      }
    ],
    'import/prefer-default-export': 0,
    'jsx-a11y/anchor-is-valid': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/label-has-for': 0,
    'jsx-a11y/no-autofocus': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/label-has-associated-control': 0,
    'prettier/prettier': [2, prettierConfig]
  }
}

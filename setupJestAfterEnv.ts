import { formatHex } from './src/lib/utils'

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace jest {
    interface Matchers<R> {
      toBeHex(expected: number): CustomMatcherResult
    }
  }
}

expect.extend({
  toBeHex(recieved: number, expected: number): jest.CustomMatcherResult {
    if (recieved !== expected) {
      return {
        pass: false,
        message: () =>
          `Expected: ${formatHex(expected)}, got: ${formatHex(recieved)}`
      }
    }
    return { pass: true, message: () => '' }
  }
})

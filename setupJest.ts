import 'jest-canvas-mock'

// Fail test on console error (react proptypes validation etc.)
// eslint-disable-next-line no-console
const consoleError = console.error
// eslint-disable-next-line no-console
console.error = (err: any, ...args: any) => {
  consoleError(err, ...args)
  throw new Error(err)
}

import { roms as testRoms } from './tests'

const requireContext = require.context('.', false, /games\.ts$/)
const modules = requireContext.keys().map((key) => requireContext(key))

const games = modules.length === 1 ? modules[0].roms : []

export const roms = [...games, ...testRoms]

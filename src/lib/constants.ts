export const MAX_8_BIT_VALUE = 0xff // 255
export const MAX_16_BIT_VALUE = 0xffff // 65535
export const MEMORY_SIZE = 0xffff + 1

export const DISPLAY_HEIGHT = 144
export const DISPLAY_WIDTH = 160

export const SCANLINES_PER_FRAME = 144

export const CPU_CLOCK_SPEED = 4194304 // Hz ~ 4 MHz
export const FPS = 59.7275
export const CYCLES_PER_FRAME = CPU_CLOCK_SPEED / FPS // = 70224

export const TILES_COUNT = 384
export const TILE_WIDTH = 8

// sprite height is 8 or 16, based on LCDC register
export const SPRITE_WIDTH = 8
export const SPRITES_COUNT = 40
export const MAX_SPRITES_PER_LINE = 10
// Size of object attribute memory in bytes
export const OAM_SIZE = SPRITES_COUNT * 4 // 4 bytes per sprite

export const ROM_BANK_SIZE = 0x4000 // 16 kb
export const RAM_BANK_SIZE = 0x2000 // 8 kb

// prettier-ignore
export const INTERRUPT_BITS = {
  V_BLANK:   0b0000_0001,
  LCD_STATS: 0b0000_0010,
  TIMER:     0b0000_0100,
  SERIAL:    0b0000_1000,
  JOYPAD:    0b0001_0000
}

// ISR - Interrupt Service Routine
// prettier-ignore
export const ISR_ADDRESSES = {
  V_BLANK:                    0x40,
  LCDC_STATUS:                0x48,
  TIMER_OVERFLOW:             0x50,
  SERIAL_TRANSFER_COMPLETION: 0x58,
  JOYPAD_PRESS:               0x60
}

// 0x104 to 0x133 the cartridge must present the following values:
export const CARTRIDGE_HEADER = [
  0xce,
  0xed,
  0x66,
  0x66,
  0xcc,
  0x0d,
  0x00,
  0x0b,
  0x03,
  0x73,
  0x00,
  0x83,
  0x00,
  0x0c,
  0x00,
  0x0d,
  0x00,
  0x08,
  0x11,
  0x1f,
  0x88,
  0x89,
  0x00,
  0x0e,
  0xdc,
  0xcc,
  0x6e,
  0xe6,
  0xdd,
  0xdd,
  0xd9,
  0x99,
  0xbb,
  0xbb,
  0x67,
  0x63,
  0x6e,
  0x0e,
  0xec,
  0xcc,
  0xdd,
  0xdc,
  0x99,
  0x9f,
  0xbb,
  0xb9,
  0x33,
  0x3e
]

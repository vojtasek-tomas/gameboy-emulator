import { bios } from './bios'
import { isBetween } from './utils'
import { MEMORY_SIZE } from './constants'
import { PPU } from './PPU'
import { Timers } from './Timers'
import { Joypad } from './Joypad'
import { Cartridge, IMMU } from './types'
import { InterruptController } from './InterruptController'
import { Serial } from './serial'
import { R_DMA } from './registers'
import { ROMOnly } from './cartridges/ROMOnly'

const BOOT_ADDR = 0xff50

/**
 * Memory mapping
 * 0x0000 -> 0xFFFF (65 536 locations)
 * --------------------------------------
 * 0x0000 -> 0x3FFF Catridge ROM, bank 0 - 16kb
 *    0x0000 -> 0x00FF BIOS - CPU starts here, removed after first run and becomes available
 *    0x0100 -> 0x014F Cartridge header - cartridge name, manufacturer etc.
 *    0x0150 -> 0x3FFF The rest of catridge data
 * 0x4000 -> 0x7FFF Catridge ROM, bank 1 - 16kb
 *
 * 0x8000 -> 0x9FFF Graphics RAM (VRAM)
 *    0x8000 -> 0x87FF  Tile set #1: tiles   0 ->  127
 *    0x8800 -> 0x8FFF  Tile set #1: tiles 128 ->  255
 *                      Tile set #0: tiles  -1 -> -128
 *    0x9000 -> 0x97FF  Tile set #0: tiles   0 ->  127
 *    0x9800 -> 0x9BFF  Tile map #0
 *    0x9C00 -> 0x9FFF  Tile map #1
 *
 * 0xA000 -> 0xBFFF Cartridge (External) RAM
 *
 * 0xC000 -> 0xDFFF Working RAM - 8kb
 * 0xE000 -> 0xFDFF Working RAM (shadow) exact copy of previous working RAM, due to the HW wiring
 *
 * 0xFE00	-> 0xFE9F Sprite attribute table (OAM)
 *
 * 0xFEA0 -> 0xFEFF Not Usable
 * 0xFF00 -> 0xFF7F I/O Registers
 * 0xFF80 -> 0xFFFE High RAM (HRAM)
 * 0xFFFF -> 0xFFFF Interrupts Enable Register (IE)
 */
export class MMU implements IMMU {
  memory: Uint8Array

  biosAvailable: boolean

  // Components
  ppu: PPU
  joypad: Joypad
  timers: Timers
  serial: Serial
  interruptController: InterruptController
  cartridge?: Cartridge
  ram: Uint8Array

  // OAM DMA stuff
  oamRequested = false
  oamTransferFromAddr = 0
  oamLastWritten = 0
  oamInProgress = false

  constructor(
    interruptController: InterruptController,
    ppu: PPU,
    timers: Timers,
    joypad: Joypad,
    serial: Serial,
    useBios = true
  ) {
    this.interruptController = interruptController
    this.ppu = ppu
    this.timers = timers
    this.joypad = joypad
    this.serial = serial
    this.biosAvailable = useBios
    this.ram = new Uint8Array(0xdfff - 0xc000 + 1)

    // TODO - only 0xFEA0 - 0xFFFE needed
    this.memory = new Uint8Array(MEMORY_SIZE).fill(0xff)
  }

  accepts = (address: number) => isBetween(address, 0x0000, 0xffff)

  loadCartridge(cartridge: Cartridge) {
    this.cartridge = cartridge
  }

  getDmaAddress() {
    // 0xfe00 -> 0xffff cannot be accessed by DMA and are mapped to work RAM
    if (isBetween(this.oamTransferFromAddr, 0xfe00, 0xffff)) {
      return this.oamTransferFromAddr - 0x2000
    }
    return this.oamTransferFromAddr
  }

  *tick() {
    // TODO move somewhere else and get rid of generators?
    while (true) {
      if (this.oamRequested) {
        this.oamRequested = false
        // Do the OAM transfer
        for (let address = 0; address < 160; address++) {
          let byte = this.readByte(this.getDmaAddress())
          yield

          if (this.oamRequested) {
            // Another OAM DMA has been requested
            this.oamRequested = false
            address = 0 // rollback
            byte = this.readByte(this.getDmaAddress())
            yield
          }
          this.oamInProgress = true
          // OAM starts at 0xfe00
          this.ppu.writeDMAByte(address | 0xfe00, byte)
          this.oamTransferFromAddr++
        }

        yield
        this.oamInProgress = false
      } else {
        yield
      }
    }
  }

  writeByte(address: number, value: number) {
    // Set to non-zero to disable boot ROM
    if (address === BOOT_ADDR) {
      if (this.biosAvailable && value) {
        this.biosAvailable = false
      }
      return
    }

    // Sprite attribute table (OAM)
    if (isBetween(address, 0xfe00, 0xfe9f)) {
      if (this.oamInProgress) {
        return
      }
      this.ppu.writeByte(address, value)
      return
    }

    if (address === R_DMA) {
      this.oamTransferFromAddr = value << 8
      this.oamRequested = true
      this.oamLastWritten = value
      return
    }

    if (this.interruptController.accepts(address)) {
      this.interruptController.writeByte(address, value)
      return
    }

    if (this.serial.accepts(address)) {
      this.serial.writeByte(address, value)
      return
    }

    if (this.ppu.accepts(address)) {
      this.ppu.writeByte(address, value)
      return
    }

    if (this.joypad.accepts(address)) {
      this.joypad.writeByte(address, value)
      return
    }

    if (this.timers.accepts(address)) {
      this.timers.writeByte(address, value)
      return
    }

    if (this.getCartridge().accepts(address)) {
      this.getCartridge().writeByte(address, value)
      return
    }

    // Working RAM + echo
    if (
      isBetween(address, 0xc000, 0xdfff) ||
      isBetween(address, 0xe000, 0xfdff)
    ) {
      this.ram[address & 0x1fff] = value
      return
    }

    // Not usable
    if (isBetween(address, 0xfea0, 0xfeff)) {
      return
    }

    this.memory[address] = value

    // if (address === 0xff01) {
    //   console.log(
    //     `[Serial output] Char=${String.fromCharCode(
    //       value
    //     )}, value=${value.toString(16)}`
    //   )
    // }
  }

  readByte(address: number): number {
    // Sprite attribute table (OAM)
    if (isBetween(address, 0xfe00, 0xfe9f)) {
      if (this.oamInProgress) {
        return 0xff
      }
      return this.ppu.readByte(address)
    }

    if (address === R_DMA) {
      return this.oamLastWritten
    }

    if (this.ppu.accepts(address)) {
      return this.ppu.readByte(address)
    }

    if (this.timers.accepts(address)) {
      return this.timers.readByte(address)
    }
    if (this.joypad.accepts(address)) {
      return this.joypad.readByte(address)
    }

    if (this.serial.accepts(address)) {
      return this.serial.readByte(address)
    }

    if (this.interruptController.accepts(address)) {
      return this.interruptController.readByte(address)
    }

    if (this.getCartridge().accepts(address)) {
      if (this.biosAvailable && address < bios.length) {
        return bios[address]
      }
      return this.getCartridge().readByte(address)
    }

    // Working RAM + echo
    if (
      isBetween(address, 0xc000, 0xdfff) ||
      isBetween(address, 0xe000, 0xfdff)
    ) {
      return this.ram[address & 0x1fff]
    }

    // Not usable
    if (isBetween(address, 0xfea0, 0xfeff)) {
      return 0xff
    }

    if (address === BOOT_ADDR) {
      return 0xff
    }

    return this.memory[address]
  }

  writeWord(address: number, value: number) {
    this.writeByte(address, value & 0xff)
    this.writeByte(address + 1, value >> 8)
  }

  readWord(address: number) {
    return (this.readByte(address + 1) << 8) + this.readByte(address)
  }

  getCartridge() {
    if (!this.cartridge) {
      return new ROMOnly(new Uint8Array(0x8000))
    }

    return this.cartridge
  }
}

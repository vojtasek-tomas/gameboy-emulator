export function getBit(number: number, bitIndex: number) {
  return (number & (1 << bitIndex)) === 0 ? 0 : 1
}

export function setBit(number: number, bitIndex: number) {
  return number | (1 << bitIndex)
}

export function clearBit(number: number, bitIndex: number) {
  const mask = ~(1 << bitIndex)
  return number & mask
}

export function updateBit(number: number, bitIndex: number, bitValue: 0 | 1) {
  const clearMask = ~(1 << bitIndex)
  return (number & clearMask) | (bitValue << bitIndex)
}

export function getSignedByte(byte: number) {
  // signed byte = <-128,127>
  // -128 = 1000 0000
  //   -1 = 1111 1111
  const hasSign = getBit(byte, 7)
  if (!hasSign) {
    return byte
  }

  // https://en.wikipedia.org/wiki/Two%27s_complement
  return -((~byte + 1) & 255)
}

export function getBytes(input: string): Uint8Array {
  return Uint8Array.from(input, (x) => x.charCodeAt(0))
}

export function clearBits(value: number, mask: number) {
  return value & ~mask
}

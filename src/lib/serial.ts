import { MemoryAccess } from './types'
import { createUnmappedMemoryError } from './errors'
import { InterruptController } from './InterruptController'

/**
 * Serial transfer data (R/W)
 * Before a transfer, it holds the next byte that will go out.
 *
 * During a transfer, it has a blend of the outgoing and incoming bytes. Each cycle, the leftmost bit is shifted out (and over the wire) and the incoming bit is shifted in from the other side:
 */
const R_SB = 0xff01

/**
 * Serial Transfer Control (R/W)
 *
 * Bit 7 - Transfer Start Flag (0=No transfer is in progress or requested, 1=Transfer in progress, or requested)
 * Bit 1 - Clock Speed (0=Normal, 1=Fast) ** CGB Mode Only **
 * Bit 0 - Shift Clock (0=External Clock, 1=Internal Clock)
 */
const R_SC = 0xff02

// prettier-ignore
// enum SC_BITS {
//   TRANSFER_START_FLAG   = 0b1000_0000,
//   CLOCK_SPEED           = 0b0000_0010,
//   SHIFT_CLOCK           = 0b0000_0001
// }

export class Serial implements MemoryAccess {
  sb = 0
  sc = 0

  interruptController: InterruptController

  constructor(interruptController: InterruptController) {
    this.interruptController = interruptController
  }

  accepts = (address: number) => [R_SB, R_SC].includes(address)

  tick = () => {
    // TODO fixme
  }

  readByte(address: number) {
    switch (address) {
      case R_SB:
        return this.sb
      case R_SC:
        return this.sc
      default:
        throw createUnmappedMemoryError('Serial', address)
    }
  }

  writeByte(address: number, value: number) {
    switch (address) {
      case R_SB:
        this.sb = value
        return
      case R_SC:
        this.sc = value | 0b0111_1110 // set unused bits
        return
      default:
        throw createUnmappedMemoryError('Serial', address)
    }
  }
}

import { MemoryAccess } from '../types'
import { isBetween, range } from '../utils'
import { TILES_COUNT, TILE_WIDTH } from '../constants'
import { getBit } from '../binaryUtils'
import { createUnmappedMemoryError } from '../errors'

export const VRAM_START = 0x8000
export const VRAM_END = 0x9fff
export const VRAM_SIZE = VRAM_END - VRAM_START + 1

const emptyRow = [0, 0, 0, 0, 0, 0, 0, 0]

const DATA_MASK = 0x1fff

type Tile = number[][]

export class Vram implements MemoryAccess {
  vram = new Uint8Array(VRAM_SIZE)

  // https://gbdev.io/pandocs/#vram-tile-data
  // 8x8 pixels, 384 tiles, 0x8000 -> 0x97ff
  tiles: Tile[] = range(0, TILES_COUNT).map(() =>
    range(0, 8).map(() => [...emptyRow])
  )

  accepts = (address: number) => isBetween(address, VRAM_START, VRAM_END)

  updateTile(address: number) {
    const index = address >> 4 // 16 bytes per tile
    const row = (address >> 1) & 7 // 2 bytes per line
    const baseAddress = address & 0xfffe // always recompute with odd line as the base line, ignore first bit

    this.tiles[index][row] = range(0, TILE_WIDTH)
      .map((x) => {
        const tileDataLow = this.vram[baseAddress]
        const tileDataHigh = this.vram[baseAddress + 1]

        const high = getBit(tileDataHigh, x)
        const low = getBit(tileDataLow, x)
        const pixel = (high << 1) | low
        return pixel
      })
      .reverse() // 7. bit == 0. pixel, 6. bit === 1. pixel...
  }

  readByte(address: number) {
    if (this.accepts(address)) {
      // 0x8001 -> 0x0001
      return this.vram[address & DATA_MASK]
    }
    throw createUnmappedMemoryError('VRAM', address)
  }

  writeByte(address: number, value: number) {
    if (this.accepts(address)) {
      // Tiles            0x8000 -> 0x97ff - store + transform pixels
      // Background maps  0x9800 -> 0x9fff - just store

      // 0x8001 -> 0x0001 etc
      this.vram[address & DATA_MASK] = value

      // Tiles
      if (isBetween(address, 0x8000, 0x97ff)) {
        // value not needed, updated is based on address (tile + line number)
        this.updateTile(address & DATA_MASK)
      }
      return
    }
    throw createUnmappedMemoryError('VRAM', address)
  }
}

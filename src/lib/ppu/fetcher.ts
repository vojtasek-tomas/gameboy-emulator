import type { PPU } from '../PPU'
import {
  getBackgroundTileMapAddress,
  getTileNumberTransformFn,
  getWindowTileMapAddress
} from './utils'

enum FetcherState {
  ReadTileNumber,

  ReadTileData0,
  ReadTileData1,
  PushToFIFO
}

type Pixel = [
  number, // R
  number, // G
  number // B
]

export enum FetcherMode {
  Background,
  Window
}

export class Fetcher {
  state = FetcherState.ReadTileNumber
  fifo: Pixel[] = []

  cycles = 0
  tileNumber = 0
  tileOffset = 0
  ppu?: PPU
  x = 0
  y = 0
  mode = FetcherMode.Background

  start(x: number, y: number, mode: FetcherMode) {
    this.x = x
    this.y = y
    this.tileNumber = 0
    this.tileOffset = 0
    this.state = FetcherState.ReadTileNumber
    this.cycles = 0
    this.fifo = []
    this.mode = mode
  }

  tick() {
    if (!this.ppu) {
      throw new Error('no ppu')
    }
    // The Fetcher runs at half the speed of the PPU (every 2 clock cycles).
    this.cycles++
    if (this.cycles < 2) {
      return
    }
    this.cycles = 0

    switch (this.state) {
      case FetcherState.ReadTileNumber:
        {
          const mapAddress =
            this.mode === FetcherMode.Window
              ? getWindowTileMapAddress(this.ppu.lcdc)
              : getBackgroundTileMapAddress(this.ppu.lcdc)

          const tileRow = Math.floor((this.y & 0xff) / 8) // first 8 lines are still the same tile (8x8 px)
          const tileCol = Math.floor(
            ((this.x + this.tileOffset * 8) & 0xff) / 8
          )
          const tileAddress = mapAddress + (tileRow * 32 + tileCol)
          const transformFn = getTileNumberTransformFn(this.ppu.lcdc)
          this.tileNumber = transformFn(this.ppu.vram.vram[tileAddress])

          this.state = FetcherState.ReadTileData0
        }
        break

      case FetcherState.ReadTileData0:
        // noop, we already stored zipped pixel
        this.state = FetcherState.ReadTileData1
        break

      case FetcherState.ReadTileData1:
        // noop, we already stored zipped pixel
        this.state = FetcherState.PushToFIFO
        break

      case FetcherState.PushToFIFO:
        // FIFO is fed with 8 pixels at once
        if (this.fifo.length <= 8) {
          const tile = this.ppu.vram.tiles[this.tileNumber]
          const linePixels = tile[this.y & 7]
          linePixels.forEach((pixel) => {
            // @ts-ignore TODO fixme
            this.fifo.push(pixel)
          })
          this.tileOffset++
          this.state = FetcherState.ReadTileNumber
        }
        break

      default:
        throw new Error('Unknown Fetcher state')
    }
  }
}

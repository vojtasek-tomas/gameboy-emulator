import { IDisplay } from '../../types'
import { DISPLAY_WIDTH, DISPLAY_HEIGHT } from '../../constants'
import { COLORS } from '../utils'

export class CanvasDisplay implements IDisplay {
  canvasContext: CanvasRenderingContext2D
  canvas: HTMLCanvasElement

  imageData: ImageData

  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas
    const context = canvas.getContext('2d')
    if (!context) {
      throw new Error('Cannot create canvas context')
    }
    this.canvasContext = context
    this.clear()
    this.imageData = context.getImageData(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT)
  }

  getImageData() {
    return this.imageData.data
  }

  draw() {
    this.canvasContext.putImageData(this.imageData, 0, 0)
  }

  clear() {
    this.canvasContext.fillStyle = `rgb(${COLORS[0].join(',')})`
    this.canvasContext.fillRect(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT)
  }
}

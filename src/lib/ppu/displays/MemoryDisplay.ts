import { IDisplay } from '../../types'
import { DISPLAY_WIDTH, DISPLAY_HEIGHT } from '../../constants'

export class MemoryDisplay implements IDisplay {
  imageData = new Uint8ClampedArray(DISPLAY_WIDTH * DISPLAY_HEIGHT * 4)
  getImageData() {
    return this.imageData
  }

  draw() {
    // no-op
  }

  clear() {
    this.imageData = new Uint8ClampedArray(DISPLAY_WIDTH * DISPLAY_HEIGHT * 4)
  }
}

import { DISPLAY_WIDTH } from '../constants'
import { getSignedByte } from '../binaryUtils'
import { LCDC_BITS } from '../bits'
import { Palette } from '../types'

export function isLcdEnabled(lcdc: number) {
  return lcdc & LCDC_BITS.LCD_ENABLED
}

export function getBackgroundTileMapAddress(lcdc: number) {
  return (lcdc & LCDC_BITS.BG_TILE_MAP ? 0x9c00 : 0x9800) & 0x1fff
}

export function getWindowTileMapAddress(lcdc: number) {
  return (lcdc & LCDC_BITS.WIN_TILE_MAP ? 0x9c00 : 0x9800) & 0x1fff
}

export function isBackgroundOrWindowEnabled(lcdc: number) {
  return lcdc & LCDC_BITS.BG_WIN_ENABLED
}

export function isWindowEnabled(lcdc: number) {
  return lcdc & LCDC_BITS.WIN_ENABLED
}

export function areSpritesEnabled(lcdc: number) {
  return lcdc & LCDC_BITS.OBJ_ENABLED
}

export function getSpriteHeight(lcdc: number) {
  return lcdc & LCDC_BITS.OBJ_SIZE ? 16 : 8
}

export function getCanvasOffset(x: number, y: number) {
  return (y * DISPLAY_WIDTH + x) * 4 // 4 = pixel definition [R,G,B,A]
}

const tileSet0Transform = (n: number) => getSignedByte(n) + 256
const tileSet1Transform = (n: number) => n

export function getTileNumberTransformFn(lcdc: number): (n: number) => number {
  const unsigned = !!(lcdc & LCDC_BITS.BG_WIN_TILE_DATA)
  /**
   * 8000-87FF  Tile set #1: tiles 0-127
   * 8800-8FFF  Tile set #1: tiles 128-255
   *            Tile set #0: tiles -1 to -128
   * 9000-97FF  Tile set #0: tiles 0-127
   */
  return unsigned
    ? tileSet1Transform // tile set #1
    : tileSet0Transform // tile set #0, offset is size of tile set = 256
}

// Black and white
export const COLORS: Palette = [
  [255, 255, 255], // 0b00
  [192, 192, 192], // 0b01
  [96, 96, 96], // 0b10
  [0, 0, 0] // 0b11
]

// Scribbltests green
export const GREEN_COLORS: Palette = [
  [224, 248, 208], // 0b00
  [136, 191, 112], // 0b01
  [52, 104, 86], // 0b10
  [9, 25, 33] // 0b11
]

// Green https://lospec.com/palette-list/nintendo-gameboy-bgb
// const COLORS: Palette = [
//   [224, 248, 208], // 0b00
//   [136, 192, 112], // 0b01
//   [52, 104, 86], // 0b10
//   [8, 24, 32] // 0b11
// ]

export function getPalette(systemPalette: Palette, register: number): Palette {
  // register BGB, OBP0 or OBP1
  const palette: Palette = [
    systemPalette[register & 0b11],
    systemPalette[(register >> 2) & 0b11],
    systemPalette[(register >> 4) & 0b11],
    systemPalette[(register >> 6) & 0b11]
  ]
  return palette
}

import { MemoryAccess, Sprite } from '../types'
import { range, isBetween, formatHex } from '../utils'
import { SPRITES_COUNT } from '../constants'
import { createUnmappedMemoryError } from '../errors'

export const OAM_START = 0xfe00
export const OAM_END = 0xfe9f
export const OAM_SIZE = OAM_END - OAM_START + 1

const DATA_MASK = 0xff

export class Oam implements MemoryAccess {
  oam = new Uint8Array(OAM_SIZE)

  sprites: Sprite[] = range(0, SPRITES_COUNT).map(() => ({
    x: 0,
    y: 0,
    tileNumber: 0,
    options: {
      isAboveBackground: false,
      isYFlipped: false,
      isXFlipped: false,
      palette: 0
    }
  }))

  accepts = (address: number) => isBetween(address, OAM_START, OAM_END)

  updateSprite(address: number, value: number) {
    const index = address >> 2 // 4 bytes per sprite
    if (index >= SPRITES_COUNT) {
      console.warn('Invalid OAM address', formatHex(address))
      return
    }
    switch (address & 0b11) {
      case 0:
        this.sprites[index].y = value - 16
        break
      case 1:
        this.sprites[index].x = value - 8
        break
      case 2:
        this.sprites[index].tileNumber = value
        break
      case 3:
        this.sprites[index].options = {
          palette: value & 0x10 ? 1 : 0,
          isXFlipped: !!(value & 0x20),
          isYFlipped: !!(value & 0x40),
          isAboveBackground: !(value & 0x80) // 0 above, 1 bellow (except color 0)
        }
        break
      default:
        throw new Error(`Unexpected sprites address = ${formatHex(address)}`)
    }
  }

  readByte(address: number) {
    if (this.accepts(address)) {
      // 0xFE01 -> 0x0001
      return this.oam[address & DATA_MASK]
    }
    throw createUnmappedMemoryError('OAM', address)
  }

  writeByte(address: number, value: number) {
    if (this.accepts(address)) {
      // 0xFE01 -> 0x0001
      this.oam[address & DATA_MASK] = value
      this.updateSprite(address & DATA_MASK, value)
      return
    }
    throw createUnmappedMemoryError('OAM', address)
  }
}

import { formatHex } from './utils'

export function createUnmappedMemoryError(section: string, address: number) {
  return new Error(`Unmapped address in ${section} at ${formatHex(address)}`)
}

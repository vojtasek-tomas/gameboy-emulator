// import { MemoryAccess } from './types'
// import { createUnmappedMemoryError } from './errors'
// import { isBetween } from './utils'

// /**
//  * FF10 - NR10 - Channel 1 Sweep register (R/W)
//  */
// const R_NR10 = 0xff10

// /**
//  * FF11 - NR11 - Channel 1 Sound length/Wave pattern duty (R/W)
//  */
// const R_NR11 = 0xff11

// /**
//  * FF12 - NR12 - Channel 1 Volume Envelope (R/W)
//  */
// const R_NR12 = 0xff12

// /**
//  * FF13 - NR13 - Channel 1 Frequency lo (Write Only)
//  */
// const R_NR13 = 0xff13

// /**
//  * FF14 - NR14 - Channel 1 Frequency hi (R/W)
//  */
// const R_NR14 = 0xff14

// /**
//  * FF1A - NR30 - Channel 3 Sound on/off (R/W)
//  */
// const R_NR30 = 0xff1a

// /**
//  * FF1B - NR31 - Channel 3 Sound Length
//  */
// const R_NR31 = 0xff1b

// /**
//  * FF1C - NR32 - Channel 3 Select output level (R/W)
//  */
// const R_NR32 = 0xff1c

// /**
//  * FF1D - NR33 - Channel 3 Frequency's lower data (W)
//  */
// const R_NR33 = 0xff1d

// /**
//  * FF1E - NR34 - Channel 3 Frequency's higher data (R/W)
//  */
// const R_NR34 = 0xff1e

// const CHANNEL_1_REGISTERS = [R_NR10, R_NR11, R_NR12, R_NR13, R_NR14]
// const CHANNEL_3_REGISTERS = [R_NR30, R_NR31, R_NR32, R_NR33, R_NR34]

// // prettier-ignore
// // NRx0
// enum SWEEP_BITS {
//   SWEEP_TIME              = 0b0111_0000,
//   SWEEP_INCREASE_DECREASE = 0b0000_1000,
//   SWEEP_SHIFT             = 0b0000_0011
// }

// // prettier-ignore
// // NRx1
// enum LENGTH_BITS {
//   WAVE_PATTERN_DUTY       = 0b1100_0000,
//   SOUND_LENGTH_DATA       = 0b0011_1111
// }

// // NRx2
// enum VOLUME_BITS {}

// // NRx3
// enum FREQUENCY_BITS {}

// // NRx4
// enum CONTROL_BITS {
//   ENABLED = 0b1000_0000
// }

// class Channel1 implements MemoryAccess {
//   sweepTime = 0
//   sweepIncrease = 0

//   accepts = (address: number) => CHANNEL_1_REGISTERS.includes(address)

//   readByte(address: number) {
//     return 0xff
//   }

//   writeByte(address: number, value: number) {
//     //
//   }
// }

// const WAVE_PATTERN_RAM_START = 0xff30
// const WAVE_PATTERN_RAM_END = 0xff3f
// const WAVE_PATTERN_RAM_SIZE = WAVE_PATTERN_RAM_END - WAVE_PATTERN_RAM_START + 1
// /**
//  * Wave
//  */
// class Channel3 implements MemoryAccess {
//   enabled = false

//   wavePatternRam: Uint8Array
//   frequency = 0

//   /**
//    * 0 - mute
//    * 1 - 100%
//    * 2 -  50%
//    * 3 -  25%
//    */
//   volume = 0

//   /**
//    * Sound Length = (256-t1)*(1/256) seconds
//    */
//   soundLength = 0

//   frequencyTimer = 0

//   constructor() {
//     this.wavePatternRam = new Uint8Array(WAVE_PATTERN_RAM_SIZE)
//   }

//   accepts = (address: number) =>
//     CHANNEL_3_REGISTERS.includes(address) ||
//     isBetween(WAVE_PATTERN_RAM_START, WAVE_PATTERN_RAM_END, address)

//   resetFrequencyTimer() {
//     this.frequencyTimer = (2048 - this.frequency) * 2
//   }

//   tick = () => {
//     if (this.frequency === 0) {
//       this.resetFrequencyTimer()
//     }
//   }

//   readByte(address: number) {
//     switch (address) {
//       case R_NR30:
//         return (Number(this.enabled) << 8) | 0b0111_1111
//       case R_NR31:
//         return this.soundLength
//       case R_NR32:
//         return this.volume << 5

//       case R_NR34:
//         return 0xff
//       default:
//         if (isBetween(WAVE_PATTERN_RAM_START, WAVE_PATTERN_RAM_END, address)) {
//           return this.wavePatternRam[address - WAVE_PATTERN_RAM_START]
//         }
//         throw createUnmappedMemoryError('APU Channel 3', address)
//     }
//   }

//   writeByte(address: number, value: number) {
//     switch (address) {
//       case R_NR30:
//         this.enabled = !!(value & CONTROL_BITS.ENABLED)
//         break
//       case R_NR32:
//         this.volume = (value & 0b0110_0000) >> 5
//         break
//       case R_NR33:
//         this.frequency = (this.frequency & 0b0111_000_000) | value
//         break
//       case R_NR34:
//         this.frequency = (this.frequency & 0xff) | ((value & 0b111) << 8)
//         break
//       default:
//         // Wave pattern RAM
//         if (isBetween(WAVE_PATTERN_RAM_START, WAVE_PATTERN_RAM_END, address)) {
//           this.wavePatternRam[address - WAVE_PATTERN_RAM_START] = value
//           return
//         }
//         throw createUnmappedMemoryError('APU Channel 3', address)
//     }
//   }
// }

// export class APU implements MemoryAccess {
//   channel1: Channel1
//   channel3: Channel3

//   constructor() {
//     this.channel1 = new Channel1()
//     this.channel3 = new Channel3()
//   }

//   accepts = (address: number) =>
//     CHANNEL_1_REGISTERS.includes(address) || this.channel3.accepts(address)

//   readByte(address: number) {
//     if (CHANNEL_1_REGISTERS.includes(address)) {
//       return this.channel1.readByte(address)
//     }
//     if (CHANNEL_3_REGISTERS.includes(address)) {
//       return this.channel3.readByte(address)
//     }
//     throw createUnmappedMemoryError('APU', address)
//   }

//   writeByte(address: number, value: number) {
//     if (CHANNEL_1_REGISTERS.includes(address)) {
//       this.channel1.writeByte(address, value)
//       return
//     }
//     if (CHANNEL_3_REGISTERS.includes(address)) {
//       this.channel3.writeByte(address, value)
//       return
//     }
//     throw createUnmappedMemoryError('APU', address)
//   }
// }

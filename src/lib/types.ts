import type { InterruptController } from './InterruptController'

type UpperBasicRegisters = 'A' | 'B' | 'D' | 'H'
type LowerBasicRegisters = 'F' | 'C' | 'E' | 'L'
export type SimpleRegisters = UpperBasicRegisters | LowerBasicRegisters

export type DualRegisters = 'AF' | 'BC' | 'DE' | 'HL' | 'SP'

export type RegisterNames = SimpleRegisters | DualRegisters

export type Flags = 'Z' | 'N' | 'H' | 'C'

export interface ICpu {
  get: (register: RegisterNames) => number
  set: (register: RegisterNames, value: number) => this
  increment16Bit: (
    register: 'stackPointer' | 'programCounter',
    value: number
  ) => this
  incrementPC: (value?: number) => this
  setPC: (value: number) => this
  getPC: () => number
  incrementSP: (value?: number) => this
  setSP: (value: number) => this
  getSP: () => number
  setFlag: (flag: Flags) => this
  clearFlag: (flag: Flags) => this
  getFlag: (flag: Flags) => number
  clearFlags: () => this
  ime: boolean
  halted: boolean
  justHalted: boolean
  haltBug: boolean
  imeRequested: boolean
}

type ReadFn = (address: number) => number
type WriteFn = (address: number, value: number) => void

export type IMMU = MemoryAccess & {
  readWord: ReadFn
  writeWord: WriteFn
  loadCartridge: (cartridge: Cartridge) => void
}

export interface IDisplay {
  draw: () => void
  getImageData: () => Uint8ClampedArray
  clear: () => void
}

export type CommandDefinition = [string, Instruction]

export interface MemoryAccess {
  readByte: ReadFn
  writeByte: WriteFn
  accepts: (address: number) => boolean
}

export type Cartridge = MemoryAccess

export type CartridgeMeta = {
  title: string
  cartridgeType: number
  romType: number
  romSize: number
  ramType: number
  ramSize: number
}

export interface TickableEmulator {
  readU8Tick: () => number
  readU16Tick: () => number
  readByteTick: ReadFn
  readWordTick: ReadFn
  writeByteTick: WriteFn
  writeWordTick: WriteFn
  cpu: ICpu
  tick: () => void
  cyclesCount: number
  interruptController: InterruptController
}

export type Instruction = (e: TickableEmulator) => void

export enum DMAState {
  INACTIVE,
  TRANSFER,
  TRIGGERED,
  RESTART
}

export type Sprite = {
  y: number // 0. byte
  x: number // 1. byte
  tileNumber: number // 2.byte
  options: {
    // 3. byte
    isAboveBackground: boolean // 7. bit
    isYFlipped: boolean // 6. bit
    isXFlipped: boolean // 5. bit
    palette: 0 | 1 // 4. bit
  }
}

export type RGBDefinition = [number, number, number]
export type Palette = [
  RGBDefinition,
  RGBDefinition,
  RGBDefinition,
  RGBDefinition
]

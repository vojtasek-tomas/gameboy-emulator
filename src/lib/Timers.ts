import { CPU_CLOCK_SPEED, INTERRUPT_BITS } from './constants'
import { InterruptController } from './InterruptController'
import { createUnmappedMemoryError } from './errors'
import { MemoryAccess } from './types'

export const MAX_DIVIDER = 16384

/**
 * Divider Register
 *
 * This register is incremented 16384 (~16779 on SGB) times a second. Writing any value sets it to $00.
 */
export const R_DIV = 0xff04

/**
 * Timer counter
 *
 * This timer is incremented by a clock frequency specified by the TAC register ($FF07). The timer generates an interrupt when it overflows.
 */
const R_TIMA = 0xff05

/**
 * Timer Modulo (R/W)
 *
 * When the TIMA overflows, this data will be loaded.
 */
const R_TMA = 0xff06

/**
 * Timer Control (R/W)
 *
 * Bits 3-7 unused = always 1
 * Bit 2 - Timer Stop
 *    0: Stop Timer
 *    1: Start Timer
 *
 * Bits 1+0 - Input Clock Select
 *    00: 4.096 KHz (~4.194 KHz SGB)
 *    01: 262.144 Khz (~268.4 KHz SGB)
 *    10: 65.536 KHz (~67.11 KHz SGB)
 *    11: 16.384 KHz (~16.78 KHz SGB)
 */
const R_TAC = 0xff07

// cycles needed to increment the counter
// CPU 4 MHz / timer frequency = # cycles
const TIMERS: { [bits: number]: number } = {
  0b00: CPU_CLOCK_SPEED / 4_096, // every 1024 cycles
  0b01: CPU_CLOCK_SPEED / 262_144, // every   16 cycles
  0b10: CPU_CLOCK_SPEED / 65_536, // every   64 cycles
  0b11: CPU_CLOCK_SPEED / 16_384 // every  256 cycles
}

// TIMA is incremented when the given bit of divCounter changes from 1 to 0.
const TIMERS_EDGE: { [bits: number]: number } = {
  0b00: TIMERS[0b00] / 2,
  0b01: TIMERS[0b01] / 2,
  0b10: TIMERS[0b10] / 2,
  0b11: TIMERS[0b11] / 2
}

const timersRegisters = [R_TMA, R_TIMA, R_TAC, R_DIV]

const TAC_UNUSED_MASK = 0b1111_1000

export class Timers implements MemoryAccess {
  /**
   * DIV (0xff04) is actually upper 8 bits of internal counter of 16bit register
   * Counts up at a frequency of 16382 Hz -> every 256 CPU clock cycles the divider register needs to increment
   * CPU 4 MHz / 16382 Hz = 256 cycles
   */
  dividerCounter = 0

  tima = 0
  tac = 0
  tma = 0

  loadingDelayClocks = 0

  interruptController: InterruptController

  constructor(interruptController: InterruptController) {
    this.interruptController = interruptController
  }

  accepts = (address: number) => timersRegisters.includes(address)

  getFrequency() {
    return TIMERS[this.tac & 0b11]
  }

  getFrequencyEdge() {
    return TIMERS_EDGE[this.tac & 0b11]
  }

  isEnabled() {
    return !!(this.tac & 0b0100)
  }

  hasCounterBit() {
    if (!this.isEnabled()) {
      return false
    }
    return (this.dividerCounter & this.getFrequencyEdge()) > 0
  }

  tick() {
    const prevCounterBit = this.hasCounterBit()
    this.dividerCounter = (this.dividerCounter + 1) & 0xffff

    if (this.loadingDelayClocks >= 0) {
      this.loadingDelayClocks--
    }
    if (this.loadingDelayClocks === 0) {
      this.tima = this.tma
    }

    if (!this.isEnabled()) {
      return
    }

    // Timer is incremented on falling edge (on change from 1 to 0)
    if (prevCounterBit && !this.hasCounterBit()) {
      this.incrementTima()
    }
  }

  incrementTima() {
    this.tima = (this.tima + 1) & 0xff
    if (this.tima === 0) {
      // TIMA loading has delay but interrupt is triggered right away
      this.loadingDelayClocks = 4
      this.interruptController.requestInterrupt(INTERRUPT_BITS.TIMER)
    }
  }

  readByte(address: number): number {
    switch (address) {
      case R_DIV:
        return this.dividerCounter >> 8
      case R_TIMA:
        return this.tima
      case R_TMA:
        return this.tma
      case R_TAC:
        return this.tac | TAC_UNUSED_MASK // unused mask
      default:
        throw createUnmappedMemoryError('Timers', address)
    }
  }

  writeByte(address: number, value: number) {
    switch (address) {
      case R_DIV: {
        // https://gbdev.io/pandocs/Timer_Obscure_Behaviour.html
        if (this.hasCounterBit()) {
          this.incrementTima()
        }

        this.dividerCounter = 0
        return
      }
      case R_TIMA:
        // Ignore writes while TMA is loading to TIMA
        if (this.loadingDelayClocks !== 0) {
          this.tima = value
          // Abort interrupt and TMA load
          this.loadingDelayClocks = -1
        }

        return
      case R_TMA:
        this.tma = value
        // TIMA is also changed on the cycle TMA is loaded
        if (this.loadingDelayClocks === 0) {
          this.tima = value
        }
        return
      case R_TAC: {
        const prevCounterBit = this.hasCounterBit()
        this.tac = (value & 0b111) | TAC_UNUSED_MASK // remaining/unused bits are always 1

        // TIMA also incremented on falling edge
        if (prevCounterBit && !this.hasCounterBit()) {
          this.incrementTima()
        }

        return
      }
      default:
        throw createUnmappedMemoryError('Timers', address)
    }
  }
}

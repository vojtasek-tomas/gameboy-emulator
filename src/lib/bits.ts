// prettier-ignore
export enum LCDC_BITS {
  LCD_ENABLED       = 0b1000_0000, // 7.
  WIN_TILE_MAP      = 0b0100_0000, // 6.
  WIN_ENABLED       = 0b0010_0000, // 5.
  BG_WIN_TILE_DATA  = 0b0001_0000, // 4.

  BG_TILE_MAP       = 0b0000_1000, // 3.
  OBJ_SIZE          = 0b0000_0100, // 2.
  OBJ_ENABLED       = 0b0000_0010, // 1.
  BG_WIN_ENABLED    = 0b0000_0001, // 0.
}

// prettier-ignore
export enum STAT_BITS {
  // unused // 7.
  LYC_EQUALS_LY_INTERRUPT   = 0b0100_0000, // 6.
  OAM_INTERRUPT             = 0b0010_0000, // 5.
  V_BLANK_INTERRUPT         = 0b0001_0000, // 4.

  H_BLANK_INTERRUPT         = 0b0000_1000, // 3.
  LYC_EQUALS_LY_FLAG        = 0b0000_0100, // 2.
  MODE                      = 0b0000_0011, // 0. - 1.
}

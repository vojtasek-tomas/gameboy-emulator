import { MemoryAccess, RGBDefinition, IDisplay, Palette, Sprite } from './types'
import {
  DISPLAY_WIDTH,
  DISPLAY_HEIGHT,
  MAX_SPRITES_PER_LINE,
  TILE_WIDTH,
  SPRITE_WIDTH,
  INTERRUPT_BITS
} from './constants'
import { InterruptController } from './InterruptController'
import { createUnmappedMemoryError } from './errors'
import { Fetcher, FetcherMode } from './ppu/fetcher'
import {
  getCanvasOffset,
  isBackgroundOrWindowEnabled,
  isWindowEnabled,
  areSpritesEnabled,
  getSpriteHeight,
  getPalette,
  isLcdEnabled,
  COLORS
} from './ppu/utils'
import { STAT_BITS } from './bits'
import { Vram } from './ppu/vram'
import { Oam } from './ppu/oam'
import {
  R_LCDC,
  R_STAT,
  R_SCY,
  R_SCX,
  R_LY,
  R_LYC,
  R_BGP,
  R_WX,
  R_WY,
  R_OBP0,
  R_OBP1
} from './registers'
import { clearBits } from './binaryUtils'

export enum LCD_MODES {
  H_BLANK = 0b00,
  V_BLANK = 0b01,
  OAM_SEARCH = 0b10,
  PIXEL_TRANSFER = 0b11
}

export enum LCD_STATE {
  H_BLANK,
  V_BLANK,
  OAM_SEARCH,
  PIXEL_TRANSFER,
  VOID
}

const gpuRegisters = [
  R_LCDC,
  R_STAT,
  R_SCY,
  R_SCX,
  R_LY,
  R_LYC,
  R_BGP,
  R_WX,
  R_WY,
  R_OBP0,
  R_OBP1
]

const STAT_UNUSED_MASK = 0b1000_0000
const MIN_FIFO_LENGTH = 8

enum Access {
  READ,
  WRITE
}

const TOTAL_LINE_CLOCKS = 456

export class PPU implements MemoryAccess {
  // Registers
  stat = 0
  lcdc = 0
  ly = 0
  lyc = 0
  scx = 0
  scy = 0
  wx = 0
  wy = 0
  bgp = 0
  obp0 = 0
  obp1 = 0

  lyToCompare = 0

  // Memory
  vram: Vram // 0x8000 -> 0x9FFF Video RAM (VRAM)
  oam: Oam // 0xFE00 -> FE9F Object Attribute Memory (OAM)

  display: IDisplay
  palette: Palette

  interruptController: InterruptController

  windowX = 0
  windowLineTrigger = false
  skipFrames = 0 // enable LCD -> skip first frame

  x = 0
  droppedPixels = 0
  fetcher = new Fetcher()
  windowLine = 0
  lineClocks = 0
  currentSCX = 0
  previousStatFlag: number | boolean = 0

  // Internal state to track current line and mode since the real values are delayed
  state = {
    mode: LCD_STATE.H_BLANK,
    line: 0
  }

  pendingMode: LCD_MODES | null = null
  pendingInterrupt: number | null = null

  constructor(
    interruptController: InterruptController,
    display: IDisplay,
    palette: Palette = COLORS
  ) {
    this.interruptController = interruptController
    this.display = display
    this.palette = palette

    this.vram = new Vram()
    this.oam = new Oam()
    this.fetcher.ppu = this
  }

  accepts = (address: number) =>
    gpuRegisters.includes(address) ||
    this.vram.accepts(address) ||
    this.oam.accepts(address)

  getMode() {
    return this.stat & 0b11
  }

  checkWindowTrigger = () => {
    // window is enabled for current line if WY=LY
    if (this.wy === this.ly) {
      this.windowLineTrigger = true
    }
  }

  tick() {
    if (!isLcdEnabled(this.lcdc)) {
      this.lineClocks = 0
      return
    }
    // this.lineClocks += 4
    this.lineClocks++

    // Mode change is delayed
    if (this.pendingMode !== null) {
      this.changeMode(this.pendingMode)
      this.statUpdate(this.pendingMode)
      this.pendingMode = null
    }

    if (this.state.line < 144) {
      this.screenLoop()
    } else {
      this.vblankLoop()
    }
  }

  screenLoop() {
    // 2   -> 3              -> 0
    // OAM -> PIXEL TRANSFER -> H-BLANK

    // There is no delay for line to compare at line 0
    if (this.lineClocks === 0) {
      if (this.state.line !== 0) {
        this.lyToCompare = -1
      } else {
        this.lyToCompare = 0
      }
    }

    // Line to compare is 4 cycles late
    if (this.lineClocks === 4) {
      this.lyToCompare = this.ly
      this.checkLyEqualsLyc() // TODO lyc=ly should not be checked at 0 line?
    }

    switch (this.state.mode) {
      case LCD_STATE.OAM_SEARCH:
      case LCD_STATE.VOID:
        this.checkWindowTrigger()

        // Exit
        if (this.lineClocks === 80) {
          this.state.mode = LCD_STATE.PIXEL_TRANSFER
          this.pendingMode = LCD_MODES.PIXEL_TRANSFER
          this.initPixelTransfer()
        }
        break

      case LCD_STATE.PIXEL_TRANSFER:
        this.processPixelTransfer()

        if (this.x === DISPLAY_WIDTH) {
          this.state.mode = LCD_STATE.H_BLANK
          this.pendingMode = LCD_MODES.H_BLANK

          if (areSpritesEnabled(this.lcdc)) {
            this.renderSprites() // TODO convert to pixel fetcher
          }
        }
        break

      case LCD_STATE.H_BLANK:
        if (this.lineClocks === TOTAL_LINE_CLOCKS) {
          this.lineClocks = 0
          this.ly++
          this.state.line++
          this.lyToCompare = -1
          this.stat = clearBits(this.stat, STAT_BITS.LYC_EQUALS_LY_FLAG)

          if (this.ly === DISPLAY_HEIGHT) {
            this.state.mode = LCD_STATE.V_BLANK
            this.pendingMode = LCD_MODES.V_BLANK
            this.windowLineTrigger = false
            if (this.skipFrames === 0) {
              this.display.draw()
            }
          } else {
            this.state.mode = LCD_STATE.OAM_SEARCH
            this.pendingMode = LCD_MODES.OAM_SEARCH
          }
        }
        break

      default:
        break
    }
  }

  vblankLoop() {
    // Lines 144 - 152
    if (this.state.line < 153) {
      if (this.lineClocks === 0) {
        this.lyToCompare = -1
      }

      if (this.lineClocks === 4) {
        this.lyToCompare = this.ly
        this.checkLyEqualsLyc()

        if (this.state.line === 144) {
          this.interruptController.requestInterrupt(INTERRUPT_BITS.V_BLANK)
          this.statUpdate(LCD_MODES.OAM_SEARCH)
        }
      }

      if (this.lineClocks === TOTAL_LINE_CLOCKS) {
        this.lineClocks = 0
        this.ly++
        this.state.line++
        this.lyToCompare = -1
        this.stat = clearBits(this.stat, STAT_BITS.LYC_EQUALS_LY_FLAG)
      }
      this.statUpdate(LCD_MODES.V_BLANK)
      return
    }

    // Line 153
    if (this.state.line === 153) {
      if (this.lineClocks === 0) {
        this.lyToCompare = -1
      }

      if (this.lineClocks === 4) {
        this.lyToCompare = 153
        this.checkLyEqualsLyc()
        this.ly = 0
      }

      if (this.lineClocks === 8) {
        this.lyToCompare = -1
      }

      if (this.lineClocks === 12) {
        this.lyToCompare = 0
        this.checkLyEqualsLyc()
      }

      // End of frame -> reset
      if (this.lineClocks === TOTAL_LINE_CLOCKS) {
        this.state.mode = LCD_STATE.OAM_SEARCH
        this.pendingMode = LCD_MODES.OAM_SEARCH
        if (this.skipFrames > 0) {
          this.skipFrames--
        }
        this.windowLine = 0
        this.ly = 0
        this.lyToCompare = 0
        this.state.line = 0
        this.lineClocks = 0
      }
    }
  }

  initPixelTransfer() {
    this.x = 0
    // TODO
    this.currentSCX = this.scx

    // Fetcher starts with background rendering
    // TOOD really?
    const y = (this.ly + this.scy) & 0xff
    this.fetcher.fifo = []
    this.fetcher.start(this.currentSCX, y, FetcherMode.Background)
    this.droppedPixels = 0
    this.windowX = this.wx - 7
  }

  processPixelTransfer() {
    this.fetcher.tick()
    // FIFO needs (always) at least 8 pixels to start drawing
    if (this.fetcher.fifo.length <= MIN_FIFO_LENGTH) {
      return
    }

    if (isBackgroundOrWindowEnabled(this.lcdc)) {
      // TODO check this - this might be true even if bg&win is disabled
      // discard pixels from tile that are not visible due to X scroll
      if (this.droppedPixels < this.currentSCX % TILE_WIDTH) {
        this.droppedPixels++
        this.fetcher.fifo.shift()
        return
      }

      // WX is between <0,6> - discard previous fetched pixels
      // TODO WX=0 is probably broken somehow https://discord.com/channels/465585922579103744/465586075830845475/786173202211799051
      if (this.fetcher.mode === FetcherMode.Window && this.windowX < 0) {
        this.windowX++
        this.fetcher.fifo.shift()
        return
      }

      // Switch to window fetching - discard FIFO and start again
      const windowPossible =
        this.windowLineTrigger && this.x === Math.max(0, this.wx - 7)
      if (
        isBackgroundOrWindowEnabled(this.lcdc) &&
        isWindowEnabled(this.lcdc) &&
        windowPossible &&
        this.fetcher.mode !== FetcherMode.Window
      ) {
        const x = (this.x - this.wx + 7) & 0xff
        const y = this.windowLine
        this.windowLine++
        this.fetcher.start(x, y, FetcherMode.Window)
        return
      }
    }

    if (isBackgroundOrWindowEnabled(this.lcdc)) {
      const pixel = this.fetcher.fifo.shift()
      if (pixel === undefined) {
        throw new Error('Trying to shift out pixels from empty FIFO!')
      }
      const palette = getPalette(this.palette, this.bgp)
      // @ts-ignore
      const color = palette[pixel]
      this.renderPixel(color)
    } else {
      // TODO is this necessary?
      this.fetcher.fifo.shift()
      const palette = getPalette(this.palette, this.bgp)
      const color = palette[0]
      this.renderPixel(color)
    }

    this.x++
  }

  checkLyEqualsLyc(shouldTrigger = true) {
    if (this.lyc === this.lyToCompare) {
      this.stat |= STAT_BITS.LYC_EQUALS_LY_FLAG
    } else {
      this.stat = clearBits(this.stat, STAT_BITS.LYC_EQUALS_LY_FLAG)
    }
    if (shouldTrigger) {
      this.statUpdate(this.getMode())
    }
  }

  statUpdate(mode: number) {
    if (!isLcdEnabled(this.lcdc)) {
      return
    }

    const oamIntr =
      mode === LCD_MODES.OAM_SEARCH && this.stat & STAT_BITS.OAM_INTERRUPT
    const hblankIntr =
      mode === LCD_MODES.H_BLANK && this.stat & STAT_BITS.H_BLANK_INTERRUPT
    const vblankIntr =
      mode === LCD_MODES.V_BLANK && this.stat & STAT_BITS.V_BLANK_INTERRUPT
    const lyEqualsLycIntr =
      this.stat & STAT_BITS.LYC_EQUALS_LY_INTERRUPT &&
      this.stat & STAT_BITS.LYC_EQUALS_LY_FLAG

    const statFlag = oamIntr || hblankIntr || vblankIntr || lyEqualsLycIntr

    if (!this.previousStatFlag && statFlag) {
      this.interruptController.requestInterrupt(INTERRUPT_BITS.LCD_STATS)
    }
    this.previousStatFlag = statFlag
  }

  changeMode(mode: number) {
    this.stat = clearBits(this.stat, STAT_BITS.MODE) | mode
  }

  renderSprite = (sprite: Sprite) => {
    const spriteHeight = getSpriteHeight(this.lcdc)

    const palette = getPalette(
      this.palette,
      sprite.options.palette === 0 ? this.obp0 : this.obp1
    )

    // Sprite line
    const y = sprite.options.isYFlipped
      ? spriteHeight - 1 - (this.ly - sprite.y)
      : this.ly - sprite.y

    for (
      let { x } = sprite;
      x < DISPLAY_WIDTH && x < sprite.x + SPRITE_WIDTH;
      x++
    ) {
      if (x < 0) {
        continue
      }

      const extraOffset = (y >> 3) & 1 // double sprite height -> take next tile
      let tileNumber = sprite.tileNumber
      // bit 0 of the tile index is ignored for 8x16 objects
      if (spriteHeight === 16) {
        tileNumber &= 0xfe
      }
      const tile = this.vram.tiles[tileNumber + extraOffset]

      const xIndex = x - sprite.x

      const tileRow = y
      const tileCol = sprite.options.isXFlipped ? 7 - xIndex : xIndex
      // & 7 - double sprite height
      const pixel = tile[tileRow & 7][tileCol]

      // 0 is always transparent
      if (pixel === 0) {
        continue
      }

      const shade = palette[pixel]
      const canvasOffset = getCanvasOffset(x, this.ly)
      const imageData = this.display.getImageData()

      // Behind background
      if (!sprite.options.isAboveBackground) {
        // Sprite is below background but the background color is not transparent -> skip
        const bgPal = getPalette(this.palette, this.bgp)
        if (
          imageData[canvasOffset] !== bgPal[0][0] &&
          imageData[canvasOffset + 1] !== bgPal[0][1] &&
          imageData[canvasOffset + 2] !== bgPal[0][2]
        ) {
          continue
        }
      }

      imageData[canvasOffset] = shade[0]
      imageData[canvasOffset + 1] = shade[1]
      imageData[canvasOffset + 2] = shade[2]
      imageData[canvasOffset + 3] = 255 // alpha, always 255
    }
  }

  getVisibleSprites() {
    const spriteHeight = getSpriteHeight(this.lcdc)

    const possibleSprites = this.oam.sprites.filter((sprite) => {
      return sprite.y <= this.ly && this.ly < sprite.y + spriteHeight
    })

    const visibleSprites = possibleSprites
      .sort((s1, s2) => {
        if (s1.x < s2.x) {
          return -1
        }
        if (s1.x > s2.x) {
          return 1
        }
        return 0
      }) // sorted by x
      .slice(0, MAX_SPRITES_PER_LINE) // first 10 sprites
      .reverse()
    return visibleSprites
  }

  renderSprites() {
    const visibleSprites = this.getVisibleSprites()
    visibleSprites.forEach(this.renderSprite)
  }

  renderPixel = (pixel: RGBDefinition) => {
    const canvasOffset = getCanvasOffset(this.x, this.ly)
    const imageData = this.display.getImageData()
    imageData[canvasOffset] = pixel[0]
    imageData[canvasOffset + 1] = pixel[1]
    imageData[canvasOffset + 2] = pixel[2]
    imageData[canvasOffset + 3] = 255 // alpha, always 255
  }

  canAccessOAM(access: Access) {
    if (access === Access.READ) {
      if (
        this.getMode() === LCD_MODES.PIXEL_TRANSFER ||
        this.getMode() === LCD_MODES.OAM_SEARCH
      ) {
        return false
      }

      if (this.pendingMode === LCD_MODES.OAM_SEARCH) {
        return false
      }
    }

    const isOamAccess =
      this.getMode() === LCD_MODES.OAM_SEARCH && this.lineClocks <= 76
    const isPixelTransfer = this.getMode() === LCD_MODES.PIXEL_TRANSFER
    const isUsed = isOamAccess || isPixelTransfer
    return !isUsed
  }

  canAccessVram(access: Access) {
    if (this.getMode() === LCD_MODES.PIXEL_TRANSFER) {
      return false
    }
    if (
      access === Access.READ &&
      this.getMode() === LCD_MODES.OAM_SEARCH &&
      this.lineClocks === 80
    ) {
      return false
    }

    return true
  }

  readByte(address: number): number {
    switch (address) {
      case R_LCDC:
        return this.lcdc
      case R_LY:
        return this.ly
      case R_LYC:
        return this.lyc
      case R_STAT:
        return this.stat | STAT_UNUSED_MASK
      case R_SCX:
        return this.scx
      case R_SCY:
        return this.scy
      case R_BGP:
        return this.bgp
      case R_WX:
        return this.wx
      case R_WY:
        return this.wy
      case R_OBP0:
        return this.obp0
      case R_OBP1:
        return this.obp1
      default:
        if (this.vram.accepts(address)) {
          if (!this.canAccessVram(Access.READ)) {
            return 0xff
          }
          // 0x8001 -> 0x0001
          return this.vram.readByte(address)
        }
        if (this.oam.accepts(address)) {
          if (!this.canAccessOAM(Access.READ)) {
            return 0xff
          }
          // 0xFE01 -> 0x0001
          return this.oam.readByte(address)
        }
        throw createUnmappedMemoryError('GPU', address)
    }
  }

  writeDMAByte(address: number, value: number) {
    // OAM is locked during DMA transfer -> bypass
    this.oam.writeByte(address, value)
  }

  writeByte(address: number, value: number) {
    switch (address) {
      case R_LCDC: {
        const wasEnabled = isLcdEnabled(this.lcdc)
        this.lcdc = value
        const isEnabled = isLcdEnabled(this.lcdc)
        // Off -> on
        if (!wasEnabled && isEnabled) {
          // OAM on the first line is skipped
          // https://www.reddit.com/r/EmuDev/comments/6h2asw/stat_register_and_stat_interrupt_delay_in_dmgcgb/
          this.changeMode(LCD_MODES.H_BLANK)
          this.state.mode = LCD_STATE.VOID
          this.pendingMode = null

          this.lineClocks = 4
          this.checkLyEqualsLyc()
          this.previousStatFlag = 0
          this.skipFrames = 1
        }

        // On -> off
        if (wasEnabled && !isEnabled) {
          this.ly = 0
          this.lyToCompare = 0
          this.state.line = 0
          this.changeMode(LCD_MODES.H_BLANK)
          this.display.clear()
        }
        return
      }
      case R_LY:
        // no-op
        return
      case R_LYC:
        this.lyc = value
        if (isLcdEnabled(this.lcdc)) {
          this.checkLyEqualsLyc()
          this.statUpdate(this.getMode())
        }
        return
      case R_STAT:
        this.stat =
          (this.stat & STAT_BITS.LYC_EQUALS_LY_FLAG) | // keep ly=lyc flag
          (this.stat & STAT_BITS.MODE) | // keep current mode
          (value & STAT_BITS.H_BLANK_INTERRUPT) |
          (value & STAT_BITS.V_BLANK_INTERRUPT) |
          (value & STAT_BITS.LYC_EQUALS_LY_INTERRUPT) |
          (value & STAT_BITS.OAM_INTERRUPT)

        // STAT write bug
        // https://github.com/AntonioND/giibiiadvance/blob/master/source/gb_core/ppu_dmg.c#L87-L96
        // https://forums.nesdev.com/viewtopic.php?t=19016
        if (
          isLcdEnabled(this.lcdc) &&
          [LCD_MODES.H_BLANK, LCD_MODES.V_BLANK].includes(this.getMode())
        ) {
          this.statUpdate(this.getMode())
        }

        return
      case R_SCX:
        this.scx = value
        return
      case R_SCY:
        this.scy = value
        return
      case R_BGP:
        this.bgp = value
        return
      case R_WX:
        this.wx = value
        return
      case R_WY:
        this.wy = value
        return
      case R_OBP0:
        this.obp0 = value
        return
      case R_OBP1:
        this.obp1 = value
        return
      default:
        if (this.vram.accepts(address)) {
          if (!this.canAccessVram(Access.WRITE)) {
            return
          }
          this.vram.writeByte(address, value)
          return
        }
        if (this.oam.accepts(address)) {
          if (!this.canAccessOAM(Access.WRITE)) {
            return
          }
          this.oam.writeByte(address, value)
          return
        }
        throw createUnmappedMemoryError('GPU', address)
    }
  }
}

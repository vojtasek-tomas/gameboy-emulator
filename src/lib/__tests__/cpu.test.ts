import { Cpu } from '../Cpu'

describe('Cpu', () => {
  it('should set AF correctly', () => {
    const r = new Cpu()

    r.set('A', 0xab)
    r.set('F', 0xff)

    expect(r.get('AF')).toEqual(0xabf0)
  })
})

import { Timers, R_DIV } from '../Timers'
import { InterruptController } from '../InterruptController'
import { range } from '../utils'

describe('Timers', () => {
  const getDiv = (t: Timers) => t.dividerCounter >> 8
  describe('divider', () => {
    it('should increment every 256 cycles', () => {
      const timers = new Timers(new InterruptController())

      range(0, 255).forEach(() => {
        timers.tick()
      })
      expect(getDiv(timers)).toEqual(0)
      range(0, 4).forEach(() => {
        timers.tick()
      })
      expect(getDiv(timers)).toEqual(1)

      range(0, 256 * 2).forEach(() => {
        timers.tick()
      })
      expect(getDiv(timers)).toEqual(3)
    })

    it('should correctly reset', () => {
      const timers = new Timers(new InterruptController())

      range(0, 255).forEach(() => {
        timers.tick()
      })
      expect(getDiv(timers)).toEqual(0)

      timers.writeByte(R_DIV, 0x00) // reset
      expect(getDiv(timers)).toEqual(0)

      range(0, 255).forEach(() => {
        timers.tick()
      })
      expect(getDiv(timers)).toEqual(0)
      timers.tick()
      timers.tick()
      timers.tick()
      timers.tick()
      expect(getDiv(timers)).toEqual(1)
    })
  })

  describe('tima', () => {
    test('increment', () => {
      const timers = new Timers(new InterruptController())
      timers.tac = 0b0000_0100

      range(0, 255 * 4).forEach(() => {
        timers.tick()
      })
      expect(timers.tima).toEqual(0)

      range(0, 4 * 4).forEach(() => {
        timers.tick()
      })
      expect(timers.tima).toEqual(1)
    })

    test('overflow', () => {
      const interruptController = new InterruptController()
      const requestInterrupt = jest.spyOn(
        interruptController,
        'requestInterrupt'
      )

      const timers = new Timers(interruptController)
      timers.tac = 0b0000_0100 // start default timer
      timers.tma = 0xab

      range(0, 256 * 255 * 4).forEach(() => {
        timers.tick()
      })
      expect(timers.tima).toEqual(255)
      expect(requestInterrupt).toHaveBeenCalledTimes(0)
      range(0, 256 * 4).forEach(() => {
        timers.tick()
      })

      // interupt is NOT delayed
      expect(requestInterrupt).toHaveBeenCalledTimes(1)
      // write to TIMA is delayed
      expect(timers.tima).toEqual(0)

      timers.tick()
      timers.tick()
      timers.tick()
      timers.tick()
      // TIMA loaded with TMA value
      expect(timers.tima).toEqual(0xab)
      expect(requestInterrupt).toHaveBeenCalledTimes(1)
    })
  })
})

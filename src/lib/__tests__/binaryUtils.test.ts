import { getBit } from '../binaryUtils'

describe('binaryUtils', () => {
  test('getBit', () => {
    expect(getBit(0b10011111, 7)).toEqual(1)
  })
})

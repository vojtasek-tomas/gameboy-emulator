import { LCD_STATE, PPU } from '../PPU'
import { InterruptController } from '../InterruptController'
import { range } from '../utils'
import { R_LY } from '../registers'
import { MemoryDisplay } from '../ppu/displays/MemoryDisplay'
import { LCDC_BITS, STAT_BITS } from '../bits'

const CYCLES_PER_SCANLINE = 456
// const SCANLINES = 154
// const CYCLES_PER_FRAME = SCANLINES * CYCLES_PER_SCANLINE // 70 244

describe('PPU verified', () => {
  test('line 153 quirk', () => {
    /**
     * the last scanline still lasts the full 456 cycles
     * but LY reads zero after 4 cycles
     * https://discordapp.com/channels/465585922579103744/465586075830845475/762052058316275722
     */
    const interruptController = new InterruptController()
    const ppu = new PPU(interruptController, new MemoryDisplay())

    const hasLyEqualsLycFlag = () =>
      (ppu.stat & STAT_BITS.LYC_EQUALS_LY_FLAG) !== 0

    // Direct enable -> skip initial LCD sequence with "special" zero line
    ppu.lcdc = LCDC_BITS.LCD_ENABLED
    ppu.state.mode = LCD_STATE.OAM_SEARCH

    const ticksTo152 = CYCLES_PER_SCANLINE * 153
    range(0, ticksTo152).forEach(() => {
      ppu.tick()
    })

    // Line 153 start
    expect(ppu.lineClocks).toEqual(0)
    expect(ppu.readByte(R_LY)).toEqual(153)

    // Line 153, first ticks, LY=153
    ppu.tick() // 1
    expect(hasLyEqualsLycFlag()).toEqual(false)
    expect(ppu.lineClocks).toEqual(1)
    expect(ppu.readByte(R_LY)).toEqual(153)

    ppu.tick() // 2
    ppu.tick() // 3
    ppu.tick() // 4
    expect(ppu.lineClocks).toEqual(4)
    expect(ppu.readByte(R_LY)).toEqual(0)
    expect(hasLyEqualsLycFlag()).toEqual(false)

    ppu.tick() // 5
    ppu.tick() // 6
    ppu.tick() // 7
    ppu.tick() // 8
    ppu.tick() // 9
    ppu.tick() // 10
    ppu.tick() // 11
    expect(hasLyEqualsLycFlag()).toEqual(false)
    ppu.tick() // 12
    expect(hasLyEqualsLycFlag()).toEqual(true)
    expect(ppu.readByte(R_LY)).toEqual(0)
  })
})

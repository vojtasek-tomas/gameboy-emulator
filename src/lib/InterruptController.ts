import { INTERRUPT_BITS, ISR_ADDRESSES } from './constants'
import { MemoryAccess } from './types'
import { createUnmappedMemoryError } from './errors'

/**
 * Interrupt Flag (R/W)
 *
 * Bit 4: Transition from High to Low of Pin number P10-P13
 * Bit 3: Serial I/O transfer complete
 * Bit 2: Timer Overflow
 * Bit 1: LCDC (see STAT)
 * Bit 0: V-Blank
 */
export const R_IF = 0xff0f

/**
 * Interrupt Enable (R/W)
 *
 * Bit 4: Transition from High to Low of Pin number P10-P13
 * Bit 3: Serial I/O transfer complete
 * Bit 2: Timer Overflow
 * Bit 1: LCDC (see STAT)
 * Bit 0: V-Blank
 */
export const R_IE = 0xffff

// ordered by priority, higher first
// prettier-ignore
const INTERRUPT_MAPPING = [
  { bit: INTERRUPT_BITS.V_BLANK,    address: ISR_ADDRESSES.V_BLANK },
  { bit: INTERRUPT_BITS.LCD_STATS,  address: ISR_ADDRESSES.LCDC_STATUS },
  { bit: INTERRUPT_BITS.TIMER,      address: ISR_ADDRESSES.TIMER_OVERFLOW },
  { bit: INTERRUPT_BITS.SERIAL,     address: ISR_ADDRESSES.SERIAL_TRANSFER_COMPLETION },
  { bit: INTERRUPT_BITS.JOYPAD,     address: ISR_ADDRESSES.JOYPAD_PRESS }
]

export class InterruptController implements MemoryAccess {
  /**
   * When bits are set, an interrupt has happened
   */
  if = 0x00

  /**
   * When bits are set, the corresponding interrupt can be triggered
   */
  ie = 0x00

  accepts = (address: number) => [R_IF, R_IE].includes(address)

  requestInterrupt(interruptBit: number) {
    this.if |= interruptBit
  }

  clearInterrupt(interruptBit: number) {
    this.if &= ~interruptBit
  }

  hasAvailableInterrupts() {
    return !!this.getAvailableInterrupts()
  }

  getAvailableInterrupts() {
    return this.ie & this.if
  }

  getInterruptAddress() {
    const flags = this.getAvailableInterrupts()

    const interrupt = INTERRUPT_MAPPING.find((intr) => flags & intr.bit)
    if (interrupt) {
      this.clearInterrupt(interrupt.bit)
      return interrupt.address
    }

    return 0x0000
  }

  readByte(address: number) {
    switch (address) {
      case R_IE:
        return this.ie
      case R_IF:
        return this.if | 0b1110_0000 // unused top bits
      default:
        throw createUnmappedMemoryError('InterruptController', address)
    }
  }

  writeByte(address: number, value: number) {
    switch (address) {
      case R_IE:
        this.ie = value
        break
      case R_IF:
        this.if = value
        break
      default:
        throw createUnmappedMemoryError('InterruptController', address)
    }
  }
}

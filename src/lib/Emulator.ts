import { MMU } from './MMU'
import {
  CommandDefinition,
  Cartridge,
  TickableEmulator,
  IDisplay,
  Palette
} from './types'
import { CYCLES_PER_FRAME } from './constants'
import { cbInstructions, instructions } from './ops/instructions'
import { PPU } from './PPU'
import { Timers } from './Timers'
import { Joypad } from './Joypad'
import { InterruptController } from './InterruptController'
import { Cpu } from './Cpu'
import { range } from './utils'
import { Serial } from './serial'

export class Emulator implements TickableEmulator {
  // Components
  cpu: Cpu
  mmu: MMU
  timers: Timers
  ppu: PPU
  joypad: Joypad
  serial: Serial
  interruptController: InterruptController

  // Counters
  frameCount = 0
  cyclesCount = 0
  commandsCount = 0
  frameCyclesCount = 0

  // Events
  onExecute: (
    pc: number,
    instruction: string,
    opCode: number,
    cycles: number
  ) => void
  onStateChanged: (isRunning: boolean) => void

  loopIds: number[] = []

  mmuTick: Generator

  constructor(
    display: IDisplay,
    options: { useBios: boolean; palette?: Palette } = { useBios: true }
  ) {
    this.cpu = new Cpu()

    this.interruptController = new InterruptController()
    this.ppu = new PPU(this.interruptController, display, options.palette)
    this.timers = new Timers(this.interruptController)
    this.joypad = new Joypad(this.interruptController)
    this.serial = new Serial(this.interruptController)
    this.mmu = new MMU(
      this.interruptController,
      this.ppu,
      this.timers,
      this.joypad,
      this.serial,
      options.useBios
    )
    this.mmuTick = this.mmu.tick()

    this.initialize(options.useBios)
    this.onExecute = () => null
    this.onStateChanged = () => null
  }

  loadCartridge = (cartridge: Cartridge) => this.mmu.loadCartridge(cartridge)

  initialize(useBios: boolean) {
    if (!useBios) {
      this.cpu
        .setPC(0x100)
        .setSP(0xfffe)
        .set('A', 0x01)
        .set('B', 0xff)
        .set('C', 0x13)
        .set('D', 0x00)
        .set('E', 0xc1)
        .set('H', 0x84)
        .set('L', 0x03)
        .clearFlags()

      // Timers
      this.timers.dividerCounter = 0x1833 // DIV / divCounter
      this.timers.tima = 0x00 // TIMA
      this.timers.tma = 0x00 // TIMA
      this.timers.tac = 0x00 // TAC

      this.mmu.writeByte(0xff02, 0x7e) // SC

      this.mmu.writeByte(0xff03, 0xff) // ??

      // Sound
      this.mmu.writeByte(0xff10, 0x80) // NR10
      this.mmu.writeByte(0xff11, 0xbf) // NR11
      this.mmu.writeByte(0xff12, 0xf3) // NR12
      this.mmu.writeByte(0xff14, 0xbf) // NR14
      this.mmu.writeByte(0xff16, 0x3f) // NR21
      this.mmu.writeByte(0xff17, 0x00) // NR22
      this.mmu.writeByte(0xff19, 0xbf) // NR24
      this.mmu.writeByte(0xff1a, 0x7f) // NR30
      this.mmu.writeByte(0xff1b, 0xff) // NR31
      this.mmu.writeByte(0xff1c, 0x9f) // NR32
      this.mmu.writeByte(0xff1e, 0xbf) // NR33
      this.mmu.writeByte(0xff20, 0xff) // NR41
      this.mmu.writeByte(0xff21, 0x00) // NR42
      this.mmu.writeByte(0xff22, 0x00) // NR43
      this.mmu.writeByte(0xff23, 0xbf) // NR30
      this.mmu.writeByte(0xff24, 0x77) // NR50
      this.mmu.writeByte(0xff25, 0xf3) // NR51
      this.mmu.writeByte(0xff26, 0xf1) // NR52

      // Video
      this.ppu.lcdc = 0x91 // LCDC
      this.ppu.stat = 0x80 // STAT
      this.mmu.writeByte(0xff42, 0x00) // SCY
      this.mmu.writeByte(0xff43, 0x00) // SCX
      this.mmu.writeByte(0xff45, 0x00) // LYC
      this.mmu.writeByte(0xff47, 0xfc) // BGP
      this.mmu.writeByte(0xff48, 0xff) // OBP0
      this.mmu.writeByte(0xff49, 0xff) // OBP1
      this.mmu.writeByte(0xff4a, 0x00) // WY
      this.mmu.writeByte(0xff4b, 0x00) // WX

      // Interrupts
      this.interruptController.if = 0xe1
      this.interruptController.ie = 0
    }
  }

  fetch = (): { command: CommandDefinition; opCode: number } => {
    const opCode = this.readU8Tick()
    if (opCode === 0xcb) {
      const cbOpCode = this.readU8Tick()
      return {
        command: cbInstructions[cbOpCode],
        opCode: 0xcb00 + cbOpCode
      }
    }
    return {
      command: instructions[opCode],
      opCode
    }
  }

  execute = (pc: number, command: CommandDefinition, opCode: number) => {
    const [instruction, execute] = command

    execute(this)
    this.onExecute(pc, instruction, opCode, this.cyclesCount)
  }

  readU8Tick() {
    const value = this.readByteTick(this.cpu.getPC())
    this.cpu.incrementPC()
    return value
  }

  readU16Tick() {
    const value = this.readWordTick(this.cpu.getPC())
    this.cpu.incrementPC()
    this.cpu.incrementPC()
    return value
  }

  readByteTick(address: number) {
    const value = this.mmu.readByte(address)
    this.tick()
    return value
  }

  readWordTick(address: number) {
    const lower = this.readByteTick(address)
    const upper = this.readByteTick(address + 1) << 8
    return lower + upper
  }

  writeByteTick(address: number, value: number) {
    this.mmu.writeByte(address, value)
    this.tick()
  }

  writeWordTick(address: number, value: number) {
    this.writeByteTick(address + 1, value >> 8)
    this.writeByteTick(address, value & 0xff)
  }

  handleInterrupts() {
    this.cpu.ime = false
    this.cpu.halted = false

    // Serving an interrupt is supposed to take 5 M-cycles
    // 2x write byte tick + 3x internal tick
    this.tick()
    this.tick()
    this.tick()

    // upper nibble of PC first
    this.cpu.incrementSP(-1)
    this.writeByteTick(this.cpu.getSP(), this.cpu.getPC() >> 8)

    // ack interrupt
    const address = this.interruptController.getInterruptAddress()

    // lower nibble if PC
    this.cpu.incrementSP(-1)
    this.writeByteTick(this.cpu.getSP(), this.cpu.getPC() & 0xff)

    // Go to interrupt handler
    this.cpu.setPC(address)
  }

  tick = () => {
    const cycles = 4

    this.mmuTick.next()
    /**
     * When drawing only background it generates 8 pixels every 8 cycles into the upper part of the FIFO
     * and then shifts them down over the next 8 cycles ready for the next 8 pixels to be written
     */
    range(0, 4).forEach(() => {
      this.ppu.tick()
    })

    range(0, 4).forEach(() => {
      this.timers.tick()
    })

    this.frameCyclesCount += cycles
    this.cyclesCount += cycles
  }

  runFrame(triggerChange = false) {
    if (triggerChange) {
      this.onStateChanged(true)
    }
    while (this.frameCyclesCount < CYCLES_PER_FRAME) {
      this.runStep()
    }
    this.frameCount++
    this.frameCyclesCount = 0
    if (triggerChange) {
      this.onStateChanged(true)
    }
  }

  getCommand(opCode: number) {
    if (opCode === 0xcb) {
      const cbOpCode = this.readU8Tick()
      return {
        command: cbInstructions[cbOpCode],
        opCode: 0xcb00 + cbOpCode
      }
    }
    return {
      command: instructions[opCode],
      opCode
    }
  }

  runStep() {
    if (this.cpu.halted && !this.cpu.justHalted) {
      this.tick()
    }

    const pendingInterrupts = this.interruptController.hasAvailableInterrupts()

    this.cpu.justHalted = false

    const currentIme = this.cpu.ime
    if (this.cpu.imeRequested) {
      this.cpu.imeRequested = false
      this.cpu.ime = true
    }

    if (this.cpu.halted && !currentIme && pendingInterrupts) {
      this.cpu.halted = false
    } else if (currentIme && pendingInterrupts) {
      this.cpu.halted = false
      this.handleInterrupts()
    } else if (!this.cpu.halted) {
      const pc = this.cpu.getPC()

      const opCode = this.readU8Tick()

      if (this.cpu.haltBug) {
        this.cpu.incrementPC(-1)
        this.cpu.haltBug = false
      }
      const command = this.getCommand(opCode)
      this.execute(pc, command.command, command.opCode)
    } else {
      // throw new Error('invalid state')
    }

    // this.execute(pc, command, opCode)
    // this.commandsCount++

    // if (this.cpu.state === CpuState.HALT) {
    //   // just tick, CPU will come back to normal after interrupt request
    //   this.tick()
    // } else {
    //   if (this.cpu.state === CpuState.ENABLED_IME) {
    //     this.cpu.ime = true
    //   }
    //   const pc = this.cpu.getPC()
    //   const { command, opCode } = this.fetch()

    //   if (!command) {
    //     throw new Error(
    //       `Unknown command ${formatHex(opCode)}, cycles=${this.cyclesCount}`
    //     )
    //   }

    //   if (this.cpu.state === CpuState.HALT_BUG) {
    //     this.cpu.setPC(pc - 1)
    //     this.cpu.state = CpuState.NORMAL
    //   }

    //   this.execute(pc, command, opCode)
    // }
    // this.commandsCount++
  }

  runCycles(cycles: number) {
    this.onStateChanged(true)
    const endCycles = this.cyclesCount + cycles
    while (this.cyclesCount < endCycles) {
      this.runStep()
    }
    this.onStateChanged(false)
  }

  loop = () => {
    this.runFrame()
    const loopId = window.requestAnimationFrame(this.loop)
    this.loopIds.push(loopId)
  }

  stop() {
    this.loopIds.forEach(window.cancelAnimationFrame)
    this.joypad.unlisten()
    this.onStateChanged(false)
  }

  run() {
    this.onStateChanged(true)
    this.joypad.listen()
    window.requestAnimationFrame(this.loop)
  }
}

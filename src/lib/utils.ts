export function number2hex(val: number, padNumber = 2) {
  return val.toString(16).padStart(padNumber, '0')
}

export function formatHex(val: number, padNumber = 2) {
  return `0x${number2hex(val, padNumber).toUpperCase()}`
}

export function formatBytes(bytes: number, decimals = 2) {
  if (bytes === 0) return '0 B'

  const k = 1024
  const dm = decimals < 0 ? 0 : decimals
  const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  const i = Math.floor(Math.log(bytes) / Math.log(k))

  return `${parseFloat((bytes / k ** i).toFixed(dm))} ${sizes[i]}`
}

export function isBetween(x: number, min: number, max: number) {
  return x >= min && x <= max
}

export function range(start: number, stop: number, step = 1) {
  if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
    return []
  }

  const result = []
  for (let i = start; step > 0 ? i < stop : i > stop; i += step) {
    result.push(i)
  }

  return result
}

export function sum(numbers: number[]) {
  return numbers.reduce((a, b) => a + b, 0)
}

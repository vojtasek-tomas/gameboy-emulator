import { INTERRUPT_BITS } from './constants'
import { InterruptController } from './InterruptController'
import { MemoryAccess } from './types'
import { createUnmappedMemoryError } from './errors'

/**
 * Register for reading joy pad info
 * and determining system type. (R/W)
 */
const R_JOYP = 0xff00

const joypadRegisters = [R_JOYP]

/**
 * Bit 7 - Not used
 * Bit 6 - Not used
 * Bit 5 - P15 Select Button Keys      (0=Select)
 * Bit 4 - P14 Select Direction Keys   (0=Select)
 * Bit 3 - P13 Input Down  or Start    (0=Pressed) (Read Only)
 * Bit 2 - P12 Input Up    or Select   (0=Pressed) (Read Only)
 * Bit 1 - P11 Input Left  or Button B (0=Pressed) (Read Only)
 * Bit 0 - P10 Input Right or Button A (0=Pressed) (Read Only)
 *
 *           P14        P15
 *           |          |
 * P10-------O-Right----O-A
 *           |          |
 * P11-------O-Left-----O-B
 *           |          |
 * P12-------O-Up-------O-Select
 *           |          |
 * P13-------O-Down-----O-Start
 *
 */

// prettier-ignore
const ARROW_BITS = {
  RIGHT:      0b0001,
  LEFT:       0b0010,
  UP:         0b0100,
  DOWN:       0b1000
}

// prettier-ignore
const BUTTON_BITS = {
  A:          0b0001,
  B:          0b0010,
  SELECT:     0b0100,
  START:      0b1000
}

type Mapping = { [code: string]: number } // event.code

// https://keycode.info/
const ARROWS_MAPPING: Mapping = {
  ArrowRight: ARROW_BITS.RIGHT,
  ArrowLeft: ARROW_BITS.LEFT,
  ArrowUp: ARROW_BITS.UP,
  ArrowDown: ARROW_BITS.DOWN,

  // AWSD
  KeyD: ARROW_BITS.RIGHT,
  KeyA: ARROW_BITS.LEFT,
  KeyW: ARROW_BITS.UP,
  KeyS: ARROW_BITS.DOWN
}

const BUTTONS_MAPPING: Mapping = {
  KeyX: BUTTON_BITS.A,
  KeyC: BUTTON_BITS.B,

  KeyJ: BUTTON_BITS.A,
  KeyK: BUTTON_BITS.B,
  KeyG: BUTTON_BITS.SELECT,
  KeyH: BUTTON_BITS.START
}

const JOYP_UNUSED_MASK = 0b1100_0000

export class Joypad implements MemoryAccess {
  // which group of keys should be returned by readByte, controlled from outside
  selectedColumn = 0b11

  arrowBits = 0b1111

  buttonBits = 0b1111

  interruptController: InterruptController

  enabled = false

  constructor(interruptController: InterruptController) {
    this.interruptController = interruptController
    this.listen()
  }

  accepts = (address: number) => joypadRegisters.includes(address)

  listen = () => {
    if (this.enabled) {
      return
    }
    window.addEventListener('keydown', this.onKeyDown)
    window.addEventListener('keyup', this.onKeyUp)
    this.enabled = true
  }

  unlisten = () => {
    if (!this.enabled) {
      return
    }
    window.removeEventListener('keydown', this.onKeyDown)
    window.removeEventListener('keyup', this.onKeyUp)
    this.enabled = false
  }

  onKeyDown = (e: KeyboardEvent) => {
    const { code } = e
    const arrowBit = ARROWS_MAPPING[code]
    if (arrowBit) {
      this.arrowBits &= ~arrowBit
      this.update()
    }
    const buttonBit = BUTTONS_MAPPING[code]
    if (buttonBit) {
      this.buttonBits &= ~buttonBit
      this.update()
    }
  }

  onKeyUp = (e: KeyboardEvent) => {
    const { code } = e
    const arrowBit = ARROWS_MAPPING[code]
    if (arrowBit) {
      this.arrowBits |= arrowBit
      this.update()
    }
    const buttonBit = BUTTONS_MAPPING[code]
    if (buttonBit) {
      this.buttonBits |= buttonBit
      this.update()
    }
  }

  update = () => {
    this.interruptController.requestInterrupt(INTERRUPT_BITS.JOYPAD)
  }

  readByte(address: number) {
    if (address === R_JOYP) {
      if (this.selectedColumn & 0b01) {
        return this.buttonBits | JOYP_UNUSED_MASK
      }
      if (this.selectedColumn & 0b10) {
        return this.arrowBits | JOYP_UNUSED_MASK
      }
      return 0xff
    }
    throw createUnmappedMemoryError('Joypad', address)
  }

  writeByte(address: number, value: number) {
    if (address === R_JOYP) {
      // Only bits 5 & 6 are writeable
      this.selectedColumn = (value & 0b0011_0000) >> 4
      return
    }
    throw createUnmappedMemoryError('Joypad', address)
  }
}

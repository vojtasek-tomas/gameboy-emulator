import {
  DualRegisters,
  Flags,
  RegisterNames,
  SimpleRegisters,
  ICpu
} from './types'
import { getBit, setBit, clearBit } from './binaryUtils'

function isBasicRegister(register: RegisterNames): register is SimpleRegisters {
  return register.length === 1
}

export class Cpu implements ICpu {
  ime = false

  halted = false
  haltBug = false
  imeRequested = false
  justHalted = false

  registers = {
    // 8bit registers, 0x00 - 0xFF -> 0 - 255
    A: 0,
    B: 0,
    C: 0,
    D: 0,
    E: 0,

    // CPU Flags register
    // 3-0 are always 0
    // | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
    // | Z | N | H | C | 0 | 0 | 0 | 0 |
    F: 0,
    H: 0,
    L: 0,

    /**
     * Memory address of the next instruction
     * 16 bit
     */
    programCounter: 0,
    /**
     * Memory address of the top of stack
     * 16 bit
     * Stack grows downwards!
     */
    stackPointer: 0
  }

  getMapping: {
    [key in DualRegisters]: () => number
  } = {
    AF: () => (this.registers.A << 8) + this.registers.F,
    BC: () => (this.registers.B << 8) + this.registers.C,
    DE: () => (this.registers.D << 8) + this.registers.E,
    HL: () => (this.registers.H << 8) + this.registers.L,
    SP: () => this.getSP()
  }

  setMapping: {
    [key in DualRegisters]: (value: number) => this
  } = {
    AF: (value) => this.set('A', value >> 8).set('F', value),
    BC: (value) => this.set('B', value >> 8).set('C', value),
    DE: (value) => this.set('D', value >> 8).set('E', value),
    HL: (value) => this.set('H', value >> 8).set('L', value),
    SP: (value) => this.setSP(value)
  }

  flagsIndexMapping: {
    [key in Flags]: number
  } = {
    Z: 7,
    N: 6,
    H: 5,
    C: 4
  }

  get(register: RegisterNames) {
    if (isBasicRegister(register)) {
      if (register === 'F') {
        return this.registers[register] & ~0xf
      }
      return this.registers[register]
    }
    const fn = this.getMapping[register]
    return fn()
  }

  set(register: RegisterNames, value: number) {
    if (isBasicRegister(register)) {
      if (register === 'F') {
        if ((value & 0b1111) > 0) {
          // eslint-disable-next-line no-param-reassign
          value &= ~0b1111
        }
      }

      // handle overflow
      this.registers[register] = value & 0xff
      return this
    }
    const fn = this.setMapping[register]
    return fn(value)
  }

  increment16Bit(register: 'stackPointer' | 'programCounter', value = 1) {
    const newValue = this.registers[register] + value
    this.registers[register] = newValue & 0xffff
    return this
  }

  incrementPC(value = 1) {
    return this.increment16Bit('programCounter', value)
  }

  incrementSP(value = 1) {
    return this.increment16Bit('stackPointer', value)
  }

  setSP(value: number) {
    this.registers.stackPointer = value
    return this
  }

  setPC(value: number) {
    this.registers.programCounter = value
    return this
  }

  getPC() {
    return this.registers.programCounter
  }

  getSP() {
    return this.registers.stackPointer
  }

  getFlag(flag: Flags) {
    return getBit(this.registers.F, this.flagsIndexMapping[flag])
  }

  setFlag(flag: Flags) {
    this.registers.F = setBit(this.registers.F, this.flagsIndexMapping[flag])
    return this
  }

  clearFlag(flag: Flags) {
    this.registers.F = clearBit(this.registers.F, this.flagsIndexMapping[flag])
    return this
  }

  clearFlags() {
    this.registers.F = 0x00
    return this
  }
}

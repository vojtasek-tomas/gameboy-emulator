import { RAM_BANK_SIZE, ROM_BANK_SIZE } from '../constants'

export function getRamAddress(address: number, bank: number, ramSize: number) {
  const offset = bank * RAM_BANK_SIZE
  const realAddress = (address & 0x1fff) + offset
  return realAddress & (ramSize - 1)
}

export function getRomAddress(address: number, bank: number, romSize: number) {
  const offset = bank * ROM_BANK_SIZE
  const realAddress = (address & 0x3fff) + offset
  return realAddress & (romSize - 1)
}

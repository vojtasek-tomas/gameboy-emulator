import { MBC1, MODES } from '../MBC1'
import { range } from '../../utils'
import { ROM_BANK_SIZE, RAM_BANK_SIZE } from '../../constants'

describe('MBC1', () => {
  it('should skip zero based banks', () => {
    const data = range(0x00, 0xf7)
      .map((bankNumber) => range(0, ROM_BANK_SIZE).map(() => bankNumber))
      .flat()

    const binary = Uint8Array.from(data)
    const cart = new MBC1(binary, data.length, 0)

    // bank 0x20 -> 0x21
    cart.writeByte(0x2000, 0x00) // / XXX0_00000
    cart.writeByte(0x4000, 0x01) //  0010_00000 -> 0x20
    expect(cart.readByte(0x4000)).toEqual(0x20)

    // Same data for bank 20 and 21
    cart.writeByte(0x4000, 0x11) //  0011_00000 -> 0x21
    expect(cart.readByte(0x4000)).toEqual(0x20)
  })

  it('should switch ROM banks', () => {
    const data = range(0x00, 0xf7)
      .map((bankNumber) => range(0, ROM_BANK_SIZE).map(() => bankNumber))
      .flat()

    data[0x1132a7] = 0xab

    const binary = Uint8Array.from(data)
    const cart = new MBC1(binary, data.length, 0)
    cart.writeByte(0x2000, 0b00100)
    cart.writeByte(0x4000, 0b10)
    expect((cart.bank2 << 5) | cart.bank1).toEqual(0x44)
    expect(cart.readByte(0x72a7)).toEqual(0xab)
  })

  it('should switch RAM banks', () => {
    const data = range(0x00, 0x04)
      .map((bankNumber) => range(0, ROM_BANK_SIZE).map(() => bankNumber))
      .flat()

    const binary = Uint8Array.from(data)
    const cart = new MBC1(binary, data.length, 4 * RAM_BANK_SIZE)
    cart.bank2 = 0b10
    cart.mode = MODES.RAM_BANKING
    cart.ramEnabled = true
    cart.ram[0x5123] = 0xaa

    expect(cart.readByte(0xb123)).toEqual(0xaa)
  })

  it('should switch RAM banks 2', () => {
    const data = range(0x00, 0x04)
      .map((bankNumber) => range(0, ROM_BANK_SIZE).map(() => bankNumber))
      .flat()

    const binary = Uint8Array.from(data)
    const cart = new MBC1(binary, data.length, 2 * RAM_BANK_SIZE)

    // change to RAM mode
    cart.writeByte(0x6000, 1)

    // enable RAM
    cart.writeByte(0x0000, 0x0a)

    // write to 0 RAM bank
    cart.writeByte(0xa000, 0xaa)

    // read from RAM
    expect(cart.readByte(0xa000)).toEqual(0xaa)

    // disable RAM
    cart.writeByte(0x0000, 0xff)

    // read from disabled RAM
    expect(cart.readByte(0xa000)).toEqual(0xff)
  })
})

import { ROMOnly } from './ROMOnly'
import { MBC1 } from './MBC1'
import { Cartridge, CartridgeMeta } from '../types'
import { MBC5 } from './MBC5'
import { RAM_BANK_SIZE, ROM_BANK_SIZE } from '../constants'
import { formatHex } from '../utils'
import { MBC3 } from './MBC3'
import { MBC2 } from './MBC2'

// https://gbdev.io/pandocs/#_0148-rom-size

const ramSizesMapping: { [type: number]: number } = {
  0: 0,
  1: 2048,
  2: 1 * RAM_BANK_SIZE, //  8192
  3: 4 * RAM_BANK_SIZE, // 32768
  4: 16 * RAM_BANK_SIZE, // 131_072
  5: 8 * RAM_BANK_SIZE // 65536
}

const romSizesMapping: { [type: number]: number } = {
  0x00: 2 * ROM_BANK_SIZE, //  32KB (no ROM banking)
  0x01: 4 * ROM_BANK_SIZE, //  64KB (4 banks)
  0x02: 8 * ROM_BANK_SIZE, // 128KB (8 banks)
  0x03: 16 * ROM_BANK_SIZE, // 256KB (16 banks)
  0x04: 32 * ROM_BANK_SIZE, // 512KB (32 banks)
  0x05: 64 * ROM_BANK_SIZE, //   1MB (64 banks)  - only 63 banks used by MBC1
  0x06: 128 * ROM_BANK_SIZE, //   2MB (128 banks) - only 125 banks used by MBC1
  0x07: 256 * ROM_BANK_SIZE, //   4MB (256 banks)
  0x08: 512 * ROM_BANK_SIZE //   8MB (512 banks)
}

export function createCartridge(
  bytes: Uint8Array
): {
  cartridge: Cartridge
  meta: CartridgeMeta
} {
  const cartridgeType = bytes[0x147]
  const romType = bytes[0x148]
  const ramType = bytes[0x149]
  const romSize = romSizesMapping[romType]
  const ramSize = ramSizesMapping[ramType]

  const title = Array.from(bytes.slice(0x134, 0x144))
    .map((c) => String.fromCharCode(c))
    .join('')
    .trim()

  const meta = {
    title,
    cartridgeType,
    romType,
    romSize,
    ramType,
    ramSize
  }

  switch (cartridgeType) {
    case 0x00:
      return {
        cartridge: new ROMOnly(bytes),
        meta
      }
    case 0x01: // MBC1
    case 0x02: // MBC1+RAM
    case 0x03: // MBC1+RAM+BATTERY
      return {
        cartridge: new MBC1(bytes, romSize, ramSize),
        meta
      }
    case 0x05:
    case 0x06: {
      // MBC2 has fixed internal RAM 512 x 4bits
      const realRamSize = ramSize || 512
      return {
        cartridge: new MBC2(bytes, romSize, realRamSize),
        meta: {
          ...meta,
          ramSize: realRamSize
        }
      }
    }
    case 0x11: // MBC3
    case 0x12: // MBC3+RAM
    case 0x13: // MBC3+RAM+BATTERY
      return {
        meta,
        cartridge: new MBC3(bytes, romSize, ramSize)
      }
    case 0x19: // MBC5
    case 0x1a: // MBC5+RAM
    case 0x1b: // MBC5+RAM+BATTERY
      return {
        cartridge: new MBC5(bytes, romSize, ramSize),
        meta
      }
    default:
      throw new Error(
        `Missing implementation for cartridge type ${formatHex(cartridgeType)}`
      )
  }
}

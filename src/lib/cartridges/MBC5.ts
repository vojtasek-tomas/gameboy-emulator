import { isBetween } from '../utils'
import { Cartridge } from '../types'
import { createUnmappedMemoryError } from '../errors'
import { getRamAddress, getRomAddress } from './utils'

export class MBC5 implements Cartridge {
  binary: Uint8Array

  ramEnabled = false
  ram: Uint8Array

  romSize: number
  ramSize: number

  // lower ROM bank register
  romb0 = 1
  // upper ROM bank register
  romb1 = 0
  // RAM bank register
  ramb = 0

  constructor(binary: Uint8Array, romSize: number, ramSize: number) {
    this.binary = binary.slice(0, romSize)
    this.romSize = romSize
    this.ramSize = ramSize
    this.ram = new Uint8Array(ramSize)
  }

  accepts = (address: number) =>
    isBetween(address, 0x0000, 0x7fff) || isBetween(address, 0xa000, 0xbfff)

  readByte(address: number) {
    // 0000-3FFF - ROM Bank 00 (Read Only)
    if (isBetween(address, 0x0000, 0x3fff)) {
      // always bank 0 - no offset here
      return this.binary[address]
    }

    // 4000-7FFF - ROM Bank 01-1FF (Read Only)
    if (isBetween(address, 0x4000, 0x7fff)) {
      const bank = this.romb0 | (this.romb1 << 8)
      const realAddress = getRomAddress(address, bank, this.romSize)
      return this.binary[realAddress]
    }

    // A000-BFFF - RAM Bank 00-03, if any (Read/Write)
    if (isBetween(address, 0xa000, 0xbfff)) {
      if (!this.ramEnabled || !this.ramSize) {
        return 0xff
      }
      const realAddress = getRamAddress(address, this.ramb, this.ramSize)
      return this.ram[realAddress]
    }

    throw createUnmappedMemoryError('MBC5 (read)', address)
  }

  writeByte(address: number, value: number) {
    // 0000-1FFF - RAM Enable (Write Only)
    if (isBetween(address, 0x0000, 0x1fff)) {
      this.ramEnabled = value === 0x0a
      return
    }

    // 2000-2FFF - Low 8 bits of ROM Bank Number (Write Only)
    if (isBetween(address, 0x2000, 0x2fff)) {
      this.romb0 = value
      return
    }

    // 3000-3FFF - High bit of ROM Bank Number (Write Only)
    if (isBetween(address, 0x3000, 0x3fff)) {
      this.romb1 = value & 0b1
      return
    }

    // 4000-5FFF - RAM Bank Number (Write Only)
    if (isBetween(address, 0x4000, 0x5fff)) {
      this.ramb = value & 0b1111
      return
    }

    // 6000-7FFF - nothing
    if (isBetween(address, 0x6000, 0x7fff)) {
      // no-op
      return
    }

    // A000-BFFF - RAM Bank 00-03, if any (Read/Write)
    if (isBetween(address, 0xa000, 0xbfff)) {
      if (!this.ramEnabled || !this.ramSize) {
        return // nop-op
      }
      const realAddress = getRamAddress(address, this.ramb, this.ramSize)
      this.ram[realAddress] = value
      return
    }

    throw createUnmappedMemoryError('MBC5 (write)', address)
  }
}

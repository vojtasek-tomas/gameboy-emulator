import { isBetween } from '../utils'
import { Cartridge } from '../types'
import { createUnmappedMemoryError } from '../errors'
import { getRamAddress, getRomAddress } from './utils'

/**
 * 0000-3FFF - ROM Bank 00 (Read Only)
 * 4000-7FFF - ROM Bank 01-0F (Read Only)
 *
 * A000-BFFF - 512x4bits RAM, built-in into the MBC2 chip (Read/Write)
 *
 * 0000-3FFF - RAM Enable and ROM Bank Number (Write Only)
 */

export class MBC2 implements Cartridge {
  binary: Uint8Array

  ram: Uint8Array
  romSize: number
  ramSize: number

  romb = 0x01
  ramEnabled = false

  constructor(binary: Uint8Array, romSize: number, ramSize: number) {
    this.binary = binary.slice(0, romSize)
    this.romSize = romSize
    this.ramSize = ramSize
    this.ram = new Uint8Array(ramSize)
  }

  accepts = (address: number) =>
    isBetween(address, 0x0000, 0x7fff) || isBetween(address, 0xa000, 0xbfff)

  readByte(address: number) {
    // 0000-3FFF - ROM Bank 00 (Read Only)
    if (isBetween(address, 0x0000, 0x3fff)) {
      // always bank 0 - no offset here
      return this.binary[address]
    }

    // 4000-7FFF - ROM Bank 01-0F (Read Only)
    if (isBetween(address, 0x4000, 0x7fff)) {
      const realAddress = getRomAddress(address, this.romb, this.romSize)
      return this.binary[realAddress]
    }

    // A000-BFFF - 512x4bits RAM, built-in into the MBC2 chip (Read/Write)
    if (isBetween(address, 0xa000, 0xbfff)) {
      if (this.ramEnabled) {
        const realAddress = getRamAddress(address, 0, this.ramSize)
        return this.ram[realAddress] | 0xf0 // top bits are undefined
      }
      return 0xff
    }

    throw createUnmappedMemoryError('MBC2 (read)', address)
  }

  writeByte(address: number, value: number) {
    if (isBetween(address, 0x0000, 0x3fff)) {
      // 8. bit of address decides what to do
      if (address & 0b1_0000_0000) {
        // ROMB handle
        this.romb = value & 0xf || 0x01 // register doesn't allow 0
      } else {
        // RAMG handle
        this.ramEnabled = (value & 0x0f) === 0x0a
      }
      return
    }

    if (isBetween(address, 0x4000, 0x7fff)) {
      // no-op
      return
    }

    // A000-BFFF - 512x4bits RAM, built-in into the MBC2 chip (Read/Write)
    if (isBetween(address, 0xa000, 0xbfff)) {
      if (this.ramEnabled) {
        const realAddress = getRamAddress(address, 0, this.ramSize)
        this.ram[realAddress] = (value & 0xf) | 0xf0
      }
      return
    }

    throw createUnmappedMemoryError('MBC2 (write)', address)
  }
}

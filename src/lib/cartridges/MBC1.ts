import { isBetween } from '../utils'
import { Cartridge } from '../types'
import { createUnmappedMemoryError } from '../errors'
import { RAM_BANK_SIZE, ROM_BANK_SIZE } from '../constants'

/**
 * 0000-3FFF - ROM Bank 00 (Read Only)
 * 4000-7FFF - ROM Bank 01-7F (Read Only)
 *
 * 0000-1FFF - RAM Enable (Write Only)
 * 2000-3FFF - ROM Bank Number (Write Only) - low 4 bits
 *
 * 4000-5FFF - RAM Bank Number - or - Upper Bits of ROM Bank Number (Write Only)
 * 6000-7FFF - ROM/RAM Mode Select (Write Only)
 *
 * A000-BFFF - RAM Bank 00-03, if any (Read/Write)
 */

// rename https://gekkio.fi/files/gb-docs/gbctr.pdf
export const MODES = {
  ROM_BANKING: 0x00, // (up to 8KByte RAM, 2MByte ROM) (default)
  RAM_BANKING: 0x01 // (up to 32KByte RAM, 512KByte ROM)
}

export class MBC1 implements Cartridge {
  binary: Uint8Array

  ramEnabled = false

  mode = MODES.ROM_BANKING

  ram: Uint8Array

  romSize: number

  ramSize: number

  bank1 = 0b0_0001

  bank2 = 0b00

  constructor(binary: Uint8Array, romSize: number, ramSize: number) {
    this.binary = binary.slice(0, romSize)
    this.romSize = romSize
    this.ramSize = ramSize
    this.ram = new Uint8Array(ramSize)
  }

  accepts = (address: number) =>
    isBetween(address, 0x0000, 0x7fff) || isBetween(address, 0xa000, 0xbfff)

  getRAMOffset() {
    const bankNumber = this.mode === MODES.ROM_BANKING ? 0 : this.bank2
    return bankNumber * RAM_BANK_SIZE
  }

  getRAMAddress(address: number) {
    const offset = this.getRAMOffset()
    const realAddress = (address & 0x1fff) + offset
    return realAddress & (this.ramSize - 1)
  }

  getLowerROMOffset() {
    const bankNumber = this.mode === MODES.ROM_BANKING ? 0 : this.bank2 << 5
    return bankNumber * ROM_BANK_SIZE
  }

  getUpperROMOffset() {
    return (this.bank1 | (this.bank2 << 5)) * ROM_BANK_SIZE
  }

  readByte(address: number) {
    // 0000-3FFF - ROM Bank 00 (Read Only)
    if (isBetween(address, 0x0000, 0x3fff)) {
      const offset = this.getLowerROMOffset()
      const realAddress = (offset | (address & 0x3fff)) & (this.romSize - 1)
      return this.binary[realAddress]
    }

    // 4000-7FFF - ROM Bank 01-7F (Read Only)
    if (isBetween(address, 0x4000, 0x7fff)) {
      const offset = this.getUpperROMOffset()
      const realAddress = (offset | (address & 0x3fff)) & (this.romSize - 1)
      return this.binary[realAddress]
    }

    // A000-BFFF - RAM Bank 00-03, if any (Read/Write)
    if (isBetween(address, 0xa000, 0xbfff)) {
      if (!this.ramEnabled || !this.ramSize) {
        return 0xff
      }
      const realAddress = this.getRAMAddress(address)
      return this.ram[realAddress]
    }

    throw createUnmappedMemoryError('MBC1 (read)', address)
  }

  writeByte(address: number, value: number) {
    // 0000-1FFF - RAM Enable (Write Only)
    if (isBetween(address, 0x0000, 0x1fff)) {
      this.ramEnabled = (value & 0xf) === 0xa
      return
    }

    // 2000-3FFF - ROM Bank Number (Write Only) - low 5 bits
    if (isBetween(address, 0x2000, 0x3fff)) {
      // 0 bank is always skipped
      const sanitized = (value & 0x1f) === 0 ? 1 : value & 0x1f
      this.bank1 = sanitized
      return
    }

    // 4000-5FFF - RAM Bank Number - or - Upper Bits of ROM Bank Number (Write Only)
    if (isBetween(address, 0x4000, 0x5fff)) {
      const sanitized = value & 0b11
      this.bank2 = sanitized
      return
    }

    // 6000-7FFF - ROM/RAM Mode Select (Write Only)
    if (isBetween(address, 0x6000, 0x7fff)) {
      this.mode = value & 1 ? MODES.RAM_BANKING : MODES.ROM_BANKING
      return
    }

    // A000-BFFF - RAM Bank 00-03, if any (Read/Write)
    if (isBetween(address, 0xa000, 0xbfff)) {
      if (!this.ramEnabled || !this.ramSize) {
        return // nop-op
      }
      const realAddress = this.getRAMAddress(address)
      this.ram[realAddress] = value
      return
    }

    throw createUnmappedMemoryError('MBC1 (write)', address)
  }
}

import { isBetween } from '../utils'
import { Cartridge } from '../types'
import { createUnmappedMemoryError } from '../errors'
import { getRamAddress, getRomAddress } from './utils'

export class MBC3 implements Cartridge {
  binary: Uint8Array

  ramEnabled = false
  ram: Uint8Array

  romSize: number
  ramSize: number

  // ROM bank register
  romb = 0x1
  // RAM bank register
  ramb = 0x0

  constructor(binary: Uint8Array, romSize: number, ramSize: number) {
    this.binary = binary.slice(0, romSize)
    this.romSize = romSize
    this.ramSize = ramSize
    this.ram = new Uint8Array(ramSize)
  }

  accepts = (address: number) =>
    isBetween(address, 0x0000, 0x7fff) || isBetween(address, 0xa000, 0xbfff)

  readByte(address: number) {
    // 0000-3FFF - ROM Bank 00 (Read Only)
    if (isBetween(address, 0x0000, 0x3fff)) {
      // always bank 0 - no offset here
      return this.binary[address]
    }

    // 4000-7FFF - ROM Bank 01-1FF (Read Only)
    if (isBetween(address, 0x4000, 0x7fff)) {
      const realAddress = getRomAddress(address, this.romb, this.romSize)
      return this.binary[realAddress]
    }

    // A000-BFFF - RAM Bank 00-03, if any (Read/Write)
    // A000-BFFF - RTC Register 08-0C (Read/Write)
    // FIXME
    if (isBetween(address, 0xa000, 0xbfff)) {
      if (!this.ramEnabled) {
        return 0xff
      }
      if (isBetween(this.ramb, 0x00, 0x03)) {
        const realAddress = getRamAddress(address, this.ramb, this.ramSize)
        return this.ram[realAddress]
      }
      return 0xff
    }

    throw createUnmappedMemoryError('MBC3 (read)', address)
  }

  writeByte(address: number, value: number) {
    // 0000-1FFF - RAM and Timer Enable (Write Only)
    if (isBetween(address, 0x0000, 0x1fff)) {
      this.ramEnabled = (value & 0x0a) === 0x0a
      return
    }

    // 2000-3FFF - ROM Bank Number (Write Only)
    if (isBetween(address, 0x2000, 0x3fff)) {
      this.romb = value === 0 ? 1 : value
      return
    }

    // 4000-5FFF - RAM Bank Number - or - RTC Register Select (Write Only)
    if (isBetween(address, 0x4000, 0x5fff)) {
      this.ramb = value & 0b1111
      return
    }

    // 6000-7FFF - Latch Clock Data (Write Only)
    if (isBetween(address, 0x6000, 0x7fff)) {
      // FIXME
      return
    }

    // A000-BFFF - RAM Bank 00-03, if any (Read/Write)
    // A000-BFFF - RTC Register 08-0C (Read/Write)
    // FIXME
    if (isBetween(address, 0xa000, 0xbfff)) {
      // THIS IS BROKEN
      if (!this.ramEnabled) {
        return // nop-op
      }
      if (isBetween(this.ramb, 0x00, 0x03)) {
        const realAddress = getRamAddress(address, this.ramb, this.ramSize)
        this.ram[realAddress] = value
        return
      }
      return
    }

    throw createUnmappedMemoryError('MBC3 (write)', address)
  }
}

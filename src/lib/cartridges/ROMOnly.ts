import { isBetween } from '../utils'
import { Cartridge } from '../types'

export class ROMOnly implements Cartridge {
  binary: Uint8Array

  constructor(binary: Uint8Array) {
    this.binary = binary
  }

  accepts = (address: number) => isBetween(address, 0x0000, 0x7fff)

  readByte(address: number) {
    // 0000-7FFF - ROM (Read Only)
    if (this.accepts(address)) {
      return this.binary[address]
    }

    // default value
    return 0xff
  }

  // eslint-disable-next-line class-methods-use-this
  writeByte() {
    // no-op
  }
}

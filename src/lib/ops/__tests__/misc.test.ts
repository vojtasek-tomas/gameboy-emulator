import { cpl, i } from '../misc'
import { TestEmulator } from './utils'

describe('misc', () => {
  describe('cpl', () => {
    it('should invert bits', () => {
      const val = 0b11000011
      expect(cpl(val)).toEqual(0b00111100)
    })
  })

  describe('daa', () => {
    it('should work', () => {
      const e = new TestEmulator()

      const [, daa] = i[0x27]

      e.cpu.setFlag('C').set('A', 0x00)
      daa(e)

      expect(e.cpu.get('A')).toEqual(0x60)
    })
  })
})

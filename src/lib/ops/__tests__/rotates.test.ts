import { i } from '../rotates'
import { TestEmulator } from './utils'

describe('rotates', () => {
  describe('RLCA', () => {
    test('carry flag', () => {
      const e = new TestEmulator()

      e.cpu.set('A', 0b1001_0110)
      const [, RLCA] = i[0x07]
      RLCA(e)
      expect(e.cpu.get('A')).toEqual(0b0010_1101)
      expect(e.cpu.getFlag('C')).toEqual(1)
    })
  })
})

import { IMMU, TickableEmulator } from '../../types'
import { Cpu } from '../../Cpu'
import { InterruptController } from '../../InterruptController'

export class TestMMU implements IMMU {
  accepts = () => true

  memory = new Uint8Array(0xffff + 1)

  loadCartridge = () => null

  readByte(address: number) {
    return this.memory[address]
  }

  readWord(address: number) {
    return (this.readByte(address + 1) << 8) + this.readByte(address)
  }

  writeByte(address: number, value: number) {
    this.memory[address] = value
  }

  writeWord(address: number, value: number) {
    this.writeByte(address, value & 0xff)
    this.writeByte(address + 1, value >> 8)
  }
}

export class TestEmulator implements TickableEmulator {
  mmu = new TestMMU()

  cpu = new Cpu()

  ticks = 0

  cyclesCount = 4

  tick = () => {
    this.ticks++
    this.cyclesCount += 4
  }

  interruptController = new InterruptController()

  readU8Tick() {
    const value = this.readByteTick(this.cpu.getPC())
    this.cpu.incrementPC()
    return value
  }

  readU16Tick() {
    const value = this.readWordTick(this.cpu.getPC())
    this.cpu.incrementPC()
    this.cpu.incrementPC()
    return value
  }

  readByteTick(address: number) {
    const value = this.mmu.readByte(address)
    this.tick()
    return value
  }

  readWordTick(address: number) {
    const lower = this.readByteTick(address)
    const upper = this.readByteTick(address + 1) << 8
    return lower + upper
  }

  writeByteTick(address: number, value: number) {
    this.mmu.writeByte(address, value)
    this.tick()
  }

  writeWordTick(address: number, value: number) {
    this.writeByteTick(address, value & 0xff)
    this.writeByteTick(address + 1, value >> 8)
  }
}

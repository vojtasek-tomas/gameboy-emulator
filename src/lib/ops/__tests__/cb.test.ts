import { BIT_I_R, RL_R, SRA_R } from '../cb'
import { TestEmulator } from './utils'

describe('cb', () => {
  describe('BIT_I_R', () => {
    test('set Z if bit is zero', () => {
      const e = new TestEmulator()

      e.cpu.set('A', 0x00)
      BIT_I_R(7, 'A')(e)
      expect(e.cpu.getFlag('Z')).toEqual(1)
    })

    test('set Z if bit is not zero', () => {
      const e = new TestEmulator()

      e.cpu.set('A', 0xff)
      BIT_I_R(7, 'A')(e)
      expect(e.cpu.getFlag('Z')).toEqual(0)
    })
  })

  describe('RL_R', () => {
    test('new carry shift', () => {
      const e = new TestEmulator()
      e.cpu.set('A', 0xff)
      RL_R('A')(e)
      expect(e.cpu.get('A')).toEqual(0xfe)
      expect(e.cpu.getFlag('C')).toEqual(1)
    })

    test('old carry usage', () => {
      const e = new TestEmulator()
      e.cpu.set('A', 0x00).setFlag('C')
      RL_R('A')(e)
      expect(e.cpu.get('A')).toEqual(0x01)
      expect(e.cpu.getFlag('C')).toEqual(0)
    })
  })

  describe('SRA_R', () => {
    test('carry shift', () => {
      const e = new TestEmulator()
      e.cpu.set('A', 0b10100111)
      SRA_R('A')(e)
      expect(e.cpu.get('A')).toEqual(0b11010011)
      expect(e.cpu.getFlag('C')).toEqual(1)
    })
  })
})

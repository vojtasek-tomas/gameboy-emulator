import { INC_RR, DEC_RR } from '../alu16'
import { Emulator } from '../../Emulator'
import { MemoryDisplay } from '../../ppu/displays/MemoryDisplay'

function createEmulator() {
  return new Emulator(new MemoryDisplay())
}

describe('alu16', () => {
  describe('INC nn', () => {
    test('overflow increment', () => {
      const e = createEmulator()

      e.cpu.set('BC', 0xffff)
      INC_RR('BC')(e)
      expect(e.cpu.get('BC')).toEqual(0x00)
      expect(e.cpu.getFlag('Z')).toEqual(0)
    })

    test('standard increment', () => {
      const e = createEmulator()

      e.cpu.set('BC', 0x0001)
      INC_RR('BC')(e)
      expect(e.cpu.get('BC')).toEqual(0x0002)
    })
  })

  describe('DEC nn', () => {
    test('overflow decrement', () => {
      const e = createEmulator()

      e.cpu.set('BC', 0x0000)
      DEC_RR('BC')(e)
      expect(e.cpu.get('BC')).toEqual(0xffff)
      expect(e.cpu.getFlag('Z')).toEqual(0)
    })

    test('standard decrement', () => {
      const e = createEmulator()

      e.cpu.set('BC', 0x0001)
      DEC_RR('BC')(e)
      expect(e.cpu.get('BC')).toEqual(0x0000)
      expect(e.cpu.getFlag('Z')).toEqual(0)
    })
  })
})

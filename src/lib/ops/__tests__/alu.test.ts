import { adc, ADD_A_R, OR_A_R, INC_R, SUB_A_R, CP_R } from '../alu'
import { Emulator } from '../../Emulator'
import { MemoryDisplay } from '../../ppu/displays/MemoryDisplay'

function createEmulator() {
  return new Emulator(new MemoryDisplay())
}

describe('ALU', () => {
  describe('ADD A,n', () => {
    test('adding', () => {
      const e = createEmulator()

      e.cpu.set('A', 0x0f).set('C', 0xf0)

      ADD_A_R('C')(e)
      expect(e.cpu.get('A')).toEqual(0xff)
    })

    test('carry flag', () => {
      const e = createEmulator()

      e.cpu.set('A', 0xff).set('C', 0x02)

      ADD_A_R('C')(e)
      expect(e.cpu.getFlag('C')).toEqual(1)
      expect(e.cpu.get('A')).toEqual(1)
    })

    test('zero flag', () => {
      const e = createEmulator()

      e.cpu.set('A', 0xff).set('C', 0x01)

      ADD_A_R('C')(e)
      expect(e.cpu.getFlag('Z')).toEqual(1)
      expect(e.cpu.get('A')).toEqual(0)
    })
  })

  describe('CP_R', () => {
    test('carry flag', () => {
      const e = createEmulator()

      e.cpu.set('A', 0x01).set('C', 0x02)

      CP_R('C')(e)
      expect(e.cpu.getFlag('C')).toEqual(1)

      e.cpu.set('A', 0x02).set('C', 0x01)

      CP_R('C')(e)
      expect(e.cpu.getFlag('C')).toEqual(0)
    })
  })

  describe('INC n', () => {
    test('overflow increment', () => {
      const e = createEmulator()

      e.cpu.set('A', 0xff)
      INC_R('A')(e)
      expect(e.cpu.get('A')).toEqual(0x00)
      expect(e.cpu.getFlag('Z')).toEqual(1)
      expect(e.cpu.getFlag('H')).toEqual(1)
    })

    test('half carry', () => {
      const e = createEmulator()
      e.cpu.set('A', 0x0f)
      INC_R('A')(e)
      expect(e.cpu.get('A')).toEqual(0x10)
      expect(e.cpu.getFlag('Z')).toEqual(0)
      expect(e.cpu.getFlag('H')).toEqual(1)
    })
  })

  describe('OR n', () => {
    test('binary or', () => {
      const e = createEmulator()

      e.cpu.set('A', 0xff).set('C', 0x01)

      OR_A_R('C')(e)
      expect(e.cpu.get('A')).toEqual(0xff)
    })

    test('zero flag', () => {
      const e = createEmulator()

      e.cpu.set('A', 0x00).set('C', 0x00)

      OR_A_R('C')(e)
      expect(e.cpu.get('A')).toEqual(0x00)
      expect(e.cpu.getFlag('Z')).toEqual(1)
    })
  })

  describe('SUB n', () => {
    test('correct result', () => {
      const e = createEmulator()
      e.cpu.set('A', 0xff)
      SUB_A_R('A')(e)
      expect(e.cpu.get('A')).toEqual(0x00)
      expect(e.cpu.getFlag('Z')).toEqual(1)
    })

    test('different registers', () => {
      const e = createEmulator()
      e.cpu.set('A', 0xff)
      e.cpu.set('B', 0x0f)
      SUB_A_R('B')(e)
      expect(e.cpu.get('A')).toEqual(0xf0)
      expect(e.cpu.getFlag('Z')).toEqual(0)
    })
  })

  describe('ADC n', () => {
    test('correct result', () => {
      const e = createEmulator()
      e.cpu.set('A', 0xff)
      adc(e.cpu, 1)
      expect(e.cpu.get('A')).toEqual(0x00)
      expect(e.cpu.getFlag('Z')).toEqual(1)
      expect(e.cpu.getFlag('H')).toEqual(1)
      expect(e.cpu.getFlag('C')).toEqual(1)
      expect(e.cpu.getFlag('N')).toEqual(0)
    })
  })
})

import { CommandDefinition, TickableEmulator } from '../types'

export const i: CommandDefinition[] = []

/**
 * 1. RST n
 *
 * Push present address onto stack.
 * Jump to address $0000 + n.
 *
 * Use with:
 * n = $00,$08,$10,$18,$20,$28,$30,$38
 */
function RST_N(n: number) {
  return (e: TickableEmulator) => {
    e.tick() // internal
    e.cpu.incrementSP(-2)
    e.writeWordTick(e.cpu.getSP(), e.cpu.getPC())
    e.cpu.setPC(n)
  }
}

i[0xc7] = ['RST 00h', RST_N(0x00)]
i[0xcf] = ['RST 08h', RST_N(0x08)]
i[0xd7] = ['RST 10h', RST_N(0x10)]
i[0xdf] = ['RST 18h', RST_N(0x18)]
i[0xe7] = ['RST 20h', RST_N(0x20)]
i[0xef] = ['RST 28h', RST_N(0x28)]
i[0xf7] = ['RST 30h', RST_N(0x30)]
i[0xff] = ['RST 38h', RST_N(0x38)]

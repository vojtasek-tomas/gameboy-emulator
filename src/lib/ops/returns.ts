import { CommandDefinition, TickableEmulator } from '../types'

export const i: CommandDefinition[] = []

function ret(e: TickableEmulator) {
  const value = e.readWordTick(e.cpu.getSP())
  e.tick() // internal
  e.cpu.setPC(value)
  e.cpu.incrementSP(2)
}

/**
 * 1. RET
 *
 * Pop two bytes from stack & jump to that address.
 */
i[0xc9] = ['RET', ret]

/**
 * 2. RET cc
 *
 * Return if following condition is true:
 *
 * Use with:
 * cc = NZ, Return if Z flag is reset.
 * cc = Z, Return if Z flag is set.
 * cc = NC, Return if C flag is reset.
 * cc = C, Return if C flag is set.
 */
i[0xc0] = [
  'RET NZ',
  (e: TickableEmulator) => {
    e.tick() // internal
    if (!e.cpu.getFlag('Z')) {
      ret(e)
    }
  }
]
i[0xc8] = [
  'RET Z',
  (e: TickableEmulator) => {
    e.tick() // internal
    if (e.cpu.getFlag('Z')) {
      ret(e)
    }
  }
]
i[0xd0] = [
  'RET NC',
  (e: TickableEmulator) => {
    e.tick() // internal
    if (!e.cpu.getFlag('C')) {
      ret(e)
    }
  }
]
i[0xd8] = [
  'RET C',
  (e: TickableEmulator) => {
    e.tick() // internal
    if (e.cpu.getFlag('C')) {
      ret(e)
    }
  }
]

/**
 * 3. RETI
 * Pop two bytes from stack & jump to that address then enable interrupts.
 */
i[0xd9] = [
  'RETI',
  (e: TickableEmulator) => {
    ret(e)
    e.cpu.ime = true
  }
]

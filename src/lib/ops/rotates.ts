import { CommandDefinition, TickableEmulator } from '../types'
import { getBit } from '../binaryUtils'

export const i: CommandDefinition[] = []

/**
 * 1. RLCA
 *
 * Rotate A left. Old bit 7 to Carry flag
 *
 * Flags affected:
 * Z - Reset.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 7 data.
 */
i[0x07] = [
  'RLCA',
  (e: TickableEmulator) => {
    const registerValue = e.cpu.get('A')
    const old7Bit = getBit(registerValue, 7)
    const result = (registerValue << 1) | old7Bit
    e.cpu.clearFlags()
    if (old7Bit) {
      e.cpu.setFlag('C')
    }
    e.cpu.set('A', result)
  }
]

/**
 * 2. RLA
 *
 * Description:
 * Rotate A left through Carry flag.
 *
 * Flags affected:
 * Z - Reset.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 7 data.
 */
i[0x17] = [
  'RLA',
  (e: TickableEmulator) => {
    const registerValue = e.cpu.get('A')
    const old7Bit = getBit(registerValue, 7)
    const result = (registerValue << 1) | e.cpu.getFlag('C')

    e.cpu.clearFlags()
    if (old7Bit) {
      e.cpu.setFlag('C')
    }
    e.cpu.set('A', result)
  }
]

/**
 * 3. RRCA
 *
 * Rotate A right. Old bit 0 to Carry flag
 *
 * Flags affected:
 * Z - Reset.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 0 data.
 */
i[0x0f] = [
  'RRCA',
  (e: TickableEmulator) => {
    const registerValue = e.cpu.get('A')
    const old0Bit = getBit(registerValue, 0)
    const result = (registerValue >> 1) | (old0Bit << 7)
    e.cpu.clearFlags()
    if (old0Bit) {
      e.cpu.setFlag('C')
    }
    e.cpu.set('A', result)
  }
]

/**
 * 4. RRA
 *
 * Description:
 * Rotate A right through Carry flag.
 *
 * Flags affected:
 * Z - Reset.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 0 data.
 */
i[0x1f] = [
  'RRA',
  (e: TickableEmulator) => {
    const registerValue = e.cpu.get('A')
    const old0Bit = getBit(registerValue, 0)

    const result = (registerValue >> 1) | (e.cpu.getFlag('C') << 7)

    e.cpu.clearFlags()
    if (old0Bit) {
      e.cpu.setFlag('C')
    }
    e.cpu.set('A', result)
  }
]

import { getBit, updateBit, setBit, clearBit } from '../binaryUtils'
import {
  ICpu,
  SimpleRegisters,
  CommandDefinition,
  TickableEmulator
} from '../types'

export const i: CommandDefinition[] = []

/**
 * 1. BIT b,r
 * Test bit b in register r
 *
 * Use with:
 * b = 0 - 7, r = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if bit b of register r is 0
 * N - Reset
 * H - Set
 * C - Not affected
 */
function bit(cpu: ICpu, bitIndex: number, value: number) {
  const b = getBit(value, bitIndex)
  if (b === 0) {
    cpu.setFlag('Z')
  } else {
    cpu.clearFlag('Z')
  }
  cpu.setFlag('H').clearFlag('N')
}

export function BIT_I_R(bitIndex: number, R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    bit(e.cpu, bitIndex, e.cpu.get(R))
  }
}

export function BIT_I_$HL(bitIndex: number) {
  return (e: TickableEmulator) => {
    bit(e.cpu, bitIndex, e.readByteTick(e.cpu.get('HL')))
  }
}

i[0x40] = ['BIT 0,B', BIT_I_R(0, 'B')]
i[0x41] = ['BIT 0,C', BIT_I_R(0, 'C')]
i[0x42] = ['BIT 0,D', BIT_I_R(0, 'D')]
i[0x43] = ['BIT 0,E', BIT_I_R(0, 'E')]
i[0x44] = ['BIT 0,H', BIT_I_R(0, 'H')]
i[0x45] = ['BIT 0,L', BIT_I_R(0, 'L')]
i[0x46] = ['BIT 0,(HL)', BIT_I_$HL(0)]
i[0x47] = ['BIT 0,A', BIT_I_R(0, 'A')]

i[0x48] = ['BIT 1,B', BIT_I_R(1, 'B')]
i[0x49] = ['BIT 1,C', BIT_I_R(1, 'C')]
i[0x4a] = ['BIT 1,D', BIT_I_R(1, 'D')]
i[0x4b] = ['BIT 1,E', BIT_I_R(1, 'E')]
i[0x4c] = ['BIT 1,H', BIT_I_R(1, 'H')]
i[0x4d] = ['BIT 1,L', BIT_I_R(1, 'L')]
i[0x4e] = ['BIT 1,(HL)', BIT_I_$HL(1)]
i[0x4f] = ['BIT 1,A', BIT_I_R(1, 'A')]

i[0x50] = ['BIT 2,B', BIT_I_R(2, 'B')]
i[0x51] = ['BIT 2,C', BIT_I_R(2, 'C')]
i[0x52] = ['BIT 2,D', BIT_I_R(2, 'D')]
i[0x53] = ['BIT 2,E', BIT_I_R(2, 'E')]
i[0x54] = ['BIT 2,H', BIT_I_R(2, 'H')]
i[0x55] = ['BIT 2,L', BIT_I_R(2, 'L')]
i[0x56] = ['BIT 2,(HL)', BIT_I_$HL(2)]
i[0x57] = ['BIT 2,A', BIT_I_R(2, 'A')]

i[0x58] = ['BIT 3,B', BIT_I_R(3, 'B')]
i[0x59] = ['BIT 3,C', BIT_I_R(3, 'C')]
i[0x5a] = ['BIT 3,D', BIT_I_R(3, 'D')]
i[0x5b] = ['BIT 3,E', BIT_I_R(3, 'E')]
i[0x5c] = ['BIT 3,H', BIT_I_R(3, 'H')]
i[0x5d] = ['BIT 3,L', BIT_I_R(3, 'L')]
i[0x5e] = ['BIT 3,(HL)', BIT_I_$HL(3)]
i[0x5f] = ['BIT 3,A', BIT_I_R(3, 'A')]

i[0x60] = ['BIT 4,B', BIT_I_R(4, 'B')]
i[0x61] = ['BIT 4,C', BIT_I_R(4, 'C')]
i[0x62] = ['BIT 4,D', BIT_I_R(4, 'D')]
i[0x63] = ['BIT 4,E', BIT_I_R(4, 'E')]
i[0x64] = ['BIT 4,H', BIT_I_R(4, 'H')]
i[0x65] = ['BIT 4,L', BIT_I_R(4, 'L')]
i[0x66] = ['BIT 4,(HL)', BIT_I_$HL(4)]
i[0x67] = ['BIT 4,A', BIT_I_R(4, 'A')]

i[0x68] = ['BIT 5,B', BIT_I_R(5, 'B')]
i[0x69] = ['BIT 5,C', BIT_I_R(5, 'C')]
i[0x6a] = ['BIT 5,D', BIT_I_R(5, 'D')]
i[0x6b] = ['BIT 5,E', BIT_I_R(5, 'E')]
i[0x6c] = ['BIT 5,H', BIT_I_R(5, 'H')]
i[0x6d] = ['BIT 5,L', BIT_I_R(5, 'L')]
i[0x6e] = ['BIT 5,(HL)', BIT_I_$HL(5)]
i[0x6f] = ['BIT 5,A', BIT_I_R(5, 'A')]

i[0x70] = ['BIT 6,B', BIT_I_R(6, 'B')]
i[0x71] = ['BIT 6,C', BIT_I_R(6, 'C')]
i[0x72] = ['BIT 6,D', BIT_I_R(6, 'D')]
i[0x73] = ['BIT 6,E', BIT_I_R(6, 'E')]
i[0x74] = ['BIT 6,H', BIT_I_R(6, 'H')]
i[0x75] = ['BIT 6,L', BIT_I_R(6, 'L')]
i[0x76] = ['BIT 6,(HL)', BIT_I_$HL(6)]
i[0x77] = ['BIT 6,A', BIT_I_R(6, 'A')]

i[0x78] = ['BIT 7,B', BIT_I_R(7, 'B')]
i[0x79] = ['BIT 7,C', BIT_I_R(7, 'C')]
i[0x7a] = ['BIT 7,D', BIT_I_R(7, 'D')]
i[0x7b] = ['BIT 7,E', BIT_I_R(7, 'E')]
i[0x7c] = ['BIT 7,H', BIT_I_R(7, 'H')]
i[0x7e] = ['BIT 7,(HL)', BIT_I_$HL(7)]
i[0x7d] = ['BIT 7,L', BIT_I_R(7, 'L')]
i[0x7f] = ['BIT 7,A', BIT_I_R(7, 'A')]

/**
 * 2. SET b,r
 *
 * Set bit b in register r.
 *
 * Use with:
 * b = 0 - 7, r = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * None.
 */
function SET_b_r(bitIndex: number, R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const value = e.cpu.get(R)
    e.cpu.set(R, setBit(value, bitIndex))
  }
}

i[0xc0] = ['SET 0,B', SET_b_r(0, 'B')]
i[0xc1] = ['SET 0,C', SET_b_r(0, 'C')]
i[0xc2] = ['SET 0,D', SET_b_r(0, 'D')]
i[0xc3] = ['SET 0,E', SET_b_r(0, 'E')]
i[0xc4] = ['SET 0,H', SET_b_r(0, 'H')]
i[0xc5] = ['SET 0,L', SET_b_r(0, 'L')]
i[0xc7] = ['SET 0,A', SET_b_r(0, 'A')]

i[0xc8] = ['SET 1,B', SET_b_r(1, 'B')]
i[0xc9] = ['SET 1,C', SET_b_r(1, 'C')]
i[0xca] = ['SET 1,D', SET_b_r(1, 'D')]
i[0xcb] = ['SET 1,E', SET_b_r(1, 'E')]
i[0xcc] = ['SET 1,H', SET_b_r(1, 'H')]
i[0xcd] = ['SET 1,L', SET_b_r(1, 'L')]
i[0xcf] = ['SET 1,A', SET_b_r(1, 'A')]

i[0xd0] = ['SET 2,B', SET_b_r(2, 'B')]
i[0xd1] = ['SET 2,C', SET_b_r(2, 'C')]
i[0xd2] = ['SET 2,D', SET_b_r(2, 'D')]
i[0xd3] = ['SET 2,E', SET_b_r(2, 'E')]
i[0xd4] = ['SET 2,H', SET_b_r(2, 'H')]
i[0xd5] = ['SET 2,L', SET_b_r(2, 'L')]
i[0xd7] = ['SET 2,A', SET_b_r(2, 'A')]

i[0xd8] = ['SET 3,B', SET_b_r(3, 'B')]
i[0xd9] = ['SET 3,C', SET_b_r(3, 'C')]
i[0xda] = ['SET 3,D', SET_b_r(3, 'D')]
i[0xdb] = ['SET 3,E', SET_b_r(3, 'E')]
i[0xdc] = ['SET 3,H', SET_b_r(3, 'H')]
i[0xdd] = ['SET 3,L', SET_b_r(3, 'L')]
i[0xdf] = ['SET 3,A', SET_b_r(3, 'A')]

i[0xe0] = ['SET 4,B', SET_b_r(4, 'B')]
i[0xe1] = ['SET 4,C', SET_b_r(4, 'C')]
i[0xe2] = ['SET 4,D', SET_b_r(4, 'D')]
i[0xe3] = ['SET 4,E', SET_b_r(4, 'E')]
i[0xe4] = ['SET 4,H', SET_b_r(4, 'H')]
i[0xe5] = ['SET 4,L', SET_b_r(4, 'L')]
i[0xe7] = ['SET 4,A', SET_b_r(4, 'A')]

i[0xe8] = ['SET 5,B', SET_b_r(5, 'B')]
i[0xe9] = ['SET 5,C', SET_b_r(5, 'C')]
i[0xea] = ['SET 5,D', SET_b_r(5, 'D')]
i[0xeb] = ['SET 5,E', SET_b_r(5, 'E')]
i[0xec] = ['SET 5,H', SET_b_r(5, 'H')]
i[0xed] = ['SET 5,L', SET_b_r(5, 'L')]
i[0xef] = ['SET 5,A', SET_b_r(5, 'A')]

i[0xf0] = ['SET 6,B', SET_b_r(6, 'B')]
i[0xf1] = ['SET 6,C', SET_b_r(6, 'C')]
i[0xf2] = ['SET 6,D', SET_b_r(6, 'D')]
i[0xf3] = ['SET 6,E', SET_b_r(6, 'E')]
i[0xf4] = ['SET 6,H', SET_b_r(6, 'H')]
i[0xf5] = ['SET 6,L', SET_b_r(6, 'L')]
i[0xf7] = ['SET 6,A', SET_b_r(6, 'A')]

i[0xf8] = ['SET 7,B', SET_b_r(7, 'B')]
i[0xf9] = ['SET 7,C', SET_b_r(7, 'C')]
i[0xfa] = ['SET 7,D', SET_b_r(7, 'D')]
i[0xfb] = ['SET 7,E', SET_b_r(7, 'E')]
i[0xfc] = ['SET 7,H', SET_b_r(7, 'H')]
i[0xfd] = ['SET 7,L', SET_b_r(7, 'L')]
i[0xff] = ['SET 7,A', SET_b_r(7, 'A')]

function SET_b_$HL(bitIndex: number) {
  return (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const value = e.readByteTick(address)
    e.writeByteTick(address, setBit(value, bitIndex))
  }
}

i[0xc6] = ['SET 0,(HL)', SET_b_$HL(0)]
i[0xce] = ['SET 1,(HL)', SET_b_$HL(1)]
i[0xd6] = ['SET 2,(HL)', SET_b_$HL(2)]
i[0xde] = ['SET 3,(HL)', SET_b_$HL(3)]
i[0xe6] = ['SET 4,(HL)', SET_b_$HL(4)]
i[0xee] = ['SET 5,(HL)', SET_b_$HL(5)]
i[0xf6] = ['SET 6,(HL)', SET_b_$HL(6)]
i[0xfe] = ['SET 7,(HL)', SET_b_$HL(7)]

/**
 * 3. RES b,r
 *
 * Reset bit b in register r.
 *
 * Use with:
 * b = 0 - 7, r = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * None.
 */
function RES_b_r(bitIndex: number, R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const value = e.cpu.get(R)
    e.cpu.set(R, clearBit(value, bitIndex))
  }
}

i[0x80] = ['RES 0,B', RES_b_r(0, 'B')]
i[0x81] = ['RES 0,C', RES_b_r(0, 'C')]
i[0x82] = ['RES 0,D', RES_b_r(0, 'D')]
i[0x83] = ['RES 0,E', RES_b_r(0, 'E')]
i[0x84] = ['RES 0,H', RES_b_r(0, 'H')]
i[0x85] = ['RES 0,L', RES_b_r(0, 'L')]
i[0x87] = ['RES 0,A', RES_b_r(0, 'A')]

i[0x88] = ['RES 1,B', RES_b_r(1, 'B')]
i[0x89] = ['RES 1,C', RES_b_r(1, 'C')]
i[0x8a] = ['RES 1,D', RES_b_r(1, 'D')]
i[0x8b] = ['RES 1,E', RES_b_r(1, 'E')]
i[0x8c] = ['RES 1,H', RES_b_r(1, 'H')]
i[0x8d] = ['RES 1,L', RES_b_r(1, 'L')]
i[0x8f] = ['RES 1,A', RES_b_r(1, 'A')]

i[0x90] = ['RES 2,B', RES_b_r(2, 'B')]
i[0x91] = ['RES 2,C', RES_b_r(2, 'C')]
i[0x92] = ['RES 2,D', RES_b_r(2, 'D')]
i[0x93] = ['RES 2,E', RES_b_r(2, 'E')]
i[0x94] = ['RES 2,H', RES_b_r(2, 'H')]
i[0x95] = ['RES 2,L', RES_b_r(2, 'L')]
i[0x97] = ['RES 2,A', RES_b_r(2, 'A')]

i[0x98] = ['RES 3,B', RES_b_r(3, 'B')]
i[0x99] = ['RES 3,C', RES_b_r(3, 'C')]
i[0x9a] = ['RES 3,D', RES_b_r(3, 'D')]
i[0x9b] = ['RES 3,E', RES_b_r(3, 'E')]
i[0x9c] = ['RES 3,H', RES_b_r(3, 'H')]
i[0x9d] = ['RES 3,L', RES_b_r(3, 'L')]
i[0x9f] = ['RES 3,A', RES_b_r(3, 'A')]

i[0xa0] = ['RES 4,B', RES_b_r(4, 'B')]
i[0xa1] = ['RES 4,C', RES_b_r(4, 'C')]
i[0xa2] = ['RES 4,D', RES_b_r(4, 'D')]
i[0xa3] = ['RES 4,E', RES_b_r(4, 'E')]
i[0xa4] = ['RES 4,H', RES_b_r(4, 'H')]
i[0xa5] = ['RES 4,L', RES_b_r(4, 'L')]
i[0xa7] = ['RES 4,A', RES_b_r(4, 'A')]

i[0xa8] = ['RES 5,B', RES_b_r(5, 'B')]
i[0xa9] = ['RES 5,C', RES_b_r(5, 'C')]
i[0xaa] = ['RES 5,D', RES_b_r(5, 'D')]
i[0xab] = ['RES 5,E', RES_b_r(5, 'E')]
i[0xac] = ['RES 5,H', RES_b_r(5, 'H')]
i[0xad] = ['RES 5,L', RES_b_r(5, 'L')]
i[0xaf] = ['RES 5,A', RES_b_r(5, 'A')]

i[0xb0] = ['RES 6,B', RES_b_r(6, 'B')]
i[0xb1] = ['RES 6,C', RES_b_r(6, 'C')]
i[0xb2] = ['RES 6,D', RES_b_r(6, 'D')]
i[0xb3] = ['RES 6,E', RES_b_r(6, 'E')]
i[0xb4] = ['RES 6,H', RES_b_r(6, 'H')]
i[0xb5] = ['RES 6,L', RES_b_r(6, 'L')]
i[0xb7] = ['RES 6,A', RES_b_r(6, 'A')]

i[0xb8] = ['RES 7,B', RES_b_r(7, 'B')]
i[0xb9] = ['RES 7,C', RES_b_r(7, 'C')]
i[0xba] = ['RES 7,D', RES_b_r(7, 'D')]
i[0xbb] = ['RES 7,E', RES_b_r(7, 'E')]
i[0xbc] = ['RES 7,H', RES_b_r(7, 'H')]
i[0xbd] = ['RES 7,L', RES_b_r(7, 'L')]
i[0xbf] = ['RES 7,A', RES_b_r(7, 'A')]

function RES_b_$HL(bitIndex: number) {
  return (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const value = e.readByteTick(address)
    e.writeByteTick(address, clearBit(value, bitIndex))
  }
}

i[0x86] = ['RES 0,(HL)', RES_b_$HL(0)]
i[0x8e] = ['RES 1,(HL)', RES_b_$HL(1)]
i[0x96] = ['RES 2,(HL)', RES_b_$HL(2)]
i[0x9e] = ['RES 3,(HL)', RES_b_$HL(3)]
i[0xa6] = ['RES 4,(HL)', RES_b_$HL(4)]
i[0xae] = ['RES 5,(HL)', RES_b_$HL(5)]
i[0xb6] = ['RES 6,(HL)', RES_b_$HL(6)]
i[0xbe] = ['RES 7,(HL)', RES_b_$HL(7)]

/**
 * 6. RLC n
 *
 * Rotate n left. Old bit 7 to Carry flag
 * BIT REUSE!
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 7 data.
 */
function rlc(cpu: ICpu, value: number) {
  const old7Bit = getBit(value, 7)
  const result = (value << 1) | old7Bit

  cpu.clearFlags()
  if (old7Bit) {
    cpu.setFlag('C')
  }
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  }
  return result
}

export function RLC_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const result = rlc(e.cpu, e.cpu.get(R))
    e.cpu.set(R, result)
  }
}

i[0x07] = ['RLC A', RLC_R('A')]
i[0x00] = ['RLC B', RLC_R('B')]
i[0x01] = ['RLC C', RLC_R('C')]
i[0x02] = ['RLC D', RLC_R('D')]
i[0x03] = ['RLC E', RLC_R('E')]
i[0x04] = ['RLC H', RLC_R('H')]
i[0x05] = ['RLC L', RLC_R('L')]
i[0x06] = [
  'RLC (HL)',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const result = rlc(e.cpu, e.readByteTick(address))
    e.writeByteTick(address, result)
  }
]

/**
 * 7. RRC n
 *
 * Rotate n right. Old bit 0 to Carry flag
 * BIT REUSE!
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 0 data.
 */
function rrc(cpu: ICpu, value: number) {
  const old0Bit = getBit(value, 0)
  const result = (value >> 1) | (old0Bit << 7)

  cpu.clearFlags()
  if (old0Bit) {
    cpu.setFlag('C')
  }
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  }
  return result
}

export function RRC_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const result = rrc(e.cpu, e.cpu.get(R))
    e.cpu.set(R, result)
  }
}

i[0x0f] = ['RRC A', RRC_R('A')]
i[0x08] = ['RRC B', RRC_R('B')]
i[0x09] = ['RRC C', RRC_R('C')]
i[0x0a] = ['RRC D', RRC_R('D')]
i[0x0b] = ['RRC E', RRC_R('E')]
i[0x0c] = ['RRC H', RRC_R('H')]
i[0x0d] = ['RRC L', RRC_R('L')]
i[0x0e] = [
  'RRC (HL)',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const result = rrc(e.cpu, e.readByteTick(address))
    e.writeByteTick(address, result)
  }
]

/**
 * 6. RL n
 *
 * Rotate n left through Carry flag
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 7 data.
 */
function rl(cpu: ICpu, value: number) {
  const old7Bit = getBit(value, 7)
  const result = (value << 1) | cpu.getFlag('C')
  cpu.clearFlags()
  if (old7Bit) {
    cpu.setFlag('C')
  }
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  }
  return result
}

export function RL_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const result = rl(e.cpu, e.cpu.get(R))
    e.cpu.set(R, result)
  }
}

i[0x17] = ['RL A', RL_R('A')]
i[0x10] = ['RL B', RL_R('B')]
i[0x11] = ['RL C', RL_R('C')]
i[0x12] = ['RL D', RL_R('D')]
i[0x13] = ['RL E', RL_R('E')]
i[0x14] = ['RL H', RL_R('H')]
i[0x15] = ['RL L', RL_R('L')]
i[0x16] = [
  'RL (HL)',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const result = rl(e.cpu, e.readByteTick(address))
    e.writeByteTick(address, result)
  }
]

/**
 * 8. RR n
 *
 * Description:
 * Rotate n right through Carry flag.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 0 data.
 */

function rr(cpu: ICpu, value: number) {
  const result = (value >> 1) | (cpu.getFlag('C') << 7)
  cpu.clearFlags()
  if (result === 0) {
    cpu.setFlag('Z')
  }
  if (value & 1) {
    cpu.setFlag('C')
  }
  return result
}

export function RR_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const value = rr(e.cpu, e.cpu.get(R))
    e.cpu.set(R, value)
  }
}

i[0x1f] = ['RR A', RR_R('A')]
i[0x18] = ['RR B', RR_R('B')]
i[0x19] = ['RR C', RR_R('C')]
i[0x1a] = ['RR D', RR_R('D')]
i[0x1b] = ['RR E', RR_R('E')]
i[0x1c] = ['RR H', RR_R('H')]
i[0x1d] = ['RR L', RR_R('L')]
i[0x1e] = [
  'RR (HL)',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const value = e.readByteTick(address)
    const result = rr(e.cpu, value)
    e.writeByteTick(address, result)
  }
]

/**
 * 9. SLA n
 *
 * Shift n left into Carry. LSB of n set to 0.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 7 data.
 */
function sla(cpu: ICpu, value: number) {
  const old7Bit = getBit(value, 7)

  const result = value << 1

  cpu.clearFlags()
  if (old7Bit) {
    cpu.setFlag('C')
  }
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  }
  return result
}

function SLA_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const result = sla(e.cpu, e.cpu.get(R))
    e.cpu.set(R, result)
  }
}

i[0x27] = ['SLA A', SLA_R('A')]
i[0x20] = ['SLA B', SLA_R('B')]
i[0x21] = ['SLA C', SLA_R('C')]
i[0x22] = ['SLA D', SLA_R('D')]
i[0x23] = ['SLA E', SLA_R('E')]
i[0x24] = ['SLA H', SLA_R('H')]
i[0x25] = ['SLA L', SLA_R('L')]
i[0x26] = [
  'SLA (HL)',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const result = sla(e.cpu, e.readByteTick(address))
    e.writeByteTick(address, result)
  }
]

/**
 * 10. SRA n
 *
 * Shift n right into Carry. MSB doesn't change.
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * https://chortle.ccsu.edu/assemblytutorial/Chapter-14/ass14_13.html
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 0 data.
 */
function sra(cpu: ICpu, value: number) {
  const old0Bit = getBit(value, 0)
  const old7Bit = getBit(value, 7)

  const result = updateBit(value >> 1, 7, old7Bit)

  cpu.clearFlags()
  if (old0Bit) {
    cpu.setFlag('C')
  }
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  }
  return result
}

export function SRA_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const result = sra(e.cpu, e.cpu.get(R))
    e.cpu.set(R, result)
  }
}

i[0x2f] = ['SRA A', SRA_R('A')]
i[0x28] = ['SRA B', SRA_R('B')]
i[0x29] = ['SRA C', SRA_R('C')]
i[0x2a] = ['SRA D', SRA_R('D')]
i[0x2b] = ['SRA E', SRA_R('E')]
i[0x2c] = ['SRA H', SRA_R('H')]
i[0x2d] = ['SRA L', SRA_R('L')]
i[0x2e] = [
  'SRA (HL)',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const result = sra(e.cpu, e.readByteTick(address))
    e.writeByteTick(address, result)
  }
]

/**
 * 11. SRL n
 *
 * Description:
 * Shift n right into Carry. MSB set to 0
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Reset.
 * C - Contains old bit 0 data.
 */
function srl(cpu: ICpu, value: number) {
  const result = value >> 1
  cpu.clearFlags()
  if (value & 1) {
    cpu.setFlag('C')
  }
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  }
  return result
}

function SRL_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const value = e.cpu.get(R)
    const result = srl(e.cpu, value)
    e.cpu.set(R, result)
  }
}

i[0x3f] = ['SRL A', SRL_R('A')]
i[0x38] = ['SRL B', SRL_R('B')]
i[0x39] = ['SRL C', SRL_R('C')]
i[0x3a] = ['SRL D', SRL_R('D')]
i[0x3b] = ['SRL E', SRL_R('E')]
i[0x3c] = ['SRL H', SRL_R('H')]
i[0x3d] = ['SRL L', SRL_R('L')]
i[0x3e] = [
  'SRL (HL)',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const value = e.readByteTick(address)
    const result = srl(e.cpu, value)
    e.writeByteTick(address, result)
  }
]

/**
 * 1. SWAP n
 *
 * Swap upper & lower nibles of n.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Reset.
 * C - Reset.
 */
function swap(cpu: ICpu, value: number) {
  const high = (value << 4) & 0xff
  const low = value >> 4
  const result = high | low
  cpu.clearFlags()
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  }
  return result
}
function SWAP_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const val = e.cpu.get(R)
    const result = swap(e.cpu, val)
    e.cpu.set(R, result)
  }
}

function SWAP_$HL() {
  return (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const result = swap(e.cpu, e.readByteTick(address))
    e.writeByteTick(address, result)
  }
}

i[0x37] = ['SWAP A', SWAP_R('A')]
i[0x30] = ['SWAP B', SWAP_R('B')]
i[0x31] = ['SWAP C', SWAP_R('C')]
i[0x32] = ['SWAP D', SWAP_R('D')]
i[0x33] = ['SWAP E', SWAP_R('E')]
i[0x34] = ['SWAP H', SWAP_R('H')]
i[0x35] = ['SWAP L', SWAP_R('L')]
i[0x36] = ['SWAP (HL)', SWAP_$HL()]

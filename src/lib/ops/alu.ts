import {
  ICpu,
  SimpleRegisters,
  CommandDefinition,
  TickableEmulator
} from '../types'

export const i: CommandDefinition[] = []

/**
 * 1. ADD A,n
 * Add n to A.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL),#
 *
 * Flags affected:
 * Z - Set if result is zero
 * N - Reset
 * H - Set if carry from bit 3
 * C - Set if carry from bit 7
 */
function add(cpu: ICpu, value: number) {
  const valueA = cpu.get('A')
  const result = valueA + value
  cpu.clearFlags()
  if (result & 0x100) {
    cpu.setFlag('C')
  }
  if ((valueA ^ value ^ result) & 0x10) {
    cpu.setFlag('H')
  }
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  }

  cpu.set('A', result)
}

export function ADD_A_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const valueR = e.cpu.get(R)
    add(e.cpu, valueR)
  }
}

function ADD_A_$HL() {
  return (e: TickableEmulator) => {
    const value = e.readByteTick(e.cpu.get('HL'))
    add(e.cpu, value)
  }
}

function ADD_A_$N() {
  return (e: TickableEmulator) => {
    const value = e.readU8Tick()
    add(e.cpu, value)
  }
}

i[0x87] = ['ADD A,A', ADD_A_R('A')]
i[0x80] = ['ADD A,B', ADD_A_R('B')]
i[0x81] = ['ADD A,C', ADD_A_R('C')]
i[0x82] = ['ADD A,D', ADD_A_R('D')]
i[0x83] = ['ADD A,E', ADD_A_R('E')]
i[0x84] = ['ADD A,H', ADD_A_R('H')]
i[0x85] = ['ADD A,L', ADD_A_R('L')]
i[0x86] = ['ADD A,(HL)', ADD_A_$HL()]
i[0xc6] = ['ADD A,u8', ADD_A_$N()]

/**
 * 2. ADC A,n
 *
 * Description:
 * Add n + Carry flag to A.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL),#
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Set if carry from bit 3.
 * C - Set if carry from bit 7.
 */
export function adc(cpu: ICpu, value: number) {
  const result = cpu.getFlag('C') + value + cpu.get('A')

  cpu.clearFlags()
  if (result & 0x100) {
    cpu.setFlag('C')
  }
  const op = value ^ cpu.get('A') ^ result
  if (op & 0x10) {
    cpu.setFlag('H')
  }
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  }

  cpu.set('A', result)
}

function ADC_A_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    adc(e.cpu, e.cpu.get(R))
  }
}

i[0x8f] = ['ADC A,A', ADC_A_R('A')]
i[0x88] = ['ADC A,B', ADC_A_R('B')]
i[0x89] = ['ADC A,C', ADC_A_R('C')]
i[0x8a] = ['ADC A,D', ADC_A_R('D')]
i[0x8b] = ['ADC A,E', ADC_A_R('E')]
i[0x8c] = ['ADC A,H', ADC_A_R('H')]
i[0x8d] = ['ADC A,L', ADC_A_R('L')]
i[0x8e] = [
  'ADC A,(HL)',
  (e: TickableEmulator) => {
    adc(e.cpu, e.readByteTick(e.cpu.get('HL')))
  }
]
i[0xce] = [
  'ADC A,u8',
  (e: TickableEmulator) => {
    const byte = e.readU8Tick()
    adc(e.cpu, byte)
  }
]

/**
 * 3. SUB n
 * Subtract n from A.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL),#
 *
 * Flags affected:
 * Z - Set if result is zero
 * N - Set
 * H - Set if no borrow from bit 4
 * C - Set if no borrow
 */
function sub(cpu: ICpu, value: number) {
  const valueA = cpu.get('A')
  const result = valueA - value
  cpu.setFlag('N')
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  } else {
    cpu.clearFlag('Z')
  }
  if (result & 0x100) {
    cpu.setFlag('C')
  } else {
    cpu.clearFlag('C')
  }
  if (((valueA ^ value ^ result) & 0x10) !== 0) {
    cpu.setFlag('H')
  } else {
    cpu.clearFlag('H')
  }
  return result
}

export function SUB_A_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const result = sub(e.cpu, e.cpu.get(R))
    e.cpu.set('A', result)
  }
}

export function SUB_$HL() {
  return (e: TickableEmulator) => {
    const value = e.readByteTick(e.cpu.get('HL'))
    const result = sub(e.cpu, value)
    e.cpu.set('A', result)
  }
}

export function SUB_N() {
  return (e: TickableEmulator) => {
    const value = e.readU8Tick()
    const result = sub(e.cpu, value)
    e.cpu.set('A', result)
  }
}

i[0x97] = ['SUB A,A', SUB_A_R('A')]
i[0x90] = ['SUB A,B', SUB_A_R('B')]
i[0x91] = ['SUB A,C', SUB_A_R('C')]
i[0x92] = ['SUB A,D', SUB_A_R('D')]
i[0x93] = ['SUB A,E', SUB_A_R('E')]
i[0x94] = ['SUB A,H', SUB_A_R('H')]
i[0x95] = ['SUB A,L', SUB_A_R('L')]
i[0x96] = ['SUB A,(HL)', SUB_$HL()]
i[0xd6] = ['SUB A,u8', SUB_N()]

/**
 * 4. SBC A,n
 *
 * Subtract n + Carry flag from A
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL),#
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Set.
 * H - Set if no borrow from bit 4.
 * C - Set if no borrow.
 */
function sbc(cpu: ICpu, value: number) {
  const valueA = cpu.get('A')
  const result = valueA - value - cpu.getFlag('C')
  cpu.setFlag('N')
  if ((result & 0xff) === 0) {
    // overflow muze byt take zero!
    cpu.setFlag('Z')
  } else {
    cpu.clearFlag('Z')
  }
  if (result & 0x100) {
    cpu.setFlag('C')
  } else {
    cpu.clearFlag('C')
  }
  if ((valueA ^ value ^ result) & 0x10) {
    cpu.setFlag('H')
  } else {
    cpu.clearFlag('H')
  }
  return result
}

export function SBC_A_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const result = sbc(e.cpu, e.cpu.get(R))
    e.cpu.set('A', result)
  }
}

export function SBC_$HL() {
  return (e: TickableEmulator) => {
    const value = e.readByteTick(e.cpu.get('HL'))
    const result = sbc(e.cpu, value)
    e.cpu.set('A', result)
  }
}

export function SBC_N() {
  return (e: TickableEmulator) => {
    const value = e.readU8Tick()
    const result = sbc(e.cpu, value)
    e.cpu.set('A', result)
  }
}

i[0x9f] = ['SBC A,A', SBC_A_R('A')]
i[0x98] = ['SBC A,B', SBC_A_R('B')]
i[0x99] = ['SBC A,C', SBC_A_R('C')]
i[0x9a] = ['SBC A,D', SBC_A_R('D')]
i[0x9b] = ['SBC A,E', SBC_A_R('E')]
i[0x9c] = ['SBC A,H', SBC_A_R('H')]
i[0x9d] = ['SBC A,L', SBC_A_R('L')]
i[0x9e] = ['SBC A,(HL)', SBC_$HL()]
i[0xde] = ['SBC A,u8', SBC_N()]

/**
 * 5. AND n
 *
 * Logically AND n with A, result in A.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL),#
 *
 * Flags affected:
 * Z - Set if result is zero.
 * N - Reset.
 * H - Set.
 * C - Reset.
 */
function and(cpu: ICpu, value: number) {
  const result = cpu.get('A') & value
  cpu.set('A', result).clearFlag('N').setFlag('H').clearFlag('C')
  if (result === 0) {
    cpu.setFlag('Z')
  } else {
    cpu.clearFlag('Z')
  }
}

function AND_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    and(e.cpu, e.cpu.get(R))
  }
}

export function AND_$HL() {
  return (e: TickableEmulator) => {
    const value = e.readByteTick(e.cpu.get('HL'))
    and(e.cpu, value)
  }
}

export function AND_N() {
  return (e: TickableEmulator) => {
    const value = e.readU8Tick()
    and(e.cpu, value)
  }
}
i[0xa7] = ['AND A,A', AND_R('A')]
i[0xa0] = ['AND A,B', AND_R('B')]
i[0xa1] = ['AND A,C', AND_R('C')]
i[0xa2] = ['AND A,D', AND_R('D')]
i[0xa3] = ['AND A,E', AND_R('E')]
i[0xa4] = ['AND A,H', AND_R('H')]
i[0xa5] = ['AND A,L', AND_R('L')]
i[0xa6] = ['AND A,(HL)', AND_$HL()]
i[0xe6] = ['AND A,u8', AND_N()]

/**
 * 6. OR n
 * Logical OR n with register A, result in A.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL),#
 *
 * Flags affected:
 * Z - Set if result is zero
 * N - Reset
 * H - Reset
 * C - Reset
 */
function or(cpu: ICpu, value: number) {
  const valueA = cpu.get('A')
  const result = valueA | value
  cpu.clearFlags()
  if (result === 0) {
    cpu.setFlag('Z')
  }
  cpu.set('A', result)
}

export function OR_A_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const value = e.cpu.get(R)
    or(e.cpu, value)
  }
}

function OR_A_$HL() {
  return (e: TickableEmulator) => {
    const value = e.readByteTick(e.cpu.get('HL'))
    or(e.cpu, value)
  }
}

function OR_A_$N() {
  return (e: TickableEmulator) => {
    const value = e.readU8Tick()
    or(e.cpu, value)
  }
}

i[0xb7] = ['OR A,A', OR_A_R('A')]
i[0xb0] = ['OR A,B', OR_A_R('B')]
i[0xb1] = ['OR A,C', OR_A_R('C')]
i[0xb2] = ['OR A,D', OR_A_R('D')]
i[0xb3] = ['OR A,E', OR_A_R('E')]
i[0xb4] = ['OR A,H', OR_A_R('H')]
i[0xb5] = ['OR A,L', OR_A_R('L')]
i[0xb6] = ['OR A,(HL)', OR_A_$HL()]
i[0xf6] = ['OR A,u8', OR_A_$N()]

/**
 * 7. XOR n
 * Logical exclusive OR n with register A, result in A.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL),#
 *
 * Flags affected:
 * Z - Set if result is zero
 * N - Reset
 * H - Reset
 * C - Reset
 */
function xor(cpu: ICpu, value: number) {
  const result = cpu.get('A') ^ value
  cpu.set('A', result)
  cpu.clearFlags()
  if (result === 0) {
    cpu.setFlag('Z')
  }
}

export function XOR_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const value = e.cpu.get(R)
    xor(e.cpu, value)
  }
}

function XOR_A_$HL() {
  return (e: TickableEmulator) => {
    const value = e.readByteTick(e.cpu.get('HL'))
    xor(e.cpu, value)
  }
}

function XOR_A_$N() {
  return (e: TickableEmulator) => {
    const value = e.readU8Tick()
    xor(e.cpu, value)
  }
}

i[0xaf] = ['XOR A,A', XOR_R('A')]
i[0xa8] = ['XOR A,B', XOR_R('B')]
i[0xa9] = ['XOR A,C', XOR_R('C')]
i[0xaa] = ['XOR A,D', XOR_R('D')]
i[0xab] = ['XOR A,E', XOR_R('E')]
i[0xac] = ['XOR A,H', XOR_R('H')]
i[0xad] = ['XOR A,L', XOR_R('L')]
i[0xae] = ['XOR A,(HL)', XOR_A_$HL()]
i[0xee] = ['XOR A,u8', XOR_A_$N()]

/**
 * 8. CP n
 * Compare A with n. This is basically an A - n
 * subtraction instruction but the results are thrown
 * away.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL),#
 *
 * Flags affected:
 * Z - Set if result is zero. (Set if A = n.)
 * N - Set.
 * H - Set if no borrow from bit 4.
 * C - Set for no borrow. (Set if A < n.)
 */
function cp(cpu: ICpu, value: number) {
  sub(cpu, value)
}

export function CP_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const value = e.cpu.get(R)
    cp(e.cpu, value)
  }
}

function CP_A_$HL() {
  return (e: TickableEmulator) => {
    const value = e.readByteTick(e.cpu.get('HL'))
    cp(e.cpu, value)
  }
}

function CP_$N() {
  return (e: TickableEmulator) => {
    const value = e.readU8Tick()
    cp(e.cpu, value)
  }
}

i[0xbf] = ['CP A,A', CP_R('A')]
i[0xb8] = ['CP A,B', CP_R('B')]
i[0xb9] = ['CP A,C', CP_R('C')]
i[0xba] = ['CP A,D', CP_R('D')]
i[0xbb] = ['CP A,E', CP_R('E')]
i[0xbc] = ['CP A,H', CP_R('H')]
i[0xbd] = ['CP A,L', CP_R('L')]
i[0xbe] = ['CP A,(HL)', CP_A_$HL()]
i[0xfe] = ['CP A,u8', CP_$N()]

/**
 * 9. INC n
 * Increment register n.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if result is zero
 * N - Reset
 * H - Set if carry from bit 3
 * C - Not affected
 */
function inc(cpu: ICpu, value: number) {
  const result = value + 1
  cpu.clearFlag('N')
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  } else {
    cpu.clearFlag('Z')
  }

  // HALFCARRY
  if ((value & 0xf) === 0xf) {
    cpu.setFlag('H')
  } else {
    cpu.clearFlag('H')
  }
  return result
}

export function INC_$HL() {
  return (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const newValue = inc(e.cpu, e.readByteTick(address))
    e.writeByteTick(address, newValue)
  }
}

export function INC_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const newValue = inc(e.cpu, e.cpu.get(R))
    e.cpu.set(R, newValue)
  }
}

i[0x3c] = ['INC A', INC_R('A')]
i[0x04] = ['INC B', INC_R('B')]
i[0x0c] = ['INC C', INC_R('C')]
i[0x14] = ['INC D', INC_R('D')]
i[0x1c] = ['INC E', INC_R('E')]
i[0x24] = ['INC H', INC_R('H')]
i[0x2c] = ['INC L', INC_R('L')]
i[0x34] = ['INC (HL)', INC_$HL()]

/**
 * 10. DEC n
 * Decrement register n.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(HL)
 *
 * Flags affected:
 * Z - Set if result is zero
 * N - Set
 * H - Set if no borrow from bit 4.
 * C - Not affected
 */
function dec(cpu: ICpu, value: number) {
  const result = value - 1
  cpu.setFlag('N')
  if ((result & 0xff) === 0) {
    cpu.setFlag('Z')
  } else {
    cpu.clearFlag('Z')
  }

  // HALFCARRY
  if ((value & 0xf) === 0) {
    cpu.setFlag('H')
  } else {
    cpu.clearFlag('H')
  }
  return result
}

function DEC_R(R: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const newValue = dec(e.cpu, e.cpu.get(R))
    e.cpu.set(R, newValue)
  }
}

export function DEC_$HL() {
  return (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const newValue = dec(e.cpu, e.readByteTick(address))
    e.writeByteTick(address, newValue)
  }
}

i[0x3d] = ['DEC A', DEC_R('A')]
i[0x05] = ['DEC B', DEC_R('B')]
i[0x0d] = ['DEC C', DEC_R('C')]
i[0x15] = ['DEC D', DEC_R('D')]
i[0x1d] = ['DEC E', DEC_R('E')]
i[0x25] = ['DEC H', DEC_R('H')]
i[0x2d] = ['DEC L', DEC_R('L')]
i[0x35] = ['DEC (HL)', DEC_$HL()]

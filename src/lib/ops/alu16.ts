import { DualRegisters, CommandDefinition, TickableEmulator } from '../types'
import { getSignedByte } from '../binaryUtils'

export const i: CommandDefinition[] = []

/**
 * 1. ADD HL,n
 *
 * Add n to HL.
 *
 * n = BC,DE,HL,SP
 *
 * Flags affected:
 * Z - Not affected.
 * N - Reset.
 * H - Set if carry from bit 11.
 * C - Set if carry from bit 15.
 */
function ADD_HL_RR(RR: DualRegisters) {
  return (e: TickableEmulator) => {
    const n = e.cpu.get(RR)
    const result = e.cpu.get('HL') + n
    e.cpu.clearFlag('N')
    if (result & 0x10000) {
      e.cpu.setFlag('C')
    } else {
      e.cpu.clearFlag('C')
    }
    const sanizited = result & 0xffff
    if (((e.cpu.get('HL') ^ n ^ result) & 0x1000) !== 0) {
      e.cpu.setFlag('H')
    } else {
      e.cpu.clearFlag('H')
    }
    e.cpu.set('HL', sanizited)
    e.tick() // internal
  }
}
i[0x09] = ['ADD HL,BC', ADD_HL_RR('BC')]
i[0x19] = ['ADD HL,DE', ADD_HL_RR('DE')]
i[0x29] = ['ADD HL,HL', ADD_HL_RR('HL')]
i[0x39] = ['ADD HL,SP', ADD_HL_RR('SP')]

/**
 * 2. ADD SP,n
 *
 * Add n to Stack Pointer (SP).
 *
 * Use with:
 * n = one byte signed immediate value (#).
 *
 * Flags affected:
 * Z - Reset.
 * N - Reset.
 * H - Set or reset according to operation.
 * C - Set or reset according to operation.
 */
i[0xe8] = [
  'ADD SP,i8',
  (e: TickableEmulator) => {
    const n = getSignedByte(e.readU8Tick())
    const result = e.cpu.getSP() + n

    const op = e.cpu.getSP() ^ n ^ result

    e.cpu.clearFlags()
    if (op & 0x10) {
      e.cpu.setFlag('H')
    }

    if (op & 0x100) {
      e.cpu.setFlag('C')
    }

    e.cpu.setSP(result)
    e.tick() // internal
    e.tick() // internal
  }
]

/**
 * 3. INC nn
 *
 * Increment register nn.
 *
 * Use with:
 * nn = BC,DE,HL,SP
 *
 * Flags affected:
 * None.
 */
export function INC_RR(RR: DualRegisters) {
  return (e: TickableEmulator) => {
    const previousValue = e.cpu.get(RR)
    const newValue = (previousValue + 1) & 0xffff
    e.cpu.set(RR, newValue)
    e.tick() // internal
  }
}

i[0x03] = ['INC BC', INC_RR('BC')]
i[0x13] = ['INC DE', INC_RR('DE')]
i[0x23] = ['INC HL', INC_RR('HL')]
i[0x33] = ['INC SP', INC_RR('SP')]

/**
 * 4. DEC nn
 *
 * Decrement register nn.
 *
 * Use with:
 * nn = BC,DE,HL,SP
 *
 * Flags affected:
 * None.
 */
export function DEC_RR(RR: DualRegisters) {
  return (e: TickableEmulator) => {
    const previousValue = e.cpu.get(RR)
    const newValue = (previousValue - 1) & 0xffff
    e.cpu.set(RR, newValue)
    e.tick() // internal
  }
}

i[0x0b] = ['DEC BC', DEC_RR('BC')]
i[0x1b] = ['DEC DE', DEC_RR('DE')]
i[0x2b] = ['DEC HL', DEC_RR('HL')]
i[0x3b] = ['DEC SP', DEC_RR('SP')]

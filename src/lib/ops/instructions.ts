import { i as aluInstructions } from './alu'
import { i as alu16Instructions } from './alu16'
import { i as jumpInstructions } from './jumps'
import { i as loadInstructions } from './loads'
import { i as callInstructions } from './calls'
import { i as restartInstructions } from './restarts'
import { i as returnInstructions } from './returns'
import { i as rotatesInstructions } from './rotates'
import { i as miscInstructions } from './misc'

export { i as cbInstructions } from './cb'

export const instructions = {
  ...callInstructions,
  ...jumpInstructions,
  ...aluInstructions,
  ...alu16Instructions,
  ...loadInstructions,
  ...miscInstructions,
  ...restartInstructions,
  ...returnInstructions,
  ...rotatesInstructions
}

import { CommandDefinition, TickableEmulator } from '../types'

export const i: CommandDefinition[] = []

/**
 * 2. DAA
 *
 * Decimal adjust register A.
 * This instruction adjusts register A so that the
 * correct representation of Binary Coded Decimal (BCD)
 * is obtained
 *
 * Flags affected:
 * Z - Set if register A is zero.
 * N - Not affected.
 * H - Reset.
 * C - Set or reset according to operation.
 */
i[0x27] = [
  'DAA',
  (e: TickableEmulator) => {
    let tmp = e.cpu.get('A')
    if (!e.cpu.getFlag('N')) {
      if (e.cpu.getFlag('H') || (tmp & 0x0f) > 9) {
        tmp += 0x06
      }
      if (e.cpu.getFlag('C') || tmp > 0x9f) {
        tmp += 0x60
      }
    } else {
      if (e.cpu.getFlag('H')) {
        tmp = (tmp - 6) & 0xff
      }
      if (e.cpu.getFlag('C')) {
        tmp -= 0x60
      }
    }

    e.cpu.clearFlag('H').clearFlag('Z')

    if ((tmp & 0x100) === 0x100) {
      e.cpu.setFlag('C')
    }
    if ((tmp & 0xff) === 0) {
      e.cpu.setFlag('Z')
    }

    e.cpu.set('A', tmp)
  }
]

/**
 * 3. CPL
 *
 * Complement A register. (Flip all bits.)
 *
 * Flags affected:
 * Z - Not affected.
 * N - Set.
 * H - Set.
 * C - Not affected.
 */
export function cpl(val: number) {
  return val ^ 0xff
}

i[0x2f] = [
  'CPL',
  (e: TickableEmulator) => {
    e.cpu
      .set('A', cpl(e.cpu.get('A')))
      .setFlag('N')
      .setFlag('H')
  }
]

/**
 * 4. CCF
 *
 * Complement carry flag.
 * If C flag is set, then reset it
 * If C flag is reset, then set it
 *
 * Flags affected:
 * Z - Not affected.
 * N - Reset.
 * H - Reset.
 * C - Complemented.
 */
i[0x3f] = [
  'CCF',
  (e: TickableEmulator) => {
    if (e.cpu.getFlag('C')) {
      e.cpu.clearFlag('C')
    } else {
      e.cpu.setFlag('C')
    }
    e.cpu.clearFlag('N').clearFlag('H')
  }
]

/**
 * 5. SCF
 *
 * Set carry flag.
 *
 * Flags affected:
 * Z - Not affected.
 * N - Reset.
 * H - Reset.
 * C - Set.
 */
i[0x37] = [
  'SCF',
  (e: TickableEmulator) => {
    e.cpu.clearFlag('N').clearFlag('H').setFlag('C')
  }
]

/**
 * 6. NOP
 *
 * No operation.
 */
i[0x00] = ['NOP', () => null]

/**
 * 7. HALT
 *
 * Power down CPU until an interrupt occurs. Use this
 * when ever possible to reduce energy consumption.
 */
i[0x76] = [
  'HALT',
  (e: TickableEmulator) => {
    // e.tick()

    e.cpu.halted = true
    if (e.interruptController.hasAvailableInterrupts()) {
      if (e.cpu.ime) {
        e.cpu.halted = false
        e.cpu.incrementPC(-1)
      } else {
        e.cpu.halted = false
        e.cpu.haltBug = true
      }
    }
    e.cpu.justHalted = true

    // if (e.cpu.ime || !e.interruptController.hasAvailableInterrupts()) {
    //   e.cpu.halted = true
    // } else {
    //   e.cpu.haltBug = true
    // }
    // e.
  }
]

/**
 * 8. STOP
 *
 * Description:
 * Halt CPU & LCD display until button pressed
 */
i[0x10] = [
  'STOP',
  (e: TickableEmulator) => {
    e.cpu.incrementPC()
  }
]

/**
 * 9. DI
 * This instruction disables interrupts but not
 * immediately. Interrupts are disabled after
 * instruction after DI is executed.
 *
 * Flags affected:
 * None.
 */
i[0xf3] = [
  'DI',
  (e: TickableEmulator) => {
    e.cpu.ime = false
  }
]

/**
 * 10. EI
 *
 * Enable interrupts. This intruction enables interrupts
 * but not immediately. Interrupts are enabled after
 * instruction after EI is executed.
 *
 * Flags affected:
 * None.
 */
i[0xfb] = [
  'EI',
  (e: TickableEmulator) => {
    // interrupts are enabled after one cycle
    e.cpu.imeRequested = true
  }
]

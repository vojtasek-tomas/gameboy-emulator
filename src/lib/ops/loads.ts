import {
  DualRegisters,
  RegisterNames,
  CommandDefinition,
  SimpleRegisters,
  TickableEmulator
} from '../types'
import { getSignedByte } from '../binaryUtils'

export const i: CommandDefinition[] = []

/**
 * 1. LD nn,n
 *
 * Put value nn into n.
 *
 * Use with:
 * nn = B,C,D,E,H,L,BC,DE,HL,SP
 * n = 8 bit immediate value
 */
export function LD_R_u8(R: RegisterNames) {
  return (e: TickableEmulator) => {
    const byte = e.readU8Tick()
    e.cpu.set(R, byte)
  }
}

i[0x06] = ['LD B,u8', LD_R_u8('B')]
i[0x0e] = ['LD C,u8', LD_R_u8('C')]
i[0x16] = ['LD D,u8', LD_R_u8('D')]
i[0x1e] = ['LD E,u8', LD_R_u8('E')]
i[0x26] = ['LD H,u8', LD_R_u8('H')]
i[0x2e] = ['LD L,u8', LD_R_u8('L')]
i[0x3e] = ['LD A,u8', LD_R_u8('A')]

/**
 * 2. LD r1,r2
 *
 * Put value r2 into r1.
 *
 * Use with:
 * r1,r2 = A,B,C,D,E,H,L,(HL)
 */
i[0x36] = [
  'LD (HL),u8',
  (e: TickableEmulator) => {
    const byte = e.readU8Tick()
    e.writeByteTick(e.cpu.get('HL'), byte)
  }
]

/**
 * 4. LD n,A
 *
 * Put value A into n.
 *
 * Use with:
 * n = A,B,C,D,E,H,L,(BC),(DE),(HL),(nn)
 * nn = two byte immediate value. (LS byte first.)
 */
function LD_$RR_A(RR: DualRegisters) {
  return (e: TickableEmulator) => {
    const value = e.cpu.get('A')
    const address = e.cpu.get(RR)
    e.writeByteTick(address, value)
  }
}

// LD R1,R2
function LD_R1_R2(R1: SimpleRegisters, R2: SimpleRegisters) {
  return (e: TickableEmulator) => {
    const value = e.cpu.get(R2)
    e.cpu.set(R1, value)
  }
}

function LD_R_A(R: SimpleRegisters) {
  return LD_R1_R2(R, 'A')
}

// LD R1,(R2)
function LD_R1_$R2(R1: SimpleRegisters, R2: DualRegisters) {
  return (e: TickableEmulator) => {
    const value = e.readByteTick(e.cpu.get(R2))
    e.cpu.set(R1, value)
  }
}

// LD (R1),R2
function LD_$R1_R2(R1: DualRegisters, R2: SimpleRegisters) {
  return (e: TickableEmulator) => {
    e.writeByteTick(e.cpu.get(R1), e.cpu.get(R2))
  }
}

i[0x78] = ['LD A,B', LD_R1_R2('A', 'B')]
i[0x79] = ['LD A,C', LD_R1_R2('A', 'C')]
i[0x7a] = ['LD A,D', LD_R1_R2('A', 'D')]
i[0x7b] = ['LD A,E', LD_R1_R2('A', 'E')]
i[0x7c] = ['LD A,H', LD_R1_R2('A', 'H')]
i[0x7d] = ['LD A,L', LD_R1_R2('A', 'L')]
i[0x0a] = ['LD A,(BC)', LD_R1_$R2('A', 'BC')]
i[0x1a] = ['LD A,(DE)', LD_R1_$R2('A', 'DE')]
i[0x7e] = ['LD A,(HL)', LD_R1_$R2('A', 'HL')]

i[0x40] = ['LD B,B', LD_R1_R2('B', 'B')]
i[0x41] = ['LD B,C', LD_R1_R2('B', 'C')]
i[0x42] = ['LD B,D', LD_R1_R2('B', 'D')]
i[0x43] = ['LD B,E', LD_R1_R2('B', 'E')]
i[0x44] = ['LD B,H', LD_R1_R2('B', 'H')]
i[0x45] = ['LD B,L', LD_R1_R2('B', 'L')]
i[0x46] = ['LD B,(HL)', LD_R1_$R2('B', 'HL')]

i[0x48] = ['LD C,B', LD_R1_R2('C', 'B')]
i[0x49] = ['LD C,C', LD_R1_R2('C', 'C')]
i[0x4a] = ['LD C,D', LD_R1_R2('C', 'D')]
i[0x4b] = ['LD C,E', LD_R1_R2('C', 'E')]
i[0x4c] = ['LD C,H', LD_R1_R2('C', 'H')]
i[0x4d] = ['LD C,L', LD_R1_R2('C', 'L')]
i[0x4e] = ['LD C,(HL)', LD_R1_$R2('C', 'HL')]

i[0x50] = ['LD D,B', LD_R1_R2('D', 'B')]
i[0x51] = ['LD D,C', LD_R1_R2('D', 'C')]
i[0x52] = ['LD D,D', LD_R1_R2('D', 'D')]
i[0x53] = ['LD D,E', LD_R1_R2('D', 'E')]
i[0x54] = ['LD D,H', LD_R1_R2('D', 'H')]
i[0x55] = ['LD D,L', LD_R1_R2('D', 'L')]
i[0x56] = ['LD D,(HL)', LD_R1_$R2('D', 'HL')]

i[0x58] = ['LD E,B', LD_R1_R2('E', 'B')]
i[0x59] = ['LD E,C', LD_R1_R2('E', 'C')]
i[0x5a] = ['LD E,D', LD_R1_R2('E', 'D')]
i[0x5b] = ['LD E,E', LD_R1_R2('E', 'E')]
i[0x5c] = ['LD E,H', LD_R1_R2('E', 'H')]
i[0x5d] = ['LD E,L', LD_R1_R2('E', 'L')]
i[0x5e] = ['LD E,(HL)', LD_R1_$R2('E', 'HL')]

i[0x60] = ['LD H,B', LD_R1_R2('H', 'B')]
i[0x61] = ['LD H,C', LD_R1_R2('H', 'C')]
i[0x62] = ['LD H,D', LD_R1_R2('H', 'D')]
i[0x63] = ['LD H,E', LD_R1_R2('H', 'E')]
i[0x64] = ['LD H,H', LD_R1_R2('H', 'H')]
i[0x65] = ['LD H,L', LD_R1_R2('H', 'L')]
i[0x66] = ['LD H,(HL)', LD_R1_$R2('H', 'HL')]

i[0x68] = ['LD L,B', LD_R1_R2('L', 'B')]
i[0x69] = ['LD L,C', LD_R1_R2('L', 'C')]
i[0x6a] = ['LD L,D', LD_R1_R2('L', 'D')]
i[0x6b] = ['LD L,E', LD_R1_R2('L', 'E')]
i[0x6c] = ['LD L,H', LD_R1_R2('L', 'H')]
i[0x6d] = ['LD L,L', LD_R1_R2('L', 'L')]
i[0x6e] = ['LD L,(HL)', LD_R1_$R2('L', 'HL')]

i[0x70] = ['LD (HL),B', LD_$R1_R2('HL', 'B')]
i[0x71] = ['LD (HL),C', LD_$R1_R2('HL', 'C')]
i[0x72] = ['LD (HL),D', LD_$R1_R2('HL', 'D')]
i[0x73] = ['LD (HL),E', LD_$R1_R2('HL', 'E')]
i[0x74] = ['LD (HL),H', LD_$R1_R2('HL', 'H')]
i[0x75] = ['LD (HL),L', LD_$R1_R2('HL', 'L')]

i[0x7f] = ['LD A,A', LD_R_A('A')]
i[0x47] = ['LD B,A', LD_R_A('B')]
i[0x4f] = ['LD C,A', LD_R_A('C')]
i[0x57] = ['LD D,A', LD_R_A('D')]
i[0x5f] = ['LD E,A', LD_R_A('E')]
i[0x67] = ['LD H,A', LD_R_A('H')]
i[0x6f] = ['LD L,A', LD_R_A('L')]
i[0x02] = ['LD (BC),A', LD_$RR_A('BC')]
i[0x12] = ['LD (DE),A', LD_$RR_A('DE')]
i[0x77] = ['LD (HL),A', LD_$RR_A('HL')]

i[0xfa] = [
  'LD A,(u16)',
  (e: TickableEmulator) => {
    const address = e.readU16Tick()
    e.cpu.set('A', e.readByteTick(address))
  }
]

// 16 bit

/**
 * 1. LD n,u16
 *
 * Put value nn into n
 *
 * Use with:
 * n = BC,DE,HL,SP
 * nn = 16 bit immediate value
 */
export function LD_RR_u16(RR: DualRegisters) {
  return (e: TickableEmulator) => {
    const word = e.readU16Tick()
    e.cpu.set(RR, word)
  }
}

i[0x01] = ['LD BC,u16', LD_RR_u16('BC')]
i[0x11] = ['LD DE,u16', LD_RR_u16('DE')]
i[0x21] = ['LD HL,u16', LD_RR_u16('HL')]
i[0x31] = ['LD SP,u16', LD_RR_u16('SP')]

/**
 * 2. LD SP,HL
 *
 * Put HL into Stack Pointer (SP)
 */
i[0xf9] = [
  'LD SP,HL',
  (e: TickableEmulator) => {
    e.cpu.set('SP', e.cpu.get('HL'))
    e.tick() // internal
  }
]

/**
 * 4. LDHL SP,n
 *
 * Put SP + n effective address into HL.
 *
 * Use with:
 * n = one byte signed immediate value.
 *
 * Flags affected:
 * Z - Reset.
 * N - Reset.
 * H - Set or reset according to operation.
 * C - Set or reset according to operation.
 */
i[0xf8] = [
  'LD HL,SP+i8',
  (e: TickableEmulator) => {
    const n = getSignedByte(e.readU8Tick())

    const sp = e.cpu.getSP()
    const result = n + sp
    e.cpu.clearFlags()
    const op = sp ^ n ^ result
    if ((op & 0x10) !== 0) {
      e.cpu.setFlag('H')
    }
    if ((op & 0x100) !== 0) {
      e.cpu.setFlag('C')
    }

    e.cpu.set('HL', result)
    e.tick() // internal
  }
]

/**
 * 5. LD (nn),SP
 *
 * Description:
 * Put Stack Pointer (SP) at address n.
 *
 * Use with:
 * nn = two byte immediate address.
 */
i[0x08] = [
  'LD (u16),SP',
  (e: TickableEmulator) => {
    const value = e.cpu.getSP()
    const address = e.readU16Tick()
    e.writeWordTick(address, value)
  }
]

/**
 * 6. PUSH nn
 *
 * Push register pair nn onto stack.
 *
 * Use with:
 * nn = AF,BC,DE,HL
 */
export function PUSH_RR(RR: DualRegisters) {
  return (e: TickableEmulator) => {
    e.tick() // internal

    const value = e.cpu.get(RR)
    e.cpu.incrementSP(-2)
    e.writeWordTick(e.cpu.getSP(), value)
  }
}

i[0xf5] = ['PUSH AF', PUSH_RR('AF')]
i[0xc5] = ['PUSH BC', PUSH_RR('BC')]
i[0xd5] = ['PUSH DE', PUSH_RR('DE')]
i[0xe5] = ['PUSH HL', PUSH_RR('HL')]

/**
 * 7. POP nn
 *
 * Pop two bytes off stack into register pair nn.
 *
 * Use with:
 * nn = AF,BC,DE,HL
 */
export function POP_RR(RR: DualRegisters) {
  return (e: TickableEmulator) => {
    const value = e.readWordTick(e.cpu.getSP())
    e.cpu.incrementSP(2)
    e.cpu.set(RR, value)
  }
}

i[0xf1] = ['POP AF', POP_RR('AF')]
i[0xc1] = ['POP BC', POP_RR('BC')]
i[0xd1] = ['POP DE', POP_RR('DE')]
i[0xe1] = ['POP HL', POP_RR('HL')]

i[0x22] = [
  'LD (HL+),A',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const value = e.cpu.get('A')
    e.writeByteTick(address, value)
    e.cpu.set('HL', address + 1)
  }
]
i[0x2a] = [
  'LD A,(HL+)',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const val = e.readByteTick(address)
    e.cpu.set('A', val)
    e.cpu.set('HL', address + 1)
  }
]
i[0x3a] = [
  'LD A,(HL-)',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    const val = e.readByteTick(address)
    e.cpu.set('A', val)
    e.cpu.set('HL', address - 1)
  }
]
i[0xea] = [
  'LD (u16),A',
  (e: TickableEmulator) => {
    const address = e.readU16Tick()
    e.writeByteTick(address, e.cpu.get('A'))
  }
]
i[0xf2] = [
  'LD A,(FF00+C)',
  (e: TickableEmulator) => {
    const value = e.readByteTick(0xff00 + e.cpu.get('C'))
    e.cpu.set('A', value)
  }
]

i[0xe0] = [
  'LD (FF00+u8),A',
  (e: TickableEmulator) => {
    const address = 0xff00 + e.readU8Tick()
    e.writeByteTick(address, e.cpu.get('A'))
  }
]
i[0xf0] = [
  'LD A,(FF00+u8)',
  (e: TickableEmulator) => {
    const address = 0xff00 + e.readU8Tick()
    const value = e.readByteTick(address)
    e.cpu.set('A', value)
  }
]

i[0x32] = [
  'LD (HL-),A',
  (e: TickableEmulator) => {
    const address = e.cpu.get('HL')
    e.writeByteTick(address, e.cpu.get('A'))
    e.cpu.set('HL', e.cpu.get('HL') - 1)
  }
]
i[0xe2] = [
  'LD (FF00+C),A',
  (e: TickableEmulator) => {
    e.writeByteTick(0xff00 + e.cpu.get('C'), e.cpu.get('A'))
  }
]

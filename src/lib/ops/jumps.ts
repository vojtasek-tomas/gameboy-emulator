import { ICpu, CommandDefinition, TickableEmulator } from '../types'
import { getSignedByte } from '../binaryUtils'

export const i: CommandDefinition[] = []

/**
 * 1. JP nn
 *
 * Jump to address nn.
 * Use with:
 * nn = two byte immediate value. (LS byte first.)
 */
i[0xc3] = [
  'JP u16',
  (e: TickableEmulator) => {
    const address = e.readU16Tick()
    e.cpu.setPC(address)
    e.tick() // internal
  }
]

/**
 * 2. JP cc,u16
 *
 * Description:
 * Jump to address n if following condition is true:
 * cc = NZ, Jump if Z flag is reset.
 * cc = Z, Jump if Z flag is set.
 * cc = NC, Jump if C flag is reset.
 * cc = C, Jump if C flag is set.
 *
 * Use with:
 * nn = two byte immediate value. (LS byte first.)
 */
export function JP_CC_nn(condition: (cpu: ICpu) => boolean) {
  return (e: TickableEmulator) => {
    const word = e.readU16Tick()
    if (condition(e.cpu)) {
      e.cpu.setPC(word)
      e.tick() // internal
    }
  }
}

i[0xc2] = ['JP NZ,u16', JP_CC_nn((cpu) => !cpu.getFlag('Z'))]
i[0xca] = ['JP Z,u16', JP_CC_nn((cpu) => !!cpu.getFlag('Z'))]
i[0xd2] = ['JP NC,u16', JP_CC_nn((cpu) => !cpu.getFlag('C'))]
i[0xda] = ['JP C,u16', JP_CC_nn((cpu) => !!cpu.getFlag('C'))]

/**
 * 3. JP (HL)
 *
 * Jump to address contained in HL.
 */
i[0xe9] = [
  'JP HL',
  (e: TickableEmulator) => {
    e.cpu.setPC(e.cpu.get('HL'))
  }
]

// If following condition is true then add n to current address and jump to it
export function JR_CC_N(condition: (cpu: ICpu) => boolean) {
  return (e: TickableEmulator) => {
    const byte = e.readU8Tick()

    if (condition(e.cpu)) {
      const signed = getSignedByte(byte)
      e.cpu.incrementPC(signed)
      e.tick() // internal
    }
  }
}

/**
 * 4. JR n
 *
 * Add n to current address and jump to it.
 *
 * Use with:
 * n = one byte signed immediate value
 */
i[0x18] = ['JR i8', JR_CC_N(() => true)]

/**
 * 5. JR cc,n
 *
 * If following condition is true then add n to current
 * address and jump to it:
 *
 * Use with:
 * n = one byte signed immediate value
 * cc = NZ, Jump if Z flag is reset.
 * cc = Z, Jump if Z flag is set.
 * cc = NC, Jump if C flag is reset.
 * cc = C, Jump if C flag is set.
 */
i[0x20] = ['JR NZ,i8', JR_CC_N((cpu) => !cpu.getFlag('Z'))]
i[0x28] = ['JR Z,i8', JR_CC_N((cpu) => !!cpu.getFlag('Z'))]
i[0x30] = ['JR NC,i8', JR_CC_N((cpu) => !cpu.getFlag('C'))]
i[0x38] = ['JR C,i8', JR_CC_N((cpu) => !!cpu.getFlag('C'))]

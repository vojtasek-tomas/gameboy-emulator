import { ICpu, CommandDefinition, TickableEmulator } from '../types'

export const i: CommandDefinition[] = []

export function CALL_NN_cc(condition: (cpu: ICpu) => boolean) {
  return (e: TickableEmulator) => {
    const value = e.cpu.getPC() + 2 // next instruction
    const word = e.readU16Tick()

    if (condition(e.cpu)) {
      e.tick() // internal

      e.cpu.incrementSP(-2)
      const address = e.cpu.getSP()

      e.writeWordTick(address, value)
      e.cpu.setPC(word)
    }
  }
}

/**
 * 1. CALL nn
 *
 * Push address of next instruction onto stack and then
 * jump to address nn.
 *
 * Use with:
 * nn = two byte immediate value. (LS byte first.)
 */
i[0xcd] = ['CALL u16', CALL_NN_cc(() => true)]

/**
 * 2. CALL cc,u16
 *
 * Call address n if following condition is true:
 * cc = NZ, Call if Z flag is reset.
 * cc = Z, Call if Z flag is set.
 * cc = NC, Call if C flag is reset.
 * cc = C, Call if C flag is set.
 *
 * Use with:
 * nn = two byte immediate value. (LS byte first.)
 */
i[0xc4] = ['CALL NZ,u16', CALL_NN_cc((cpu: ICpu) => !cpu.getFlag('Z'))]
i[0xcc] = ['CALL Z,u16', CALL_NN_cc((cpu: ICpu) => !!cpu.getFlag('Z'))]
i[0xd4] = ['CALL NC,u16', CALL_NN_cc((cpu: ICpu) => !cpu.getFlag('C'))]
i[0xdc] = ['CALL C,u16', CALL_NN_cc((cpu: ICpu) => !!cpu.getFlag('C'))]

import { Emulator } from './Emulator'
import { LogEntry } from '../components/types'
import { RegisterNames, Flags } from './types'

const registers: RegisterNames[] = ['A', 'B', 'C', 'D', 'E', 'H', 'L']
const flags: Flags[] = ['C', 'N', 'H', 'Z']

export function createLogEntry(
  emulator: Emulator,
  pc: number,
  instruction: string,
  opCode: number
): LogEntry {
  const isCb = opCode >= 0xcb00
  const cyclesCount = emulator.cyclesCount - (isCb ? 8 : 4)
  return {
    opCode,
    cyclesCount,
    instruction,
    pc,
    sp: emulator.cpu.getSP(),
    registers: registers.map((register) => [
      register,
      emulator.cpu.get(register)
    ]),
    flags: flags.map((flag) => [flag, emulator.cpu.getFlag(flag)]),
    LCDC: emulator.mmu.readByte(0xff40),
    STAT: emulator.mmu.readByte(0xff41),
    IF: emulator.mmu.readByte(0xff0f),
    IE: emulator.mmu.readByte(0xffff),
    LY: emulator.ppu.ly
  }
}

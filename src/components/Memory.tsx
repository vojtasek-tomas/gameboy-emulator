import React from 'react'
import { FixedSizeList, ListChildComponentProps } from 'react-window'
import styled from 'styled-components'
import { number2hex, range } from '../lib/utils'
import { IMMU } from '../lib/types'

type Props = {
  mmu: IMMU
  isRunning: boolean
}

const groupCount = 16 // 2 byte
const itemCount = (0xffff + 1) / groupCount

const Wrapper = styled.div`
  margin: 10px auto;
  width: 100%;
  max-width: 500px;
`

export class Memory extends React.Component<Props> {
  listRef = React.createRef<FixedSizeList>()

  renderRow = ({ index, style }: ListChildComponentProps) => {
    const { mmu } = this.props
    const startAddress = index * groupCount

    const data = range(startAddress, startAddress + groupCount).map((address) =>
      mmu.readByte(address)
    )
    const line = Array.from(data)
      .map((b) => number2hex(b))
      .join(' ')

    return (
      <div style={style}>
        <pre style={{ margin: 0 }}>
          {number2hex(index * 16, 4)}: {line}
        </pre>
      </div>
    )
  }

  goTo = (address: number) =>
    this.listRef.current?.scrollToItem(address / groupCount, 'center')

  render() {
    const { isRunning } = this.props
    return (
      <Wrapper>
        {!isRunning && (
          <FixedSizeList
            ref={this.listRef}
            height={150}
            itemCount={itemCount}
            itemSize={20}
            width={500}>
            {this.renderRow}
          </FixedSizeList>
        )}
        {isRunning && (
          <div
            style={{
              height: 150,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center'
            }}>
            Running
          </div>
        )}
        <button onClick={() => this.goTo(0x0000)}>Start</button>
        <button onClick={() => this.goTo(0x8000)}>VRAM (0x8000)</button>
        <button onClick={() => this.goTo(0x8800)}>VRAM (0x8800)</button>
        <button onClick={() => this.goTo(0xfe00)}>OAM (0xFE00)</button>
        <button onClick={() => this.goTo(0xffff)}>End</button>
      </Wrapper>
    )
  }
}

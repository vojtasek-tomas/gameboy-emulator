import React from 'react'
import styled from 'styled-components'

import { Center } from './utils'
import { OpenButton } from './OpenButton'
import { Chip } from './Chip'

type Props = {
  showDebugger: boolean
  toggleDebugger: (showDebugger: boolean) => void
  onOpen: (file?: File) => void
  onStart: () => void
  onStop: () => void
  onRestart: () => void
}

const Styled = {
  Wrapper: styled.div`
    margin: 10px 0;
  `,
  Button: styled.div`
    margin: 0 5px;
  `,
  Controls: styled.div`
    margin: 10px 0;
    display: flex;
    font-size: 12px;
  `,
  ControlGroup: styled.span`
    padding: 0 5px;
    display: flex;
    justify-content: center;
    align-items: center;

    > span {
      padding-left: 5px;
    }
  `
}

export function Controls(props: Props) {
  return (
    <Center>
      <Styled.Wrapper>
        <Center>
          <Styled.Button>
            <OpenButton onOpen={props.onOpen} />
          </Styled.Button>
          <Styled.Button>
            <button onClick={props.onStart}>Start</button>
          </Styled.Button>

          <Styled.Button>
            <button onClick={props.onRestart}>Restart</button>
          </Styled.Button>

          <Styled.Button>
            <button onClick={props.onStop}>Stop</button>
          </Styled.Button>
        </Center>

        <Styled.Controls>
          <Styled.ControlGroup>
            <Chip>← → ↑ ↓</Chip>
            <span>Arrows or WASD</span>
          </Styled.ControlGroup>
          <Styled.ControlGroup>
            <Chip>A</Chip>
            <span>X or J</span>
          </Styled.ControlGroup>
          <Styled.ControlGroup>
            <Chip>B</Chip>
            <span>C or K</span>
          </Styled.ControlGroup>
          <Styled.ControlGroup>
            <Chip>START</Chip>
            <span>H</span>
          </Styled.ControlGroup>
          <Styled.ControlGroup>
            <Chip>SELECT</Chip>
            <span>G</span>
          </Styled.ControlGroup>
        </Styled.Controls>

        <Center>
          <label>
            <input
              type="checkbox"
              checked={props.showDebugger}
              onChange={() => props.toggleDebugger(!props.showDebugger)}
              name="showDebugger"
            />
            Show debugger
          </label>
        </Center>
      </Styled.Wrapper>
    </Center>
  )
}

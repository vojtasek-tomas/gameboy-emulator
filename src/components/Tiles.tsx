import React from 'react'
import { PPU } from '../lib/PPU'

type Props = {
  ppu: PPU
}

const COLORS = [
  [255, 255, 255], // 0b00
  [192, 192, 192], // 0b01
  [96, 96, 96], // 0b10
  [0, 0, 0] // 0b11
]

const CANVAS_HEIGHT = 192
const CANVAS_WIDTH = 160
const TILES_PER_LINE = 16

export class Tiles extends React.Component<Props> {
  canvasRef = React.createRef<HTMLCanvasElement>()

  componentDidMount() {
    this.renderTiles()
  }

  componentDidUpdate() {
    this.renderTiles()
  }

  renderTiles() {
    const context = this.canvasRef.current?.getContext('2d')
    const imageData = context?.getImageData(0, 0, 160, 160)
    const { tiles } = this.props.ppu.vram

    if (imageData && context) {
      for (let y = 0; y < CANVAS_HEIGHT; y++) {
        for (let x = 0; x < CANVAS_WIDTH; x++) {
          const tileIndex =
            Math.floor(x / 8) + Math.floor(y / 8) * TILES_PER_LINE
          const tile = tiles[tileIndex]
          if (!tile) {
            continue
          }

          const pixel = tile[y & 7][x & 7]
          const offset = (x + y * CANVAS_WIDTH) * 4

          const colors = COLORS[pixel]
          imageData.data[offset] = colors[0]
          imageData.data[offset + 1] = colors[1]
          imageData.data[offset + 2] = colors[2]
          imageData.data[offset + 3] = 255 // alpha
        }
      }
      context.putImageData(imageData, 0, 0)
    }
  }

  render() {
    return (
      <>
        <canvas
          style={{ display: 'block', imageRendering: 'pixelated', zoom: 3 }}
          ref={this.canvasRef}
          height={CANVAS_HEIGHT}
          width={CANVAS_WIDTH}
        />
      </>
    )
  }
}

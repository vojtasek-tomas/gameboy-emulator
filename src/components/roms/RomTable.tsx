import React from 'react'
import Fuse from 'fuse.js'
import { useLocalStorage } from '@rehooks/local-storage'

import styled from 'styled-components'
import { roms } from '../../roms/roms'

type Props = {
  onOpen: (romName: string) => void
  onRun: (romName: string) => void
}

const KEY = 'rom_search'

const Table = styled.table`
  margin: 0 auto;
  font-size: 14px;
  max-width: 640px;
  width: 100%;
  border-collapse: collapse;

  thead {
    background: #eeeeee;
  }

  table,
  tbody,
  thead,
  td,
  th,
  td {
    text-align: left;
    padding: 0;
    border: none;
    border-spacing: 0;
  }

  td {
    padding: 1px 5px;
  }
  td:nth-child(2) {
    text-align: right;
  }
`

export function RomTable(props: Props) {
  const [search, setSearch] = useLocalStorage(KEY, '')

  const fuse = new Fuse(roms, {
    threshold: 0.5
  })

  const items = search ? fuse.search(search).map((result) => result.item) : roms
  return (
    <>
      <Table>
        <thead>
          <tr>
            <td>
              ROM name{' '}
              <input
                placeholder="Search"
                value={search}
                onChange={(e) => setSearch(e.currentTarget.value)}
              />
            </td>

            <td />
          </tr>
        </thead>
        <tbody>
          {items.map((romName) => (
            <tr key={romName}>
              <td>{romName}</td>
              <td align="right">
                <button onClick={() => props.onOpen(romName)}>OPEN</button>
                <button onClick={() => props.onRun(romName)}>RUN</button>
              </td>
            </tr>
          ))}
          {items.length === 0 && (
            <tr>
              <td colSpan={2}>empty</td>
            </tr>
          )}
        </tbody>
      </Table>
    </>
  )
}

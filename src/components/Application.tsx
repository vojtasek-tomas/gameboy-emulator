import React from 'react'
import { ThemeProvider } from 'styled-components'
import { useLocalStorage } from '@rehooks/local-storage'

import { Theme } from './types'

import { PageLayout } from './Application.styled'
import { Emulator } from './Emulator'

import { Zoom } from './Zoom'

const DEFAULT_ZOOM = 1.5

export function Application() {
  const [zoom, setZoom] = useLocalStorage('zoom', DEFAULT_ZOOM)

  const theme: Theme = { zoom }
  return (
    <ThemeProvider theme={theme}>
      <PageLayout>
        <Zoom zoom={zoom} onChange={setZoom} />
        <Emulator zoom={zoom} />
      </PageLayout>
    </ThemeProvider>
  )
}

export default Application

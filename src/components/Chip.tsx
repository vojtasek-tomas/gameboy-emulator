import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  padding: 2px 8px;
  display: inline-flex;
  align-items: center;
  border-radius: 16px;
  justify-content: center;
  background-color: #e0e0e0;
`

export function Chip({ children }: { children: React.ReactNode }) {
  return <Wrapper>{children}</Wrapper>
}

import styled from 'styled-components'
import { getBytes } from '../lib/binaryUtils'
import { Emulator } from '../lib/Emulator'
import { createCartridge } from '../lib/cartridges/factory'
import { formatBytes } from '../lib/utils'
import { IDisplay } from '../lib/types'

const cartridgeTypeMapping: { [type: number]: string } = {
  0x00: 'ROM ONLY',
  0x01: 'MBC1',
  0x02: 'MBC1+RAM',
  0x03: 'MBC1+RAM+BATTERY',
  0x05: 'MBC2',
  0x06: 'MBC2+BATTERY',
  0x08: 'ROM+RAM',
  0x09: 'ROM+RAM+BATTERY',
  0x0b: 'MMM01',
  0x0c: 'MMM01+RAM',
  0x0d: 'MMM01+RAM+BATTERY',
  0x0f: 'MBC3+TIMER+BATTERY',
  0x10: 'MBC3+TIMER+RAM+BATTERY',
  0x11: 'MBC3',
  0x12: 'MBC3+RAM',
  0x13: 'MBC3+RAM+BATTERY',
  0x19: 'MBC5',
  0x1a: 'MBC5+RAM',
  0x1b: 'MBC5+RAM+BATTERY',
  0x1c: 'MBC5+RUMBLE',
  0x1d: 'MBC5+RUMBLE+RAM',
  0x1e: 'MBC5+RUMBLE+RAM+BATTERY',
  0x20: 'MBC6',
  0x22: 'MBC7+SENSOR+RUMBLE+RAM+BATTERY',
  0xfc: 'POCKET CAMERA',
  0xfd: 'BANDAI TAMA5',
  0xfe: 'HuC3',
  0xff: 'HuC1+RAM+BATTERY'
}

export const Center = styled.div<{ margin?: string }>`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: ${(props) => props.margin || 0};
`

export function openBinaryFile(file: File): Promise<string> {
  return new Promise((resolve) => {
    const reader = new FileReader()
    reader.onload = () => {
      resolve(String(reader.result))
    }

    reader.readAsBinaryString(file)
  })
}

export function loadROM(rom: string) {
  return import(
    /* webpackInclude: /\.gb$/ */
    `../roms/${rom}`
  ).then((mod) => mod.default)
}

export function createEmulator(
  input: string,
  display: IDisplay,
  useBios = false
) {
  const bytes = getBytes(input)

  const emulator = new Emulator(display, { useBios })
  const { cartridge, meta } = createCartridge(bytes)

  const { title, cartridgeType, romType, romSize, ramType, ramSize } = meta

  console.log(
    `[Cartridge] Title=${title}, type=${
      cartridgeTypeMapping[cartridgeType]
    } (${cartridgeType}), ROM (type=${romType}, size=${formatBytes(
      romSize
    )}), RAM (type=${ramType}, size=${formatBytes(ramSize)})`
  )

  emulator.loadCartridge(cartridge)
  return emulator
}

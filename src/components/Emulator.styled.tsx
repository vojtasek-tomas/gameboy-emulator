import styled, { ThemedStyledProps } from 'styled-components'

import { Theme } from './types'

const SCREEN_PADDING_LEFT = 45
const SCREEN_PADDING_TOP = 23

const BATTERY_FONT_SIZE = 6
const INDICATOR_SIZE = 9

function zoom<P, T extends Theme>(value: number) {
  return (props: ThemedStyledProps<P, T>) => `${props.theme.zoom * value}px`
}

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`

export const GameBoy = styled.div`
  display: inline-block;
  margin: 10px;
  background: #eee;
  border-radius: 10px 10px 60px 10px;
  box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
  padding: ${zoom(20)};
`

export const Display = styled.div`
  margin: 10px;
  background-color: #777;
  border-radius: ${zoom(7)} ${zoom(7)} 70px ${zoom(7)};
  box-shadow: inset 0px 0px 20px 0px rgba(0, 0, 0, 0.66);
`

export const DisplayTop = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: ${zoom(SCREEN_PADDING_TOP)};
  font-size: ${zoom(BATTERY_FONT_SIZE)};
  font-family: Arial;
  color: #b3b3b3;
  padding: 0 ${zoom(8)} 0;
`

const DisplayHeaderText = styled.div`
  flex: 0 0 auto;
  margin: 0 ${zoom(5)};
`

export const DisplayContent = styled.div`
  display: flex;
  margin-right: ${zoom(SCREEN_PADDING_LEFT)};
  padding-bottom: ${zoom(SCREEN_PADDING_TOP)};
`

const DisplayLine = styled.div<{ width: string }>`
  flex: 1 1 ${(props) => props.width};
  height: ${zoom(3)};
  background-color: #8b1d90;
  box-shadow: 0 ${zoom(2 * 3)} 0 #283593;
  margin-top: -${zoom(2 * 3)};
`

export const Screen = styled.div`
  margin-left: auto;
  margin-right: auto;
`

const BatteryIndicator = styled.div<{ enabled?: boolean }>`
  background-color: ${(props) => (props.enabled ? `#f00` : '#000')};
  box-shadow: 0 0 3px 1px #ef5350;
  height: ${zoom(INDICATOR_SIZE)};
  width: ${zoom(INDICATOR_SIZE)};
  border-radius: ${(props) => (props.theme.zoom * INDICATOR_SIZE) / 2}px;
  margin: 10px 20px 10px 10px;
  box-shadow: inset 0px 0px 5px 0px rgba(0, 0, 0, 0.66);
`

const Battery = styled.div`
  font-family: Arial;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: #b3b3b3;
  width: ${zoom(SCREEN_PADDING_LEFT)};
  font-size: ${zoom(BATTERY_FONT_SIZE)};
  margin-bottom: 80px;
`

export const Styled = {
  Wrapper,
  GameBoy,
  Display,
  DisplayTop,
  DisplayHeaderText,
  DisplayContent,
  DisplayLine,
  Screen,
  Battery,
  BatteryIndicator
}

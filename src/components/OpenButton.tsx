import React from 'react'

type Props = {
  onOpen: (file?: File) => void
}

export function OpenButton(props: Props) {
  return (
    <div>
      <input
        onChange={(event) => {
          const { files } = event.currentTarget
          if (files) {
            props.onOpen(files[0])
          }
        }}
        accept="*"
        style={{ display: 'none' }}
        id="file"
        multiple
        type="file"
      />
      <label htmlFor="file">
        <span>Open file</span>
      </label>
    </div>
  )
}

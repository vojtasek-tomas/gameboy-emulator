import React, { useState } from 'react'
import styled from 'styled-components'

import { Center } from '../utils'

type Props = {
  onNextStep: () => void
  onNextFrame: () => void
  executeCycles: (cycles: number) => void
}

const Styled = {
  Button: styled.div`
    margin: 0 5px;
  `
}

export function Debugger(props: Props) {
  const [cycles, setCycles] = useState(0)

  return (
    <>
      <Center margin="0 0 10px">
        <Styled.Button>
          <button onClick={props.onNextStep}>Next step</button>
        </Styled.Button>

        <Styled.Button>
          <button onClick={props.onNextFrame}>Next frame</button>
        </Styled.Button>

        <Styled.Button>
          <button onClick={() => props.executeCycles(cycles)}>
            Execute cycles
          </button>
        </Styled.Button>
        <input
          type="text"
          value={cycles}
          onChange={(e) => setCycles(Number(e.currentTarget.value))}
        />
      </Center>
    </>
  )
}

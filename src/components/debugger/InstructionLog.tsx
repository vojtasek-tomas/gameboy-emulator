import React, { useRef, useEffect } from 'react'
import { FixedSizeList, ListChildComponentProps } from 'react-window'
import styled from 'styled-components'
import { formatHex } from '../../lib/utils'
import { LogEntry } from '../types'

type Props = {
  entries: LogEntry[]
  onRowClick: (entry: LogEntry, index: number) => void
  activeEntryIndex: number
}

const ITEM_HEIGHT = 20

function formatLogEntry(entry: any) {
  const { instruction, pc } = entry

  return `PC @ ${formatHex(pc, 4)} | ${instruction}`
}

const Styled = {
  Row: styled.div<{ isActive: boolean }>`
    font-family: monospace;
    white-space: pre;
    cursor: pointer;
    background: ${(props) => (props.isActive ? '#efefef' : 'none')};
  `,
  RowInner: styled.div`
    padding: 0 10px;
    display: flex;
    align-items: center;
    height: 100%;
  `,
  InstructionInfo: styled.div`
    flex: 1;
  `,
  Cycles: styled.div`
    text-align: right;
  `
}

export function InstructionLog(props: Props) {
  const { entries, activeEntryIndex } = props

  const listRef = useRef<FixedSizeList>(null)

  useEffect(() => listRef.current?.scrollToItem(activeEntryIndex), [
    activeEntryIndex
  ])

  const renderRow = ({ index, style, data }: ListChildComponentProps) => {
    const entry: LogEntry = data[index]
    return (
      <Styled.Row
        onClick={() => props.onRowClick(entry, index)}
        style={style}
        isActive={activeEntryIndex === index}>
        <Styled.RowInner>
          <Styled.InstructionInfo>
            {formatLogEntry(entry)}
          </Styled.InstructionInfo>
          <Styled.Cycles># {entry.cyclesCount}</Styled.Cycles>
        </Styled.RowInner>
      </Styled.Row>
    )
  }

  if (entries.length === 0) {
    return <div>Log is empty</div>
  }

  return (
    <FixedSizeList
      ref={listRef}
      itemData={entries}
      height={300}
      itemCount={entries.length}
      itemSize={ITEM_HEIGHT}
      width="100%">
      {renderRow}
    </FixedSizeList>
  )
}

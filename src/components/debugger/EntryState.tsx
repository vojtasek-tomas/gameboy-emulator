import React from 'react'

import styled from 'styled-components'
import { LogEntry } from '../types'
import { formatHex } from '../../lib/utils'

type Props = {
  entry: LogEntry
}

const Styled = {
  EntryState: styled.div`
    padding: 10px;
  `,
  Chips: styled.div`
    display: flex;
    flex-wrap: wrap;
    margin: 10px 0;
  `
}

export function EntryState(props: Props) {
  const { entry } = props
  const fState = entry.flags.map((f: any) => (f[1] ? f[0] : '_')).join('')

  const chips = entry.registers.map((r: any) => (
    <div key={r[0]}>
      {r[0]} | {formatHex(r[1])}
    </div>
  ))
  return (
    <Styled.EntryState>
      Registers
      <Styled.Chips>{chips}</Styled.Chips>
      Flags
      <pre>{fState}</pre>
      <pre>LCDC (0xff40){formatHex(entry.LCDC)}</pre>
      <pre>STAT (0xff41){formatHex(entry.STAT)}</pre>
      <pre>LY (0xff44){formatHex(entry.LY)}</pre>
      <pre>IF (0xff0f){formatHex(entry.IF)}</pre>
      <pre>IE (0xffff){formatHex(entry.IE)}</pre>
    </Styled.EntryState>
  )
}

import React, { useState, useRef, useEffect } from 'react'
import { useLocalStorage } from '@rehooks/local-storage'

import { Styled } from './Emulator.styled'
import { DISPLAY_HEIGHT, DISPLAY_WIDTH } from '../lib/constants'
import { Display } from './Display'
import { Controls } from './Controls'
import { createEmulator, openBinaryFile, loadROM } from './utils'
import { Emulator as EmulatorType } from '../lib/Emulator'
import { Debugger } from './debugger/Debugger'
import { RomTable } from './roms/RomTable'

// @ts-ignore
// import binary from '../roms/demos/oh.gb'
import binary from '../roms/scribbltests/statcount/statcount.gb'
import { Memory } from './Memory'
import { CanvasDisplay } from '../lib/ppu/displays/CanvasDisplay'

type Props = {
  zoom: number
}

export function Emulator(props: Props) {
  const { zoom } = props
  const [showDebugger, setShowDebugger] = useLocalStorage(
    'show-debugger',
    false
  )

  const [isRunning, setRunning] = useState(false)
  const [cartridgeData, setCartridgeData] = useState('')
  const canvasRef = useRef<HTMLCanvasElement>(null)
  const emulator = useRef<EmulatorType | null>(null)

  const onStateChanged = (isRunningState: boolean) => {
    setRunning(isRunningState)
  }

  const setup = (cartridge: string, start: boolean) => {
    window.scrollTo({ top: 0 })

    if (emulator.current) {
      emulator.current.stop()
    }

    if (!canvasRef.current) {
      console.error('Missing <canvas> element.')
      return
    }

    const display = new CanvasDisplay(canvasRef.current)
    const newEmulator = createEmulator(cartridge, display, false)
    setCartridgeData(cartridge)
    // newEmulator.onExecute = (pc, instr, _opCode, cycles) => {
    //   if (cycles > 381_000 && cycles < 383_000) {
    //     console.log(
    //       `${cycles} PC=${pc.toString(
    //         16
    //       )} ${instr}, PPU mode =${newEmulator.ppu.getMode()}, LY=${
    //         newEmulator.ppu.ly
    //       }, STAT=${newEmulator.ppu.readByte(0xff41).toString(16)}, ppu clk=${
    //         newEmulator.ppu.lineClocks
    //       }, statflag=${newEmulator.ppu.previousStatFlag}`
    //     )
    //   }
    // }

    newEmulator.onStateChanged = onStateChanged
    if (start) {
      newEmulator.run()
    }
    emulator.current = newEmulator
    // @ts-ignore
    window.e = newEmulator
  }

  const onRestart = () => setup(cartridgeData, true)

  const onOpen = (file?: File) => {
    if (!file) {
      return
    }
    openBinaryFile(file).then((data) => setup(data, true))
  }

  const onNextStep = () => emulator.current?.runStep()

  const onNextFrame = () => emulator.current?.runFrame(true)

  const runCartridge = (rom: string) =>
    loadROM(rom).then((data) => setup(data, true))
  const openCartridge = (rom: string) =>
    loadROM(rom).then((data) => setup(data, false))

  useEffect(() => {
    setup(binary, false)
  }, []) // mount-only

  return (
    <>
      <Styled.Wrapper>
        <Styled.GameBoy>
          <Display enabled={isRunning}>
            <canvas
              style={{
                display: 'block', // prevents wierd margin
                imageRendering: 'pixelated',
                zoom
              }}
              ref={canvasRef}
              height={DISPLAY_HEIGHT}
              width={DISPLAY_WIDTH}
            />
          </Display>
        </Styled.GameBoy>
      </Styled.Wrapper>
      <Controls
        onOpen={onOpen}
        onStart={() => emulator.current?.run()}
        onStop={() => emulator.current?.stop()}
        onRestart={onRestart}
        showDebugger={showDebugger}
        toggleDebugger={setShowDebugger}
      />
      {showDebugger && (
        <>
          <Debugger
            executeCycles={(cycles) => emulator.current?.runCycles(cycles)}
            onNextFrame={onNextFrame}
            onNextStep={onNextStep}
          />
          <RomTable
            onOpen={(romName) => openCartridge(romName)}
            onRun={(romName) => runCartridge(romName)}
          />
          {emulator.current && (
            <Memory isRunning={isRunning} mmu={emulator.current.mmu} />
          )}
        </>
      )}
    </>
  )
}

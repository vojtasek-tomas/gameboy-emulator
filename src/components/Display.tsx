import React from 'react'
import { Styled } from './Emulator.styled'

type Props = {
  children: React.ReactNode
  enabled?: boolean
}

export function Display(props: Props) {
  return (
    <Styled.Display>
      <Styled.DisplayTop>
        <Styled.DisplayLine width="25%" />
        <Styled.DisplayHeaderText>
          DOT MATRIX WITH STEREO SOUND
        </Styled.DisplayHeaderText>
        <Styled.DisplayLine width="12%" />
      </Styled.DisplayTop>

      <Styled.DisplayContent>
        <Styled.Battery>
          <Styled.BatteryIndicator enabled={props.enabled} />
          BATTERY
        </Styled.Battery>

        <Styled.Screen>{props.children}</Styled.Screen>
      </Styled.DisplayContent>
    </Styled.Display>
  )
}

import React from 'react'
import ReactDOM from 'react-dom'
import { createGlobalStyle } from 'styled-components'

import { Application } from './components/Application'

// import 'typeface-roboto'

// Check for root node
const id = 'application'
const rootNode = document.getElementById(id)
if (!rootNode) {
  throw new Error(`Include <div id="${id}"></div> in the app html file.`)
}

const GlobalStyle = createGlobalStyle`
  html,
  body,
  #application {
    min-height: 100%;
    min-width: 100%;
    margin: 0;
    font-family: Arial;
  }
`

const app = (
  <>
    <GlobalStyle />
    <Application />
  </>
)

ReactDOM.render(app, rootNode)
// @ts-ignore
// ReactDOM.createRoot(rootNode).render(app)

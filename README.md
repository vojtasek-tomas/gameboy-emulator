# GameBoy emulator written in TypeScript

## [Test results](./integration_tests/test_results.md)

## Status

Done:

- CPU (passes Blargg's tests)
- Timers (passes mooneye's tests)
- PPU
  - Fetcher & FIFO for background & window
  - Scanline render for sprites

Work in progress:

- PPU accuracy

TODO:

- Fetcher for sprites
- Audio unit
- proper debuger
- better UI
- GameBoy Color support
- Serial unit
